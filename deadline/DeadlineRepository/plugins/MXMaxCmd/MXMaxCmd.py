import re

from System import *
from System.Diagnostics import *
from System.IO import *
from System.Environment import *
from System.Text import Encoding

from Deadline.Plugins import *
from Deadline.Scripting import *
from Deadline.Slaves import *

from System.Diagnostics import Stopwatch

######################################################################
## Import standard python module
######################################################################
import sys
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
if root not in sys.path:
    sys.path.append(root)

import tempfile, os

from PyDeadline.deadline import env, plugins
from PyDeadline.deadline import callback
from PyDeadline.deadline import MxDLJob

######################################################################
## This is the function that Deadline calls to get an instance of the
## main DeadlinePlugin class. 
######################################################################
def GetDeadlinePlugin():
    return MaxCmdPlugin()

######################################################################
## This is the main DeadlinePlugin class for the 3dsCmd plugin.
######################################################################
class MaxCmdPlugin (DeadlinePlugin):
    StripStitching = False
    Framecount = 0
    
    LocalRendering = False
    NetworkFilePath = ""
    LocalFilePath = ""
    
    ## Called by Deadline to initialize the process.
    def InitializeProcess( self ):
        # Set the plugin specific settings.
        self.SingleFramesOnly = False
        self.PluginType = PluginType.Simple
        
        # Set the process specific settings.
        self.ProcessPriority = ProcessPriorityClass.BelowNormal
        self.UseProcessTree = True
        self.PopupHandling = True
        self.StdoutHandling = True
        
        # Set the stdout handlers.
        self.AddStdoutHandler( "'ERROR :.*", self.HandleStdoutError ) 
        self.AddStdoutHandler( "Frame [0-9]+ assigned", self.HandleStdoutProgress )
        
        # Set the popup ignorers.
        self.AddPopupIgnorer( ".*Render history settings.*" )
        self.AddPopupIgnorer( ".*Rendering In Progress.*" )
        self.AddPopupIgnorer( ".*Rendering -.*" )
        self.AddPopupIgnorer( ".*Maxwell Translation Window.*" )
        
        # Set the popup handlers.
        self.AddPopupHandler( ".*Microsoft Visual C\\+\\+ Runtime Library.*", "OK" )
        self.AddPopupHandler( ".*V-Ray warning.*", "OK" )
        
        # Handle Maxwell Plug-in Update Notification dialog
        self.AddPopupHandler( ".*Maxwell Plug-in Update Notification.*", "Don't notify me about this version automatically;Close" )
        
        # Handle RealFlow Plug-in Update Notification dialog
        self.AddPopupHandler( ".*RealFlow Plug-in Update Notification.*", "Don't notify me about this version automatically;Close" )

        # Handle unit type Notification dialog
        self.AddPopupHandler( ".*File Load: Units Mismatch.*", "OK" )
        
    ## Called by Deadline to get the render executable.
    def RenderExecutable( self ):
        cmdExe = ""
        
        if( GetPluginInfoEntryWithDefault( "Application", "Max" )  == "Viz" ):
            versionStr = GetPluginInfoEntryWithDefault( "Version", "2008" )
            vizCmdExeList = GetConfigEntry( "Viz" + versionStr + "Cmd_RenderExecutable" )
            cmdExe = SearchFileList( vizCmdExeList )
            if( cmdExe == "" ):
                FailRender( "VIZ " + versionStr + " render executable was not found in the semicolon separated list \"" + vizCmdExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        else:
            version = GetIntegerPluginInfoEntryWithDefault( "Version", 8 )
            maxCmdExeList = GetConfigEntry( "Max" + str(version) + "Cmd_RenderExecutable" )
            
            if( version < 9 ):
                LogInfo( "Not enforcing a build of Max" )
                cmdExe = SearchFileList( maxCmdExeList )
                if( cmdExe == "" ):
                    FailRender( "Max " + str(version) + " render executable was not found in the semicolon separated list \"" + maxCmdExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
            else:
                build = GetPluginInfoEntryWithDefault( "Build", "None" )
                if( build == "32bit" ):
                    LogInfo( "Enforcing 32 bit build of Max" )
                    cmdExe = SearchFileListFor32Bit( maxCmdExeList )
                    if( cmdExe == "" ):
                        FailRender( "32 bit Max " + str(version) + " render executable was not found in the semicolon separated list \"" + maxCmdExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
                elif( build == "64bit" ):
                    LogInfo( "Enforcing 64 bit build of Max" )
                    cmdExe = SearchFileListFor64Bit( maxCmdExeList )
                    if( cmdExe == "" ):
                        FailRender( "64 bit Max " + str(version) + " render executable was not found in the semicolon separated list \"" + maxCmdExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
                else:
                    LogInfo( "Not enforcing a build of Max" )
                    cmdExe = SearchFileList( maxCmdExeList )
                    if( cmdExe == "" ):
                        FailRender( "Max " + str(version) + " render executable was not found in the semicolon separated list \"" + maxCmdExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        return cmdExe
    
    ## Called by Deadline to get the render arguments.
    def RenderArgument( self ):
        try:
            cmdExe = self.RenderExecutable()
            max_dir = os.path.dirname(cmdExe)

            ### prepare plugin.ini file
            plugin_path = max_dir + '/plugin.ini'
            plugin_lang_dir = ['en-US', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'ja-JP', 'ko-KR', 'pl-PL',
                               'cs-CZ', 'de-DE', 'pt-BR', 'ru-RU', 'zh-CN', 'zh-TW']
            plugin_lang_path = max_dir + '/%s/plugin.ini'
            plugin_bak_path = max_dir + '/plugin.ini.bak'
            if cmdExe != '':
                if not os.path.exists(plugin_path):
                    File.Copy(plugin_lang_path % plugin_lang_dir[0], plugin_path, True)
                elif not os.path.exists(plugin_bak_path):
                    File.Copy(plugin_path, plugin_bak_path, True)
                else:
                    File.Copy(plugin_bak_path, plugin_path, True)

            env_map_json = GetJobInfoEntry(MxDLJob.hEnv)
            plugins_json = GetJobInfoEntry(MxDLJob.hPlugins)
            plugins_env_map = plugins.mxConfigMaxPluginEnv(plugins_json)

            ctx = plugins.mxConfigMaxPlugin(plugins_json)
            plugins.mxCopyMaxPluginRelatedFiles(plugins_json, max_dir)
            ini_fp = open(plugin_path, 'r+')
            try:
                orig_ctx = ini_fp.read()
                if not orig_ctx.endswith('\n'):
                    ctx = '\n'+ctx
                ini_fp.write(ctx)
            finally:
                ini_fp.close()

            for lang in plugin_lang_dir:
                pp = plugin_lang_path % lang
                if os.path.exists(pp):
                    File.Copy(plugin_path, pp, True)

            # prepare mxp file
            bitmapRoot = GetPluginInfoEntryWithDefault("BitmapRoot", "").strip()
            if bitmapRoot != '':
                versionStr = GetPluginInfoEntryWithDefault("Version", "2008")
                ctx = plugins.mxConfigMaxPathFile(bitmapRoot, versionStr)
                # pathConfigFile = 'c:\\mx_max_path_file.mxp'
                # File.WriteAllText(pathConfigFile, ctx, Encoding.Default)

            orig_scene_path = GetPluginInfoEntryWithDefault("SceneFile", GetDataFilename())
            scene_path = plugins.mxMakeTempFile(orig_scene_path)
            output_root = GetPluginInfoEntryWithDefault("OutputFilename", "").strip()
            versionStr = GetPluginInfoEntryWithDefault("Version", "2008")
            plugins.mxPrepareFile(versionStr, scene_path, output_root, '', max_dir, env_map_json, plugins_env_map)
            #plugins.mxPrepareFile(versionStr, scene_path, output_root, '', max_dir, None, None)

            # prepare arguments
            arguments = ""

            contOnErrPart = ""
            continueOnError = True #GetBooleanPluginInfoEntryWithDefault( "ContinueOnError", False )
            if(continueOnError) :
                contOnErrPart = " -continueOnError"

            stripPart = ""
            stripRendering = GetBooleanPluginInfoEntryWithDefault( "StripRendering", False )
            if( stripRendering ):
                strips = GetPluginInfoEntryWithDefault( "StripCount", "1" ).strip()
                overlap = GetPluginInfoEntryWithDefault( "StripOverlap", "0" ).strip()
                if( self.StripStitching ):
                    stripPart = " -stitch:" + strips + "," + overlap
                else:
                    strip = GetPluginInfoEntryWithDefault( "StripNumber", "1" ).strip()
                    stripPart = " -strip:" + strips + "," + overlap + "," + strip

            # Don't apply gamma correction during the stitching phase.
            gammaCorrectionPart = ""
            if not self.StripStitching and GetBooleanPluginInfoEntryWithDefault( "GammaCorrection", False ):
                gammaCorrectionPart = " -gammaCorrection:true"
                gammaCorrectionPart += " -gammaValueIn:" + GetPluginInfoEntryWithDefault("GammaInput", "1.0")
                gammaCorrectionPart += " -gammaValueOut:" + GetPluginInfoEntryWithDefault("GammaOutput", "1.0")
            else:
                gammaCorrectionPart = " -gammaCorrection:false"

            self.LocalRendering = False
            #outputFile = GetPluginInfoEntryWithDefault( "OutputFilename", "" ).strip()
            outputFile = ''
            if( outputFile != "" ):
                # Create the output directory if it doesn't exist.
                outputDir = Path.GetDirectoryName( outputFile )
                if not Directory.Exists( outputDir ):
                    Directory.CreateDirectory( outputDir )

                # Don't check for local rendering if we're just stitching the strips together.
                if( not self.StripStitching ):
                    #self.LocalRendering = GetBooleanConfigEntry( "LocalRendering" )
                    self.LocalRendering = GetBooleanPluginInfoEntryWithDefault( "LocalRendering", False )
                    if self.LocalRendering:
                        self.NetworkFilePath = outputDir
                        outputDir = CreateTempDirectory( "3dsOutput" )
                        self.LocalFilePath = outputDir
                        outputFile = Path.Combine( outputDir, Path.GetFileName( outputFile ) )

                        LogInfo( "Rendering to local drive, will copy files and folders to final location after render is complete" )
                    else:
                        LogInfo( "Rendering to network drive" )

                outputFile = "\"" + outputFile + "\""

            pathConfigFile = GetPluginInfoEntryWithDefault("PathConfigFile", "").strip()
            if( pathConfigFile != "" ):
                pathConfigFile = Path.Combine(GetJobsDataDirectory(), pathConfigFile)
                pathConfigFile = "\"" + pathConfigFile + "\""

            #arguments += "\"" + GetPluginInfoEntryWithDefault( "SceneFile", GetDataFilename() ) + "\""
            arguments += "\"" + scene_path + "\""
            arguments += " -v:" + str(GetIntegerConfigEntry("VerbosityLevel"))
            arguments += " -start:" + str(GetStartFrame())
            arguments += " -end:" + str(GetEndFrame())
            arguments += BlankIfEitherIsBlank( BlankIfEitherIsBlank(" -cam:\"", GetPluginInfoEntryWithDefault("Camera","").strip()), "\"")
            arguments += BlankIfEitherIsBlank(" -w:", GetPluginInfoEntryWithDefault("ImageWidth",""))
            arguments += BlankIfEitherIsBlank(" -h:", GetPluginInfoEntryWithDefault("ImageHeight",""))
            arguments += BlankIfEitherIsBlank(" -pixelAspect:", GetPluginInfoEntryWithDefault("PixelAspect",""))
            arguments += BlankIfEitherIsBlank(" -atmospherics:", GetPluginInfoEntryWithDefault("Atmospherics", ""))
            arguments += BlankIfEitherIsBlank(" -renderHidden:", GetPluginInfoEntryWithDefault("HiddenGeometry", ""))
            arguments += BlankIfEitherIsBlank(" -effects:", GetPluginInfoEntryWithDefault("Effects", ""))
            arguments += BlankIfEitherIsBlank(" -useAreaLights:", GetPluginInfoEntryWithDefault("AreaLights", ""))
            arguments += BlankIfEitherIsBlank(" -displacements:", GetPluginInfoEntryWithDefault("Displacements", ""))
            arguments += BlankIfEitherIsBlank(" -force2Sided:", GetPluginInfoEntryWithDefault("TwoSided", ""))
            arguments += BlankIfEitherIsBlank(" -videoColorCheck:", GetPluginInfoEntryWithDefault("VideoColorCheck", ""))
            arguments += BlankIfEitherIsBlank(" -superBlack:", GetPluginInfoEntryWithDefault("SuperBlack", ""))
            arguments += BlankIfEitherIsBlank(" -renderFields:", GetPluginInfoEntryWithDefault("RenderToFields", ""))
            arguments += BlankIfEitherIsBlank(" -skipRenderedFrames:", GetPluginInfoEntryWithDefault("SkipRenderedFrames", ""))
            arguments += BlankIfEitherIsBlank(" -outputName:", outputFile)
            arguments += BlankIfEitherIsBlank(" -pathFile:", pathConfigFile)
            arguments += str( gammaCorrectionPart )
            arguments += str( contOnErrPart )
            arguments += str( stripPart )
            arguments +=" -rfw:" + GetPluginInfoEntryWithDefault("ShowVFB","1")
            arguments +=" -videopostJob:" + GetPluginInfoEntryWithDefault("VideoPost","0")

            return arguments
        except:
            import traceback
            LogInfo(traceback.format_exc())
            return ''
    
    ## Called by Deadline before the render begins.
    def PreRenderTasks( self ):
        env_map_json = GetJobInfoEntry(MxDLJob.hEnv)
        plugins_json = GetJobInfoEntry(MxDLJob.hPlugins)
        dir_map_json = GetJobInfoEntry(MxDLJob.hDirMap)

        plugins_env_map = plugins.mxConfigMaxPluginEnv(plugins_json)

        env.mxSetDriverMapping(dir_map_json)
        env.mxSetMaxEnvironment(env_map_json, plugins_env_map)

        self.stopwatch = Stopwatch()
        self.stopwatch.Start()
        
        
    ## Called by Deadline after the renderer has exited.
    def PostRenderTasks( self ):
        if( self.LocalRendering ):
            LogInfo( "Moving output files and folders from " + self.LocalFilePath + " to " + self.NetworkFilePath )
            VerifyAndMoveDirectory( self.LocalFilePath, self.NetworkFilePath, True, 0 )
        
        # If we are performing a strip render, try to assemble the images.
        stripRendering = GetBooleanPluginInfoEntryWithDefault( "StripRendering", False )
        if stripRendering:
            self.StripStitching = True
            executable = self.RenderExecutable()
            arguments = self.RenderArgument()
            RunProcess( executable, arguments, Path.GetDirectoryName( executable ), -1 )

        ## rollback PATH environment variable
        env.mxRollbackPathEnvironment()

        ## send cost request
        self.stopwatch.Stop()

        r_time = self.stopwatch.Elapsed.TotalHours

        sm = SlaveManager.GetInstance()
        cpus = sm.Slave.CPUs
        task_id = sm.Slave.CurrentTasksString
        job = sm.Slave.CurrentJob

        cb = callback.MxDLTaskFinishCallback(job, task_id, cpus, r_time)
        LogInfo(cb.send())

        
    def HandleStdoutError( self ):
        FailRender(self.GetRegexMatch(0))
        
    def HandleStdoutProgress( self ):
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        
        self.Framecount = self.Framecount + 1
        if( ( endFrame - startFrame) != -1 ):
                SetProgress( ( float(self.Framecount) / ( endFrame - startFrame + 1 ) ) * 100 )
                
        if( self.Framecount < ( endFrame - startFrame + 1 ) ):
                SetStatusMessage( "Rendering frame " + str( startFrame + self.Framecount ) )

