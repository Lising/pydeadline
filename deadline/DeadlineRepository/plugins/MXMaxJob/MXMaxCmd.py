import clr

from System import *
from System.Collections.Specialized import *
from System.Diagnostics import *
from System.IO import *
from System.Text.RegularExpressions import *
from System.Environment import *

from FranticX.Net import *
from FranticX.Processes import *

from Deadline.Plugins import *
from Deadline.Scripting import *
from Deadline.Slaves import *

from System.Diagnostics import Stopwatch

######################################################################
## Import standard python module
######################################################################
import sys
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
if root not in sys.path:
    sys.path.append(root)

import tempfile

from PyDeadline.deadline import env, plugins
from PyDeadline.deadline import callback
from PyDeadline.deadline import MxDLJob

######################################################################
## This is the function that Deadline calls to get an instance of the
## main DeadlinePlugin class.
######################################################################
def GetDeadlinePlugin():
    return MaxPlugin()

######################################################################
## This is the main DeadlinePlugin class for the Max plugin.
######################################################################
class MaxPlugin (DeadlinePlugin):
    MyMaxController = None

    ## Called by Deadline to initialize the process.
    def InitializeProcess( self ):
        # Set the plugin specific settings.
        self.SingleFramesOnly = False
        self.PluginType = PluginType.Advanced

    ## Called by Deadline when the job is first loaded.
    def StartJob( self ):
        LogInfo( "Start Job called - starting up 3dsmax plugin" )


        env_map_json = GetJobInfoEntry(MxDLJob.hEnv)
        plugins_json = GetJobInfoEntry(MxDLJob.hPlugins)
        dir_map_json = GetJobInfoEntry(MxDLJob.hDirMap)

        plugins_env_map = plugins.mxConfigMaxPlugin(plugins_json)

        env.mxSetDriverMapping(dir_map_json)
        env.mxSetMaxEnvironment(env_map_json, plugins_env_map)


        self.MyMaxController = MaxController()

        # Initialize the Max controller.
        self.MyMaxController.Initialize()

        # Start 3dsmax.
        self.MyMaxController.StartMax()

        # Start the 3dsmax job (loads the scene file, etc).
        self.MyMaxController.ResetBitmapPager()
        self.MyMaxController.StartMaxJob()

    ## Called by Deadline when the job is unloaded.
    def EndJob( self ):
        LogInfo( "End Job called - shutting down 3dsmax plugin" )

        # End the 3dsmax job (unloads the scene file, etc).
        self.MyMaxController.ResetBitmapPager()
        self.MyMaxController.EndMaxJob()

        # Shutdown 3dsmax.
        self.MyMaxController.ShutdownMax()

        env.mxRollbackPathEnvironment()

    ## Called by Deadline when a task is to be rendered.
    def RenderTasks( self ):
        LogInfo( "Render Tasks called" )

        # Render the tasks.
        self.MyMaxController.ResetBitmapPager()
        self.MyMaxController.RenderTasks()

    def MonitoredManagedProcessExit( self, name ):
        FailRender( "Monitored managed process \"" + name + "\" has exited or been terminated.\n%s" % self.MyMaxController.NetworkLogGet() )

########################################################################
## Globals For Running Managed 3dsmax Process
########################################################################
ManagedMaxProcessRenderExecutable = ""
ManagedMaxProcessRenderArgument = ""
ManagedMaxProcessStartupDirectory = ""

########################################################################
## Main Max Controller Class
########################################################################
class MaxController:

    ########################################################################
    ## Globals
    ########################################################################
    MaxSocket = None
    ProgramName = ""

    Version = -1
    ForceBuild = "none"
    Is64Bit = False
    IsMaxDesign = False
    LanguageCodeStr = ""
    LanguageSubDir = ""

    UseSlaveMode = True
    UseSilentMode = False
    UseUserProfiles = False
    UserProfileDataPath = ""
    MaxDataPath = ""
    FailOnExistingMaxProcess = True
    StrictMaxCheck = True
    RestartEachFrame = False
    ShowFrameBuffer = True
    StartupMaxScript = ""
    AuthentificationToken = ""
    ErrorMessageFile = ""
    MaxStartupFile = ""

    NetworkLogFile = ""
    NetworkLogFileSize = 0
    NetworkLogFilePostfix = ""
    NetworkLogValid = False
    NetworkLogError = ""

    OverrideBitmapPager = False
    OriginalBitmapPager = "0"

    LoadMaxTimeout = 1000
    StartJobTimeout = 1000
    ProgressUpdateTimeout = 8000
    DisableProgressUpdateTimeout = False

    MaxScriptJob = False
    MaxScriptJobScript = ""

    MaxRenderExecutable = ""
    MaxIni = ""
    MaxPluginIni = ""
    UserPluginIni = ""
    UsingCustomPluginIni = False
    LightningPluginFile = ""
    TempPluginIni = ""
    TempLightningIni = ""

    MaxFilename = ""
    Camera = ""

    StartFrame = 0
    EndFrame = 0
    CurrentFrame = 0

    AuxiliaryFilenames = None

    LocalRendering = True
    RedirectOutput = False
    RenderOutputOverride = ""
    FrameNumberBase = 0
    RemovePadding = False
    OverrideSaveFile = False
    SaveFile = False

    UseJpegOutput = False
    JpegOutputPath = ""

    IgnoreMissingExternalFiles = True
    IgnoreMissingUVWs = True
    IgnoreMissingXREFs = True
    IgnoreMissingDLLs = False
    DisableMultipass = False

    IgnoreRenderElements = False
    IgnoreRenderElementsByName = []

    FailOnBlackFrames = False
    BlackPixelPercentage = 0
    BlackPixelThreshold = 0.0
    BlackFramesCheckRenderElements = False

    RegionRendering = False
    RegionRenderingSingleJob = False
    RegionRenderingSingleFrame = "0"
    RegionPadding = 0
    RegionLeft = 0
    RegionTop = 0
    RegionRight = 0
    RegionBottom = 0
    RegionType = "CROP"

    PreFrameScript = ""
    PostFrameScript = ""
    PreLoadScript = ""
    PostLoadScript = ""
    PathConfigFile = ""
    MergePathConfigFile = False

    FunctionRegex = Regex( "FUNCTION: (.*)" )
    SuccessMessageRegex = Regex( "SUCCESS: (.*)" )
    SuccessNoMessageRegex = Regex( "SUCCESS" )
    CanceledRegex = Regex( "CANCELED" )
    ErrorRegex = Regex( "ERROR: (.*)" )

    ProgressRegex = Regex( "PROGRESS (.*)" )
    SetTitleRegex = Regex( "SETTITLE (.*)" )
    StdoutRegex = Regex( "STDOUT: (.*)" )
    WarnRegex = Regex( "WARN: (.*)" )
    GetJobInfoEntryRegex = Regex( "GETJOBINFOENTRY (.*)" )
    GetSubmitInfoEntryRegex = Regex( "GETSUBMITINFOENTRY (.*)" )
    GetSubmitInfoEntryElementCountRegex = Regex( "GETSUBMITINFOENTRYELEMENTCOUNT (.*)" )
    GetSubmitInfoEntryElementRegex = Regex( "GETSUBMITINFOENTRYELEMENT ([^,]*),(.*)" )
    GetAuxFileRegex = Regex( "GETAUXFILE (.*)" )

    ########################################################################
    ## Constructor
    ########################################################################
    def __init__( self ):
        self.ProgramName = "3dsmaxProcess"

    ########################################################################
    ## Main functions (to be called from Deadline Entry Functions)
    ########################################################################
    # Reads in the plugin configuration settings and sets up everything in preparation to launch 3dsmax.
    # Also does some checking to ensure a 3dsmax job can be rendered on this machine.
    def Initialize( self ):
        # Read in the 3dsmax version.
        self.Version = GetIntegerPluginInfoEntry( "Version" )
        if( self.Version < 9 ):
            FailRender( "Only 3dsmax 9 and later is supported" )

        # Max 2010 and later have different settings for Design edition now, so we need to handle appropriately.
        if( self.Version >= 2010 ):
            self.IsMaxDesign = GetBooleanPluginInfoEntryWithDefault( "IsMaxDesign", False )

        if self.IsMaxDesign:
            LogInfo( "Rendering with 3dsmax Design version: %d" % self.Version )
        else:
            LogInfo( "Rendering with 3dsmax version: %d" % self.Version )

        # Read in the Build of 3dsmax to force.
        self.ForceBuild = GetPluginInfoEntryWithDefault( "MaxVersionToForce", "none" ).lower()
        if( self.Version > 2013 ):
            self.ForceBuild = "none"
            LogInfo( "Not forcing a build of 3dsmax because version 2014 and later is 64 bit only" )
        else:
            LogInfo( "Build of 3dsmax to force: %s" % self.ForceBuild )

        # Figure out the render executable to use for rendering.
        renderExecutableKey = "RenderExecutable" + str(self.Version)
        maxEdition = ""
        if self.IsMaxDesign:
            renderExecutableKey = renderExecutableKey + "Design"
            maxEdition = " Design"

        renderExecutableList = GetConfigEntry( renderExecutableKey ).strip()
        if( self.ForceBuild == "32bit" ):
            self.MaxRenderExecutable = SearchFileListFor32Bit( renderExecutableList )
            if( self.MaxRenderExecutable == "" ):
                FailRender( "No 32 bit 3dsmax" + maxEdition + " " + str(self.Version) + " render executable found in the semicolon separated list \"" + renderExecutableList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        elif( self.ForceBuild == "64bit" ):
            self.MaxRenderExecutable = SearchFileListFor64Bit( renderExecutableList )
            if( self.MaxRenderExecutable == "" ):
                FailRender( "No 64 bit 3dsmax" + maxEdition + " " + str(self.Version) + " render executable found in the semicolon separated list \"" + renderExecutableList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        else:
            self.MaxRenderExecutable = SearchFileList( renderExecutableList )
            if( self.MaxRenderExecutable == "" ):
                FailRender( "No 3dsmax" + maxEdition + " " + str(self.Version) + " render executable found in the semicolon separated list \"" + renderExecutableList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        LogInfo( "Rendering with executable: %s" % self.MaxRenderExecutable )

        LogInfo( "Checking registry for 3dsmax language code" )
        maxInstallDirectory = Path.GetDirectoryName( self.MaxRenderExecutable )

        maxVersionStr = str(self.Version)
        if( self.Version > 9 ):
            maxIntVersion = self.Version - 1998
            maxVersionStr = str(maxIntVersion)
        maxVersionStr += ".0"

        if self.Version < 2013:
            languageCode = ""

            englishCode = "409"
            frenchCode = "40C"
            germanCode = "407"
            japaneseCode = "411"
            simplifiedChineseCode = "804"
            koreanCode = "412"

            maxKeyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\3DSMAX\\" + maxVersionStr + "\\MAX-1:"
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxKeyName, englishCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxKeyName, frenchCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxKeyName, germanCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxKeyName, japaneseCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxKeyName, simplifiedChineseCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxKeyName, koreanCode, languageCode )

            maxWowKeyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Autodesk\\3dsMax\\" + maxVersionStr + "\\MAX-1:"
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxWowKeyName, englishCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxWowKeyName, frenchCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxWowKeyName, germanCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxWowKeyName, japaneseCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxWowKeyName, simplifiedChineseCode, languageCode )
            languageCode = self.AutoCheckRegistryForLanguage( maxInstallDirectory, maxWowKeyName, koreanCode, languageCode )

            if languageCode != "":
                LogInfo( "Found language code: " + languageCode )
                if languageCode == englishCode:
                    self.LanguageCodeStr = "enu"
                elif languageCode == frenchCode:
                    self.LanguageCodeStr = "fra"
                elif languageCode == germanCode:
                    self.LanguageCodeStr = "deu"
                elif languageCode == japaneseCode:
                    self.LanguageCodeStr = "jpn"
                elif languageCode == simplifiedChineseCode:
                    self.LanguageCodeStr = "chs"
                elif languageCode == koreanCode:
                    self.LanguageCodeStr = "kor"
                else:
                    FailRender( "Unsupported language code: " + languageCode + ". Please email this error report to deadline-support@primefocusworld.com so we can add support for this language code." )
            else:
                LogWarning( "Language code could not be found. Defaulting to 409 (English)" )
                self.LanguageCodeStr = "enu"

            LogInfo( "Language code string: " + self.LanguageCodeStr )
        else:
            maxKeyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\3DSMAX\\" + maxVersionStr
            languageCode = GetRegistryKeyValue( maxKeyName, "DefaultLanguage", "" )
            if( languageCode == "" ):
                maxKeyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Autodesk\\3DSMAX\\" + maxVersionStr
                languageCode = GetRegistryKeyValue( maxKeyName, "DefaultLanguage", "" )
                if( languageCode == "" ):
                    LogWarning( "Language code could not be found in registry. Defaulting to ENU (English)" )
                    languageCode = "ENU"

            maxKeyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\3DSMAX\\" + maxVersionStr + "\\LanguagesInstalled\\" + languageCode
            languageSubDir = GetRegistryKeyValue( maxKeyName, "LangSubDir", "" )
            if( languageSubDir == "" ):
                maxKeyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Autodesk\\3DSMAX\\" + maxVersionStr + "\\LanguagesInstalled\\" + languageCode
                languageSubDir = GetRegistryKeyValue( maxKeyName, "LangSubDir", "" )
                if( languageSubDir == "" ):
                    LogWarning( "Language sub directory could not be found in registy. Defaulting to en-US (English)" )
                    languageSubDir = "en-US"

            self.LanguageCodeStr = languageCode
            self.LanguageSubDir = languageSubDir

            LogInfo( "Language code string: " + self.LanguageCodeStr )
            LogInfo( "Language sub directory: " + self.LanguageSubDir )

        # Read in the Fail On Existing Max Process setting, which can be overridden in the plugin info file.
        if( GetBooleanPluginInfoEntryWithDefault( "OverrideFailOnExistingMaxProcess", False ) ):
            self.FailOnExistingMaxProcess = GetBooleanPluginInfoEntryWithDefault( "FailOnExistingMaxProcess", True )
            LogInfo( "Fail on existing 3dsmax process: %d (from plugin info file override)" % self.FailOnExistingMaxProcess )
        else:
            self.FailOnExistingMaxProcess = GetBooleanConfigEntryWithDefault( "FailOnExistingMaxProcess", True )
            LogInfo( "Fail on existing 3dsmax process: %d" % self.FailOnExistingMaxProcess )

        # Get timeouts.
        self.LoadMaxTimeout = GetIntegerConfigEntryWithDefault( "LoadMaxTimeout", 1000 )
        LogInfo( "Load 3dsmax timeout: %d seconds" % self.LoadMaxTimeout )
        self.StartJobTimeout = GetIntegerConfigEntryWithDefault( "StartJobTimeout", 1000 )
        LogInfo( "Start job timeout: %d seconds" % self.StartJobTimeout )
        self.ProgressUpdateTimeout = GetIntegerConfigEntryWithDefault( "ProgressUpdateTimeout", 8000 )
        LogInfo( "Progress update timeout: %d seconds" % self.ProgressUpdateTimeout )
        self.DisableProgressUpdateTimeout = GetBooleanPluginInfoEntryWithDefault( "DisableProgressUpdateTimeout", False )
        LogInfo( "Progress update timout disabled: %d" % self.DisableProgressUpdateTimeout )

        # Read in Slave Mode setting.
        self.UseSlaveMode = GetBooleanPluginInfoEntryWithDefault( "UseSlaveMode", True )
        LogInfo( "Slave mode enabled: %d" % self.UseSlaveMode )

        # Read in Silent Mode setting.
        self.UseSilentMode = GetBooleanPluginInfoEntryWithDefault( "UseSilentMode", False )
        LogInfo( "Silent mode enabled: %d" % self.UseSilentMode )

        # Read in Local Rendering setting.
        #self.LocalRendering = GetBooleanConfigEntryWithDefault( "LocalRendering", True )
        self.LocalRendering = GetBooleanPluginInfoEntryWithDefault( "LocalRendering", True )
        LogInfo( "Local rendering enabled: %d" % self.LocalRendering )

        # Read in the Strict Max Check setting.
        #self.StrictMaxCheck = GetBooleanConfigEntryWithDefault( "StrictMaxCheck", True )
        #LogInfo( "Strict 3dsmax check enabled: %d" % self.StrictMaxCheck )
        # Disabling this because it's really not necessary anymore, and the 3dsmaxcmd.exe check is good enough at telling us if there is a problem with the 3dsmax/backburner installation
        self.StrictMaxCheck = False

        maxInstallPath = Path.GetDirectoryName( self.MaxRenderExecutable )

        # Now verify the render executable and max installation if necessary.
        if( self.StrictMaxCheck ):
            LogInfo( "    Verifying installation of 3dsmax %d at %s" % (self.Version, maxInstallPath) )

            coreDll = ChangeFilename( self.MaxRenderExecutable, "core.dll" )
            versionString = GetExecutableVersion( coreDll )
            if( versionString == "unknown" ):
                FailRender( "Could not retrieve the version of 3dsmax from %s" % coreDll )
            LogInfo( "    Version of core.dll: " + versionString )

            version = str(self.Version)
            if( self.Version > 9 ):
                intVersion = self.Version - 1998
                version = str(intVersion)

            periodIndex = versionString.find( "." )
            if( versionString[0:periodIndex] != version ):
                FailRender( "Incorrect version of 3dsmax at %s - expected %d but got %s" % (maxInstallPath, self.Version, versionString) )

            LogInfo( "    Ensuring 3dsmax install is valid" )
            self.Ensure3dsmaxValid( maxInstallPath )

        self.EnsureNecessaryDlls( maxInstallPath )

        # This is sometimes neccessary for 3dsmax to run on a fresh install.
        if( GetBooleanConfigEntryWithDefault( "RunCmdWorkaround", True ) ):
            LogInfo( "Running render sanity check using 3dsmaxcmd.exe" )
            self.Hack3dsmaxCommand()

        # Set 3dsmax startup filename.
        self.MaxStartupFile = Path.Combine( GetPluginDirectory(), "deadlineStartupMax" + str(self.Version) + ".max" )
        if( not File.Exists( self.MaxStartupFile ) ):
            FailRender( "The 3dsmax start up file %s does not exist" % self.MaxStartupFile )
        if( not File.Exists( Path.ChangeExtension( self.MaxStartupFile, ".xml" ) ) ):
            FailRender( "The Backburner xml job file %s does not exist" % Path.ChangeExtension( self.MaxStartupFile, ".xml" ) )
        LogInfo( "3dsmax start up file: %s" % self.MaxStartupFile )

        # Some extra stuff if we are running version 9 or later.
        if( self.Version >= 9 ):
            # Check if we are running the 64 bit version.
            self.Is64Bit = Is64BitDllOrExe( self.MaxRenderExecutable )

            # Figure out if we are using user profiles or not (version 9 or later).
            installIni = ChangeFilename( self.MaxRenderExecutable, "InstallSettings.ini" )
            useUserProfiles = GetIniFileSetting( installIni, "Least User Privilege", "useUserProfiles", "1" )
            if( useUserProfiles == "1" ):
                self.UseUserProfiles = True
        LogInfo( "Using user profiles: %d" % self.UseUserProfiles )

        # Set 3dsmax Ini file, and figure out the network log path.
        networkLogFilePath = ""
        if( self.Version >= 2008 and self.UseUserProfiles ):
            if self.Version < 2010 or not self.IsMaxDesign:
                # In max 2008 and later, the 3dsmax.ini file is in the user profile directory if UseUserProfiles is enabled.
                if( self.Is64Bit ):
                    self.UserProfileDataPath = Path.Combine( GetLocalApplicationDataPath(), "Autodesk\\3dsmax\\" + str(self.Version) + " - 64bit\\" + self.LanguageCodeStr )
                else:
                    self.UserProfileDataPath = Path.Combine( GetLocalApplicationDataPath(), "Autodesk\\3dsmax\\" + str(self.Version) + " - 32bit\\" + self.LanguageCodeStr )
            else:
                # In max 2010, the user profile directory is different for the Design version
                if( self.Is64Bit ):
                    self.UserProfileDataPath = Path.Combine( GetLocalApplicationDataPath(), "Autodesk\\3dsMaxDesign\\" + str(self.Version) + " - 64bit\\" + self.LanguageCodeStr )
                else:
                    self.UserProfileDataPath = Path.Combine( GetLocalApplicationDataPath(), "Autodesk\\3dsMaxDesign\\" + str(self.Version) + " - 32bit\\" + self.LanguageCodeStr )

            LogInfo( "3dsmax user profile path: %s" % self.UserProfileDataPath )
            self.MaxIni = Path.Combine( self.UserProfileDataPath, "3dsmax.ini" )
            self.MaxDataPath = GetIniFileSetting( self.MaxIni, "Directories", "MaxData", self.UserProfileDataPath )
            networkLogFilePath = Path.Combine( self.UserProfileDataPath, "Network" ) # This isn't based on the MaxData path.
        else:
            self.MaxIni = ChangeFilename( self.MaxRenderExecutable, "3dsmax.ini" )
            maxRootPath = Path.GetDirectoryName( self.MaxRenderExecutable )
            self.MaxDataPath = GetIniFileSetting( self.MaxIni, "Directories", "MaxData", maxRootPath )
            networkLogFilePath = Path.Combine( maxRootPath, "Network" ) # This isn't based on the MaxData path.

        LogInfo( "3dsmax data path: %s" % self.MaxDataPath )
        if( File.Exists( self.MaxIni ) ):
            LogInfo( "3dsmax ini file: %s" % self.MaxIni )
        else:
            LogWarning( "3dsmax ini file does not exist: %s" % self.MaxIni )

        # Create the network log path if it doesn't exist.
        if( not Directory.Exists( networkLogFilePath ) ):
            LogInfo( "Creating network log path: %s" % networkLogFilePath )
            Directory.CreateDirectory( networkLogFilePath )

        self.NetworkLogFile = Path.Combine( networkLogFilePath, "Max.log" )
        LogInfo( "Network log file: %s" % self.NetworkLogFile )

        # Figure out which plugin ini file to use. First check if there is an override in the plugin info file.





        ### get the custom plugin config
        plugins_json = GetJobInfoEntry(MxDLJob.hPlugins)
        ctx = plugins.mxConfigMaxPlugin(plugins_json)

        self.ini_file_path = tempfile.gettempdir() + '/' + '__mx_max_plugin.ini'
        ini_fp = open(self.ini_file_path, 'w')

        try:
            ini_fp.write(ctx)
        finally:
            ini_fp.close()







        self.MaxPluginIni = self.ini_file_path #GetPluginInfoEntryWithDefault( "OverridePluginIni", ini_file_path )
        if( self.MaxPluginIni != "" ):
            self.MaxPluginIni = Path.Combine( GetPluginDirectory(), self.MaxPluginIni )
            if( not File.Exists( self.MaxPluginIni ) ):
                FailRender( "The alternative plugin ini file %s does not exist" % self.MaxPluginIni )
            self.UsingCustomPluginIni = True
        else:
            # Now check if there is an override in the plugin config file.
            self.MaxPluginIni = GetConfigEntryWithDefault( "AlternatePluginIni", "" )
            if( self.MaxPluginIni != "" ):
                self.UsingCustomPluginIni = True
            else:
                # Now simply use the plugin ini file in the 3dsmax directory.
                if self.Version < 2013:
                    self.MaxPluginIni = Path.Combine( Path.GetDirectoryName( self.MaxRenderExecutable ), "pluginnetrender.ini" )
                    if( not File.Exists( self.MaxPluginIni ) ):
                        self.MaxPluginIni = Path.Combine( Path.GetDirectoryName( self.MaxRenderExecutable ), "plugin.ini" )
                        if( not File.Exists( self.MaxPluginIni ) ):
                            FailRender( "The plugin ini file %s does not exist, and no alternative plugin.ini file was provided" % self.MaxPluginIni )
                else:
                    self.MaxPluginIni = Path.Combine( Path.Combine( Path.GetDirectoryName( self.MaxRenderExecutable ), self.LanguageSubDir ), "pluginnetrender.ini" )
                    if( not File.Exists( self.MaxPluginIni ) ):
                        self.MaxPluginIni =  Path.Combine( Path.Combine( Path.GetDirectoryName( self.MaxRenderExecutable ), self.LanguageSubDir ), "plugin.ini" )
                        if( not File.Exists( self.MaxPluginIni ) ):
                            FailRender( "The plugin ini file %s does not exist, and no alternative plugin.ini file was provided" % self.MaxPluginIni )

        LogInfo( "Plugin ini file: %s" % self.MaxPluginIni )

        if self.UsingCustomPluginIni:
            LogInfo( "Not including user profile plugin.ini because a custom plugin.ini is being used" )
        else:
            # Check if we have to use a user profile ini file as well.
            if( self.UseUserProfiles ):
                # This is based on the MaxData path.
                self.UserPluginIni = Path.Combine( self.MaxDataPath, "pluginnetrender.ini" )
                if( not File.Exists( self.UserPluginIni ) ):
                    self.UserPluginIni = Path.Combine( self.MaxDataPath, "plugin.ini" )
                    if( not File.Exists( self.UserPluginIni ) ):
                        self.UserPluginIni = Path.Combine( self.MaxDataPath, "Plugin.UserSettings.ini" )

                if( File.Exists( self.UserPluginIni ) ):
                    LogInfo( "Including user profile plugin ini: %s" % self.UserPluginIni )
                else:
                    LogInfo( "Not including user profile plugin ini because it does not exist: %s" % self.UserPluginIni )
            else:
                self.UserPluginIni = Path.Combine( self.MaxDataPath, "Plugin.UserSettings.ini" )
                if( File.Exists( self.UserPluginIni ) ):
                    LogInfo( "Including user profile plugin ini: %s" % self.UserPluginIni )
                else:
                    LogInfo( "Not including user profile plugin ini because it does not exist: %s" % self.UserPluginIni )

        # Determine lightning plugin location.
        if( self.Is64Bit ):
            self.LightningPluginFile = Path.Combine( GetPluginDirectory(), "lightning64Max" + str(self.Version) + ".dlx" )
        else:
            self.LightningPluginFile = Path.Combine( GetPluginDirectory(), "lightningMax" + str(self.Version) + ".dlx" )
        if( not File.Exists( self.LightningPluginFile ) ):
            FailRender( "Lightning connection plugin %s does not exist" % self.LightningPluginFile )
        LogInfo( "Lightning connection plugin: %s" % self.LightningPluginFile )

        # Check if bitmap pager setting should be overwritten. This is handled in the 3dsmax.ini file in 2009 and earlier.
        if( self.Version < 2010 ):
            self.OverrideBitmapPager = GetBooleanPluginInfoEntryWithDefault( "OverrideBitmapPager", False )
            if( self.OverrideBitmapPager ):
                self.OriginalBitmapPager = GetIniFileSetting( self.MaxIni, "Performance", "BitmapPager", "0" )
                LogInfo( "Original bitmap pager was: %s" % self.OriginalBitmapPager )
                if GetBooleanPluginInfoEntryWithDefault( "BitmapPager", False ):
                    SetIniFileSetting( self.MaxIni, "Performance", "BitmapPager", "1" )
                else:
                    SetIniFileSetting( self.MaxIni, "Performance", "BitmapPager", "0" )
                LogInfo( "New bitmap pager is: %s" % GetIniFileSetting( self.MaxIni, "Performance", "BitmapPager", "0" ) )

    # This starts up 3dsmax with our custom plugin ini file, and initially loads the 3dsmax startup scene file,
    # which contains a maxscript callback that runs a startup script which connects 3dsmax to our listening MaxSocket.
    def StartMax( self ):
        if( self.FailOnExistingMaxProcess ):
            processName = Path.GetFileNameWithoutExtension( self.MaxRenderExecutable )
            if( IsProcessRunning( processName ) ):
                LogWarning( "Found existing %s process" % processName )
                process = Process.GetProcessesByName( processName )[ 0 ]
                FailRender( "FailOnExistingMaxProcess is enabled, and a process %s with pid %d exists - shut down this copy of 3dsmax to enable network rendering on this machine" % (processName, process.Id) )

        # Reset where we're at in the 3dsmax network log.
        self.NetworkLogStart()

        # The LoadSaveSceneScripts setting is what runs the callback script we need to initiate the connection to 3dsmax.
        if( GetIniFileSetting( self.MaxIni, "MAXScript", "LoadSaveSceneScripts", "1" ) != "1" ):
            FailRender( "3dsmax.ini setting in [MAXScript], LoadSaveSceneScripts, is disabled - it must be enabled for deadline to render with 3dsmax" )

        # Initialize the listening socket.
        self.MaxSocket = ListeningSocket()
        self.MaxSocket.StartListening( 0, True, True, 10 )
        if( not self.MaxSocket.IsListening ):
            FailRender( "Failed to open a port for listening to the lightning max daemon" )
        else:
            LogInfo( "3dsmax socket connection port: %d" % self.MaxSocket.Port )

        # Create the startup script which is executed by the callback in the 3dsmax startup scene file.
        self.CreateStartupScript( self.MaxSocket.Port )

        # Create our custom plugin ini file, which includes the location of our lightning plugin.
        lightningDir = self.CopyLightningDlx()
        pluginIni = self.CreatePluginInis( lightningDir )

        # This is a workaround for 3dsmax 2010 where the ini file passed to the command line needs to be in the 3dsmax install root folder.
        if self.Version >= 2010 and self.Version < 2013:
            # This is based on the MaxData path.
            LogInfo( "Copying " + Path.GetFileName( pluginIni ) + " to 3dsmax data path: " + self.MaxDataPath )
            LogInfo( "If this fails, make sure that the necessary permissions are set on this folder to allow for this copy to take place" )

            tempPluginIni = pluginIni
            newPluginIni = Path.Combine( self.MaxDataPath, Path.GetFileName( pluginIni ) )

            File.Copy( tempPluginIni, newPluginIni, True )
            pluginIni = Path.GetFileName( pluginIni )

        # Setup the command line parameters, and then start max.
        #parameters = "-p \"" + pluginIni + "\" -q"
        parameters = "-p \"" + self.ini_file_path + "\" -q"
        if( self.UseSlaveMode ):
            parameters = parameters + " -s"
        if( self.UseSilentMode ):
            parameters = parameters + " -silent"

        parameters = parameters + " \"" + self.MaxStartupFile + "\""
        self.LaunchMax( self.MaxRenderExecutable, parameters, Path.GetDirectoryName( self.MaxRenderExecutable ) )

        # Wait for max to connect to us.
        LogInfo( "Waiting for connection from 3dsmax" )
        self.WaitForConnection( "3dsmax startup" )

        # Now get the version of lightning that we're connected to.
        self.MaxSocket.Send( "DeadlineStartup" )
        try:
            version = self.MaxSocket.Receive( 500 )
            if( version.startswith( "ERROR: " ) ):
                FailRender( "Error occurred while getting version from lightning: %s" % version[7:] )
            LogInfo( "Connected to 3dsmax plugin version %s" % version )
        except SimpleSocketTimeoutException:
            FailRender( "Timed out waiting for the lightning version to be sent - check the install of lightningMax.dlx\n%s" % self.NetworkLogGet() )

    # This tells 3dsmax to load the 3dsmax scene file we want to render. It also configures some other render settings.
    def StartMaxJob( self ):
        # Reset where we're at in the 3dsmax network log.
        self.NetworkLogStart()

        # Get the 3dsmax scene file to render.
        self.MaxFilename = GetPluginInfoEntryWithDefault( "SceneFile", GetDataFilename() )
        if( self.MaxFilename == None ):
            FailRender( "No scene file was included with the job" )
        LogInfo( "Scene file to render: \"%s\"" % self.MaxFilename )

        # Get the camers.
        self.Camera = GetPluginInfoEntryWithDefault( "Camera", "" )
        if( self.Camera == "" ):
            LogInfo( "Camera: no camera specified, rendering active viewport" )
        else:
            LogInfo( "Camera: \"%s\"" % self.Camera )

        # Get the list of auxiliary filenames.
        self.AuxiliaryFilenames = GetAuxiliaryFilenames()

        # Check if we are rendering a maxscript job.
        self.MaxScriptJob = GetBooleanPluginInfoEntryWithDefault( "MAXScriptJob", False )
        if( self.MaxScriptJob ):
            LogInfo( "This is a MAXScript Job" )

            #if( len( self.AuxiliaryFilenames ) == 1 ):
            #    FailRender( "A MAXScript job must include a script to execute" )
            #self.MaxScriptJobScript = self.AuxiliaryFilenames[ 1 ]

            if GetPluginInfoEntryWithDefault( "SceneFile", "" ) == "":
                # Scene file submitted with job, so maxscript will be the second auxiliary file.
                if( len( self.AuxiliaryFilenames ) == 1 ):
                    FailRender( "A MAXScript job must include a script to execute" )
                self.MaxScriptJobScript = self.AuxiliaryFilenames[ 1 ]
            else:
                # Scene file not submitted with job, so maxscript will be the first auxiliary file
                if( len( self.AuxiliaryFilenames ) == 0 ):
                    FailRender( "A MAXScript job must include a script to execute" )
                self.MaxScriptJobScript = self.AuxiliaryFilenames[ 0 ]

            if( not File.Exists( self.MaxScriptJobScript ) ):
                FailRender( "The maxscript submitted with the MAXScript job, \"%s\", does not exist" % self.MaxScriptJobScript )
            LogInfo( "MAXScript to be executed: \"%s\"" % self.MaxScriptJobScript )

        # Get the RestartEachFrame setting.
        self.RestartEachFrame = GetBooleanPluginInfoEntryWithDefault( "RestartRendererMode", False )
        LogInfo( "Restarting renderer after each frame: %d" % self.RestartEachFrame )

        # Get the ShowFrameBuffer setting.
        self.ShowFrameBuffer = GetBooleanPluginInfoEntryWithDefault( "ShowFrameBuffer", True )
        LogInfo( "Showing frame buffer: %d" % self.ShowFrameBuffer )

        # Get the single job region rendering setting.
        self.RegionRenderingSingleJob = GetBooleanPluginInfoEntryWithDefault( "RegionSingleJob", False )
        self.RegionRenderingSingleFrame = str(GetStartFrame())

        # Get the RenderOutputOverride setting.
        if self.RegionRenderingSingleJob:
            self.RenderOutputOverride = GetPluginInfoEntryWithDefault( "RegionFilename" + self.RegionRenderingSingleFrame, "" ).strip()
        else:
            self.RenderOutputOverride = GetPluginInfoEntryWithDefault( "RenderOutput", "" ).strip()

        if( self.RenderOutputOverride != "" ):
            self.RedirectOutput = True
            LogInfo( "Overriding render output: \"%s\"" % self.RenderOutputOverride )

        # Get the OverrideSaveFile setting.
        if GetBooleanPluginInfoEntryWithDefault( "SaveFile", True ) == GetBooleanPluginInfoEntryWithDefault( "SaveFile", False ):
            self.OverrideSaveFile = True
            self.SaveFile = GetBooleanPluginInfoEntry( "SaveFile" )
            LogInfo( "Overriding save file option: %s" % self.SaveFile )

        # Get the FrameNumberBase setting.
        self.FrameNumberBase = GetIntegerPluginInfoEntryWithDefault( "FrameNumberBase", 0 )
        LogInfo( "Frame number base: %d" % self.FrameNumberBase )

        # Get the RemovePadding setting.
        self.RemovePadding = GetBooleanPluginInfoEntryWithDefault( "RemovePadding", False )
        LogInfo( "Remove padding from output filename: %d" % self.RemovePadding )

        # Get the IgnoreMissingExternalFiles setting.
        self.IgnoreMissingExternalFiles = GetBooleanPluginInfoEntryWithDefault( "IgnoreMissingExternalFiles", True )
        LogInfo( "Ignore missing external file errors: %d" % self.IgnoreMissingExternalFiles )

        # Get the IgnoreMissingUVWs setting.
        self.IgnoreMissingUVWs = GetBooleanPluginInfoEntryWithDefault( "IgnoreMissingUVWs", True )
        LogInfo( "Ignore missing UVW errors: %d" % self.IgnoreMissingUVWs )

        # Get the IgnoreMissingXREFs setting.
        self.IgnoreMissingXREFs = GetBooleanPluginInfoEntryWithDefault( "IgnoreMissingXREFs", True )
        LogInfo( "Ignore missing XREF errors: %d" % self.IgnoreMissingXREFs )

        # Get the IgnoreMissingDLLs setting.
        self.IgnoreMissingDLLs = GetBooleanPluginInfoEntryWithDefault( "IgnoreMissingDLLs", False )
        LogInfo( "Ignore missing DLL errors: %d" % self.IgnoreMissingDLLs )

        # Get the DisableMultipass setting.
        self.DisableMultipass = GetBooleanPluginInfoEntryWithDefault( "DisableMultipass", False )
        LogInfo( "Disabling Multipass: %d" % self.DisableMultipass )

        # Get the Ignore Render Elements By Name setting.
        self.IgnoreRenderElementsByName = []
        elementIndex = 0
        while True:
            elementToIgnore = GetPluginInfoEntryWithDefault( "IgnoreRenderElementsByName" + str(elementIndex), "" )
            if elementToIgnore != "":
                self.IgnoreRenderElementsByName.append( elementToIgnore )
            else:
                break
            elementIndex = elementIndex + 1

        # Get the Ignore Render Elements setting.
        self.IgnoreRenderElements = GetBooleanPluginInfoEntryWithDefault( "IgnoreRenderElements", False )
        if( self.IgnoreRenderElements ):
            LogInfo( "Render elements will not be saved" )
        elif( len(self.IgnoreRenderElementsByName) > 0 ):
            LogInfo( "The following elements will not be saved" )
            for elementToIgnore in self.IgnoreRenderElementsByName:
                LogInfo( "   " + elementToIgnore )

        # Get the UseJpegOutput settings.
        self.UseJpegOutput = GetBooleanPluginInfoEntryWithDefault( "UseJpegOutput", False )
        self.JpegOutputPath = GetPluginInfoEntryWithDefault( "JpegOutputPath", "" ).strip()
        if( self.UseJpegOutput and self.JpegOutputPath == "" ):
            self.UseJpegOutput = False
            LogWarning( "Disabling saving of jpeg output because JpegOutputPath was not specified" )
        if( self.UseJpegOutput ):
            if( not self.JpegOutputPath.endswith( "\\" ) or not self.JpegOutputPath.endswith( "/" ) ):
                self.JpegOutputPath = self.JpegOutputPath + "\\"
            LogInfo( "Saving jpeg copy of original output file to: \"%s\"" % self.JpegOutputPath )

        # Get the FailOnBlackFrames settings.
        self.FailOnBlackFrames = GetBooleanPluginInfoEntryWithDefault( "FailOnBlackFrames", False )
        self.BlackFramesCheckRenderElements = GetBooleanPluginInfoEntryWithDefault( "BlackFramesCheckRenderElements", False )
        self.BlackPixelPercentage = GetIntegerPluginInfoEntryWithDefault( "BlackPixelPercentage", 0 )
        self.BlackPixelThreshold = GetFloatPluginInfoEntryWithDefault( "BlackPixelThreshold", 0.0 )
        if( self.FailOnBlackFrames ):
            if( self.BlackPixelPercentage < 0 ):
                self.BlackPixelPercentage = 0
            if( self.BlackPixelPercentage > 100 ):
                self.BlackPixelPercentage = 100
            if( self.BlackPixelThreshold < 0.0 ):
                self.BlackPixelThreshold = 0.0
            if( self.BlackPixelThreshold > 1.0 ):
                self.BlackPixelThreshold = 1.0
            LogInfo( "Fail on black frames enabled: black pixel percentage = %d, black pixel threshold = %.4f, check render elements = %s" % (self.BlackPixelPercentage, self.BlackPixelThreshold,self.BlackFramesCheckRenderElements) )

        # Get the RegionRendering settings.
        self.RegionRendering = GetBooleanPluginInfoEntryWithDefault( "RegionRendering", False )
        self.RegionPadding = GetIntegerPluginInfoEntryWithDefault( "RegionPadding", 0 )
        self.RegionType = GetPluginInfoEntryWithDefault( "RegionType", "CROP" ).upper()

        if self.RegionRenderingSingleJob:
            self.RegionLeft = GetIntegerPluginInfoEntryWithDefault( "RegionLeft" + self.RegionRenderingSingleFrame, 0 )
            self.RegionTop = GetIntegerPluginInfoEntryWithDefault( "RegionTop" + self.RegionRenderingSingleFrame, 0 )
            self.RegionRight = GetIntegerPluginInfoEntryWithDefault( "RegionRight" + self.RegionRenderingSingleFrame, 0 )
            self.RegionBottom = GetIntegerPluginInfoEntryWithDefault( "RegionBottom" + self.RegionRenderingSingleFrame, 0 )
        else:
            self.RegionLeft = GetIntegerPluginInfoEntryWithDefault( "RegionLeft", 0 )
            self.RegionTop = GetIntegerPluginInfoEntryWithDefault( "RegionTop", 0 )
            self.RegionRight = GetIntegerPluginInfoEntryWithDefault( "RegionRight", 0 )
            self.RegionBottom = GetIntegerPluginInfoEntryWithDefault( "RegionBottom", 0 )

        if( self.RegionRendering ):
            LogInfo( "Region rendering enabled: left = %d, top = %d, right = %d, bottom = %d, padding = %d, type = %s" % (self.RegionLeft, self.RegionTop, self.RegionRight, self.RegionBottom, self.RegionPadding, self.RegionType) )

        # Get the PreFrameScript setting.
        self.PreFrameScript = GetPluginInfoEntryWithDefault( "PreFrameScript", "" ).strip().strip( "\"" )
        if( self.PreFrameScript != "" ):
            if( not Path.IsPathRooted( self.PreFrameScript ) ):
                self.PreFrameScript = Path.Combine( GetJobsDataDirectory(), self.PreFrameScript )
            LogInfo( "Pre frame script: \"%s\"" % self.PreFrameScript )

        # Get the PostFrameScript setting.
        self.PostFrameScript = GetPluginInfoEntryWithDefault( "PostFrameScript", "" ).strip().strip( "\"" )
        if( self.PostFrameScript != "" ):
            if( not Path.IsPathRooted( self.PostFrameScript ) ):
                self.PostFrameScript = Path.Combine( GetJobsDataDirectory(), self.PostFrameScript )
            LogInfo( "Post frame script: \"%s\"" % self.PostFrameScript )

        # Get the PreLoadScript setting.
        self.PreLoadScript = GetPluginInfoEntryWithDefault( "PreLoadScript", "" ).strip().strip( "\"" )
        if( self.PreLoadScript != "" ):
            if( not Path.IsPathRooted( self.PreLoadScript ) ):
                self.PreLoadScript = Path.Combine( GetJobsDataDirectory(), self.PreLoadScript )
            LogInfo( "Pre load script: \"%s\"" % self.PreLoadScript )

        # Get the PostLoadScript setting.
        self.PostLoadScript = GetPluginInfoEntryWithDefault( "PostLoadScript", "" ).strip().strip( "\"" )
        if( self.PostLoadScript != "" ):
            if( not Path.IsPathRooted( self.PostLoadScript ) ):
                self.PostLoadScript = Path.Combine( GetJobsDataDirectory(), self.PostLoadScript )
            LogInfo( "Post load script: \"%s\"" % self.PostLoadScript )

        # Get the Path Configuration File setting.
        self.PathConfigFile = GetPluginInfoEntryWithDefault( "PathConfigFile", "" ).strip().strip( "\"" )
        if( self.PathConfigFile != "" ):
            if( not Path.IsPathRooted( self.PathConfigFile ) ):
                self.PathConfigFile = Path.Combine( GetJobsDataDirectory(), self.PathConfigFile )
            LogInfo( "Path configuration file: \"%s\"" % self.PathConfigFile )

            self.MergePathConfigFile = GetBooleanPluginInfoEntryWithDefault( "MergePathConfigFile", False )
            LogInfo( "Merge configuration file: %d" % self.MergePathConfigFile )

        # Execute pre load script if it is specified.
        if( self.PreLoadScript != "" ):
            self.ExecuteScript( 0, self.PreLoadScript, False )

        # Load the max file.
        self.LoadMaxFile()

        # Execute post load script if it is specified.
        if( self.PostLoadScript != "" ):
            self.ExecuteScript( 0, self.PostLoadScript, False )

    # This renders the current task.
    def RenderTasks( self ):
        if self.RegionRenderingSingleJob:
            self.StartFrame = GetIntegerPluginInfoEntryWithDefault( "RegionSingleFrame", 0 )
            self.EndFrame = GetIntegerPluginInfoEntryWithDefault( "RegionSingleFrame", 0 )
        else:
            self.StartFrame = GetStartFrame()
            self.EndFrame = GetEndFrame()

        VerifyMonitoredManagedProcess( self.ProgramName )

        SetProgress( 0.0 )
        self.stopwatch = Stopwatch()
        self.stopwatch.Start()

        denominator = self.EndFrame - self.StartFrame + 1.0
        for frame in range( self.StartFrame, self.EndFrame + 1 ):
            self.RenderFrame( frame )

            if denominator != 0:
                progress = ( (frame - self.StartFrame + 1.0) / denominator ) * 100.0
                SetProgress( progress )
                #SetProgress( ( (frame - self.StartFrame + 1) / denominator ) * 100.0 )

            VerifyMonitoredManagedProcess( self.ProgramName )

        SetProgress( 100.0 )

        ## send cost request
        self.stopwatch.Stop()

        r_time = self.stopwatch.Elapsed.TotalHours

        sm = SlaveManager.GetInstance()
        cpus = sm.Slave.CPUs
        task_id = sm.Slave.CurrentTasksString
        job = sm.Slave.CurrentJob

        cb = callback.MxDLTaskFinishCallback(job, task_id, cpus, r_time)
        LogInfo(cb.send())

    # This tells 3dsmax to unload the current scene file.
    def EndMaxJob( self ):
        if( not MonitoredManagedProcessIsRunning( self.ProgramName ) ):
            LogWarning( "3dsmax.exe was shut down before the proper shut down sequence" )
        else:
            response = ""

            # If an error occurs while sending EndJob, set the response so that we don't enter the while loop below.
            try:
                self.MaxSocket.Send( "EndJob" )
            except Exception, e:
                response = ( "ERROR: Error sending EndJob command: %s" % e.Message )

            countdown = 5000
            while( countdown > 0 and response == "" ):
                try:
                    countdown = countdown - 100
                    response = self.MaxSocket.Receive( 100 )

                    # If this is a STDOUT message, print it out and reset 'response' so that we keep looping
                    match = self.StdoutRegex.Match( response )
                    if( match.Success ):
                        LogInfo( match.Groups[ 1 ].Value )
                        response = ""

                    # If this is a WARN message, print it out and reset 'response' so that we keep looping
                    match = self.WarnRegex.Match( response )
                    if( match.Success ):
                        LogWarning( match.Groups[ 1 ].Value )
                        response = ""

                except Exception, e:
                    if( not isinstance( e, SimpleSocketTimeoutException ) ):
                        response = ( "ERROR: Error when waiting for renderer to close: %s" % e.Message )

            if( response == "" ):
                LogWarning( "Timed out waiting for the renderer to close." )

            if( response.startswith( "ERROR: " ) ):
                LogWarning( response[7:] )

            if( not response.startswith( "SUCCESS" ) ):
                LogWarning( "Did not receive a success message in response to EndJob: %s" % response )

            #~ try:
                #~ self.MaxSocket.Send( "EndJob" )
                #~ response = self.MaxSocket.Receive( 5000 )
                #~ if( response.startswith( "ERROR: " ) ):
                    #~ LogWarning( response[7:] )
                #~ elif( not response.startswith( "SUCCESS" ) ):
                    #~ LogWarning( "Did not receive a success message in response to EndJob: %s" % response )
            #~ except Exception, e:
                #~ if( isinstance( e, SimpleSocketTimeoutException ) ):
                    #~ LogWarning( "Timed out waiting for the renderer to close." )
                #~ else:
                    #~ LogWarning( "Error when waiting for renderer to close: %s" % e.Message )

    # This disconnects the socket connection with 3dsmax, and then shuts 3dsmax down.
    def ShutdownMax( self ):
        self.DeletePluginInis()

        LogInfo( "Disconnecting socket connection to 3dsmax" )
        try:
            self.MaxSocket.Disconnect( True )
        except Exception, e:
            LogWarning( "Error disconnecting socket connection to 3dsmax: %s" % e.Message )

        if( not MonitoredManagedProcessIsRunning( self.ProgramName ) ):
            LogWarning( "The 3ds max process has already quit" )
        else:
            #processIDs = GetMonitoredManagedProcessIDs( self.ProgramName )
            #LogInfo( "3dsmax process has %d objects" % len( processIDs ) )

            LogInfo( "Waiting for 3dsmax to shut down" )
            ShutdownMonitoredManagedProcess( self.ProgramName )
            #Sleep( 10000 )

            #LogInfo( "Terminating 3dsmax child processes" )
            #for id in processIDs:
            #    KillParentAndChildProcesses( id )

            #ShutdownMonitoredManagedProcess( self.ProgramName )

            LogInfo( "3dsmax has shut down" )

    # This resets the bitmap pager setting.
    def ResetBitmapPager( self ):
        if( self.Version < 2010 ):
            if( self.OverrideBitmapPager ):
                SetIniFileSetting( self.MaxIni, "Performance", "BitmapPager", self.OriginalBitmapPager )


    ########################################################################
    ## Helper functions
    ########################################################################
    def AutoCheckRegistryForLanguage( self, installDirectory, keyName, languageCode, autoDetectedLanguageCode ):
        if( autoDetectedLanguageCode == "" ):
            tempInstallDir = GetRegistryKeyValue( keyName + languageCode, "Installdir", "" )
            if( tempInstallDir != "" ):
                tempInstallDir = tempInstallDir.lower().replace( "/", "\\" ).rstrip( "\\" )
                if( tempInstallDir == installDirectory.lower().replace( "/", "\\" ).rstrip( "\\" ) ):
                    autoDetectedLanguageCode = languageCode
        return autoDetectedLanguageCode

    def AutoCheckRegistry( self, filenameOnly, keyName, valueName, autoDetectedDirectory ):
        if( autoDetectedDirectory == "" ):
            tempDirectory = GetRegistryKeyValue( keyName, valueName, "" )
            if( tempDirectory != "" and File.Exists( Path.Combine( tempDirectory, filenameOnly ) ) ):
                autoDetectedDirectory = tempDirectory
        return autoDetectedDirectory

    def AutoCheckFile( self, file, autoDetectedDirectory ):
        if( autoDetectedDirectory == "" ):
            if( File.Exists( file ) ):
                autoDetectedDirectory = Path.GetDirectoryName( file )
        return autoDetectedDirectory

    def Ensure3dsmaxValid( self, maxInstallPath ):
        #if( self.Version == 8 ):
        #    # This file should be in C:\Program Files\Common Files\Autodesk Shared. 3dsmax.exe fails to load if this is missing
        #    if( SearchPath( "acge16.dll" ) == "" ):
        #        LogWarning( "    'Autodesk Shared' does not appear to be in the path, attempting to correct this" )
        #        AddToPath( "c:\\Program Files\\Common Files\\Autodesk Shared" )
        #        AddToPath( "c:\\Program Files (x86)\\Common Files\\Autodesk Shared" )
        #        if( SearchPath( "acge16.dll" ) == "" ):
        #            FailRender( "3dsmax does not appear to have been installed correctly at %s - this may indicate that 3dsmax was copied without using the installer" % maxInstallPath )

        # Make sure backburner is in the path.
        if( SearchPath( "nrapi20.dll" ) == "" ):
            LogWarning( "    Backburner does not appear to be in the path, attempting to correct this" )

            backburnerInstallDir = ""
            backburnerInstallDir = self.AutoCheckRegistry( "nrapi20.dll", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Discreet\\Backburner\\2.0", "InstallDir", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckRegistry( "nrapi20.dll", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Discreet\\Backburner\\3.0", "InstallDir", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckRegistry( "nrapi20.dll", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\Backburner\\2007.0", "InstallDir", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckRegistry( "nrapi20.dll", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\Backburner\\2008.1", "InstallDir", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckRegistry( "nrapi20.dll", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\Backburner\\2008.2", "InstallDir", backburnerInstallDir )

            #backburnerInstallDir = self.AutoCheckFile( "c:\\3dsmax61\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "d:\\3dsmax61\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "e:\\3dsmax61\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "c:\\3dsmax6\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "d:\\3dsmax6\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "e:\\3dsmax6\\backburner2\\nrapi20.dll", backburnerInstallDir )

            #backburnerInstallDir = self.AutoCheckFile( "c:\\3dsmax51\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "d:\\3dsmax51\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "e:\\3dsmax51\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "c:\\3dsmax5\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "d:\\3dsmax5\\backburner2\\nrapi20.dll", backburnerInstallDir )
            #backburnerInstallDir = self.AutoCheckFile( "e:\\3dsmax5\\backburner2\\nrapi20.dll", backburnerInstallDir )

            backburnerInstallDir = self.AutoCheckFile( "c:\\Program Files\\backburner2\\nrapi20.dll", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckFile( "c:\\Program Files (x86)\\backburner2\\nrapi20.dll", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckFile( "c:\\Program Files\\Autodesk\\backburner\\nrapi20.dll", backburnerInstallDir )
            backburnerInstallDir = self.AutoCheckFile( "c:\\Program Files (x86)\\Autodesk\\backburner\\nrapi20.dll", backburnerInstallDir )

            if( backburnerInstallDir == "" ):
                FailRender( "Could not automatically detect the install location of Backburner, which 3dsmax requires to run in network rendering mode - please check the installation of 3dsmax" )

            LogInfo( "    Adding backburner install directory to path: %s" % backburnerInstallDir )
            AddToPath( backburnerInstallDir )
            if( SearchPath( "nrapi20.dll" ) == "" ):
                FailRender( "Was not able to configure Backburner into the path, which 3dsmax requires to run in network rendering mode - please contact Deadline Support" )

    def EnsureNecessaryDlls( self, maxInstallDir ):
        filesToCopy = StringCollection()
        filesToCopy.Add( "msvcr71.dll" )
        filesToCopy.Add( "msvcp71.dll" )
        filesToCopy.Add( "msvcr71d.dll" )
        filesToCopy.Add( "msvcp71d.dll" )
        filesToCopy.Add( "msvcp80.dll" )
        filesToCopy.Add( "msvcr80.dll" )
        filesToCopy.Add( "msvcp80d.dll" )
        filesToCopy.Add( "msvcr80d.dll" )

        for fileToCopy in filesToCopy:
            if( not File.Exists( Path.Combine( maxInstallDir, fileToCopy ) ) ):
                if( not File.Exists( Path.Combine( GetDeadlineBinPath(), fileToCopy ) ) ):
                    #LogWarning( "The required file %s could not be found, 3dsmax may not start correctly" % fileToCopy )
                    pass
                else:
                    try:
                        File.Copy( Path.Combine( GetDeadlineBinPath(), fileToCopy ), Path.Combine( maxInstallDir, fileToCopy ), True )
                    except:
                        #LogWarning( "The required file %s could not be copied to %s, 3dsmax may not start correctly" % (fileToCopy, maxInstallDir) )
                        pass

    def Hack3dsmaxCommand( self ):
        maxCmd = ChangeFilename( self.MaxRenderExecutable, "3dsmaxcmd.exe" )
        if( File.Exists( maxCmd ) ):
            line = self.Run3dsmaxCommand( maxCmd )

            maxInstallDirectory = Path.GetDirectoryName( self.MaxRenderExecutable ).lower().replace( "/", "\\" ).rstrip( "\\" )

            # in max 6, the file quoted is ""
            # in max 7, it is "c:\3dsmax7\", but that could change based on where max is installed
            # in max 8 and later, the date/time is printed out on the line before "Error opening..."
            if( line.find( "Error initializing max backburner plugin" ) >= 0 ):
                FailRender( "Backburner does not appear to be configured for this 3ds max." )
            #elif( line.find( "Error opening scene file: \"" ) < 0 ):
            elif( line.lower().replace( "/", "\\" ).find( maxInstallDirectory ) < 0 ):
                FailRender( "There was an error running the 3dsmax command line renderer, %s\nThis may be caused by an incorrect install of 3dsmax.\nContact Deadline support with the following information from its command line output:\n'%s'" % (maxCmd, line) )
            else:
                LogInfo( "3dsmaxcmd.exe returned: %s" % line )
        else:
            FailRender( "Could not find file %s, the 3dsmax command line renderer.\n3dsmax may not be installed properly, or this may be the incorrect version of 3dsmax." % maxCmd )

    def Run3dsmaxCommand( self, maxCmd ):
        countdown = 15000
        freshline = clr.Reference[str]()
        line = ""
        popupHandler = PopupHandler()

        mcmd = ChildProcess()
        mcmd.ControlStdOut = True
        mcmd.TerminateOnExit = True
        mcmd.HideWindow = True
        mcmd.UseProcessTree = True
        mcmd.Launch( maxCmd, "", Path.GetDirectoryName( maxCmd ) )

        while( countdown > 0 and ( mcmd.IsRunning() or mcmd.IsStdoutAvailable() ) ):
            blockingDialogMessage = popupHandler.CheckForPopups( mcmd )
            if( blockingDialogMessage != "" ):
                mcmd.Terminate()
                FailRender( "Dialog box detected while trying to run the 3dsmax command line renderer, %s\nThis may be caused by an incorrect install of 3ds max.\n%s" % (mcmd, blockingDialogMessage) )

            start = DateTime.Now.Ticks
            while( TimeSpan.FromTicks( DateTime.Now.Ticks - start ).Milliseconds < 500 ):
                if( mcmd.GetStdoutLine( freshline ) ):
                    if( freshline.Value != "" ):
                        line = freshline.Value
                else:
                    Sleep( 50 )

            countdown = countdown - 500

        if( mcmd.IsRunning() ):
            mcmd.Terminate()
            FailRender( "The 3dsmax command line renderer, %s, hung during the verification of the 3ds max install" % maxCmd )

        mcmd.Reset()
        mcmd = None

        return line

    def NetworkLogStart( self ):
        self.NetworkLogValid = False
        self.NetworkLogError = ""
        try:
            self.NetworkLogFileSize = 0
            if File.Exists( self.NetworkLogFile ):
                self.NetworkLogFileSize = GetFileSize( self.NetworkLogFile )

            if( self.NetworkLogFileSize > 0 ):
                reader = File.OpenText( self.NetworkLogFile )
                reader.BaseStream.Seek( max( self.NetworkLogFileSize - 192, 0 ), SeekOrigin.Begin )
                self.NetworkLogFilePostfix = reader.ReadToEnd()
                reader.Close()
            else:
                self.NetworkLogFileSize = 0
                self.NetworkLogFilePostfix = ""

            self.NetworkLogValid = True
        except IOException, e:
            LogWarning( "Cannot start network log because: " + e.Message )
            self.NetworkLogError = e.Message

    def NetworkLogGet( self ):
        if self.NetworkLogValid:
            try:
                if File.Exists( self.NetworkLogFile ):
                    reader = File.OpenText( self.NetworkLogFile )
                    if( self.NetworkLogFileSize >= 0 ):
                        reader.BaseStream.Seek( max( self.NetworkLogFileSize - 2048, 0 ), SeekOrigin.Begin )
                    networkLog = reader.ReadToEnd()
                    reader.Close()

                    index = networkLog.find( self.NetworkLogFilePostfix )
                    if( self.NetworkLogFilePostfix == "" or index < 0 ):
                        return networkLog
                    else:
                        return networkLog[index + len( self.NetworkLogFilePostfix ):]
                else:
                    return "Network log file does not exist: " + self.NetworkLogFile
            except IOException, e:
                LogWarning( "Cannot read from network log because: " + e.Message )
                return "Cannot read network log file because: " + e.Message
        else:
            return "Cannot read network log file because: " + self.NetworkLogError

    def CreateStartupScript( self, port ):
        self.StartupMaxScript = Path.Combine( GetSystemTempPath(), "lightning_startup.ms" )
        LogInfo( "Creating startup script: %s" % self.StartupMaxScript )

        self.ErrorMessageFile = Path.Combine( GetSystemTempPath(), "max_startup_error.txt" )
        if( File.Exists( self.ErrorMessageFile ) ):
            File.Delete( self.ErrorMessageFile )

        self.AuthentificationToken = str( DateTime.Now.TimeOfDay.Ticks )

        writer = File.CreateText( self.StartupMaxScript )
        writer.WriteLine( "if (DeadlineUtil != undefined) then (" )
        writer.WriteLine( "  if (DeadlineUtil.RunLightningDaemon %d \"TOKEN:%s\") do" % (port, self.AuthentificationToken) )
        writer.WriteLine( "    quitMax #noprompt" )
        writer.WriteLine( ") else (" )
        writer.WriteLine( "  errorStream = openFile \"%s\" mode:\"w\"" % self.ErrorMessageFile.replace( "\\", "\\\\" ) )
        writer.WriteLine( "  if errorStream == undefined then (" )
        writer.WriteLine( "    -- how should this failure be signaled?" )
        writer.WriteLine( "  ) else (" )
        writer.WriteLine( "    format \"Deadline 3dsmax startup error: lightningMax.dlx does not appear to have loaded on 3dsmax startup, check that it is the right version and installed to the right place.\" to:errorStream" )
        writer.WriteLine( "    close errorStream" )
        writer.WriteLine( "  )" )
        writer.WriteLine( "  quitMax #noprompt" )
        writer.WriteLine( ") -- RunLightningDaemon not defined" )
        writer.Close()

    def CopyLightningDlx( self ):
        lightningDir = Path.Combine( GetDeadlineTempPath(), "lightning" )
        if( not Directory.Exists( lightningDir ) ):
            Directory.CreateDirectory( lightningDir )

        newLightningPluginFile = Path.Combine( lightningDir, "lightning.dlx" )
        LogInfo( "Copying %s to %s" % (self.LightningPluginFile, newLightningPluginFile) )

        if( File.Exists( newLightningPluginFile ) ):
            try:
                File.Delete( newLightningPluginFile )
                File.Copy( self.LightningPluginFile, newLightningPluginFile, False )
            except:
                LogWarning( "Could not delete old %s - this file may be locked by another copy of 3dsmax" % newLightningPluginFile )
        else:
            File.Copy( self.LightningPluginFile, newLightningPluginFile, False )

        if( not File.Exists( newLightningPluginFile ) ):
            FailRender( "Could not copy %s to %s" % (self.LightningPluginFile, newLightningPluginFile) )

        return lightningDir

    def CreatePluginInis( self, lightningDir ):
        self.DeletePluginInis()

        self.TempLightningIni = Path.Combine( GetSystemTempPath(), "lightningplugin_" + self.AuthentificationToken + ".ini" )
        writer = File.CreateText( self.TempLightningIni )
        writer.WriteLine( "[Directories]" )
        writer.WriteLine( "Lightning Plugin=%s" % lightningDir )
        writer.Close()

        # Long paths tend to cause Max 2008 (32 bit) to crash on 64 bit OS - no idea why...
        #self.TempPluginIni = Path.Combine( ToShortPathName( GetSystemTempPath() ), "deadlineplugin_" + self.AuthentificationToken + ".ini" )
        self.TempPluginIni = Path.Combine( ToShortPathName( GetSystemTempPath() ), "dl.ini" )
        if File.Exists( self.TempPluginIni ):
            File.Delete( self.TempPluginIni )

        writer = File.CreateText( self.TempPluginIni )
        writer.WriteLine( "[Include]" )
        writer.WriteLine( "Original=%s" % self.MaxPluginIni )
        #if( self.UseUserProfiles ):
        #    if( File.Exists( self.UserPluginIni ) ):
        #        writer.WriteLine( "UserProfile=%s" % self.UserPluginIni )
        if( self.UserPluginIni != "" and File.Exists( self.UserPluginIni ) ):
                writer.WriteLine( "UserProfile=%s" % self.UserPluginIni )
        writer.WriteLine( "Deadline Lightning=%s" % self.TempLightningIni )
        writer.Close()

        return self.TempPluginIni

    def DeletePluginInis( self ):
        if( self.TempPluginIni != "" ):
            if( File.Exists( self.TempPluginIni ) ):
                try:
                    File.Delete( self.TempPluginIni )
                    self.TempPluginIni = ""
                except:
                    LogWarning( "Could not delete temp plugin ini: %s" % self.TempPluginIni )
            else:
                self.TempPluginIni = ""

        if( self.TempLightningIni != "" ):
            if( File.Exists( self.TempLightningIni ) ):
                try:
                    #File.Delete( self.TempLightningIni )
                    self.TempLightningIni = ""
                except:
                    LogWarning( "Could not delete temp lightning ini: %s" % self.TempLightningIni )
            else:
                self.TempLightningIni = ""

    def LaunchMax( self, executable, arguments, startupDir ):
        global ManagedMaxProcessRenderExecutable
        global ManagedMaxProcessRenderArgument
        global ManagedMaxProcessStartupDirectory

        ManagedMaxProcessRenderExecutable = executable
        ManagedMaxProcessRenderArgument = arguments
        ManagedMaxProcessStartupDirectory = startupDir

        StartMonitoredManagedProcess( self.ProgramName, MaxProcess() )
        VerifyMonitoredManagedProcess( self.ProgramName )

    def WaitForConnection( self, errorMessageOperation ):
        countdown = self.LoadMaxTimeout * 1000
        receivedToken = ""

        #while( countdown > 0 and not self.MaxSocket.IsConnected and MonitoredManagedProcessIsRunning( self.ProgramName ) ):
        while( countdown > 0 and not self.MaxSocket.IsConnected and not IsCanceled() ):
            try:
                VerifyMonitoredManagedProcess( self.ProgramName )
                FlushMonitoredManagedProcessStdout( self.ProgramName )

                blockingDialogMessage = CheckForMonitoredManagedProcessPopups( self.ProgramName )
                if( blockingDialogMessage != "" ):
                    #self.ShutdownMax()
                    FailRender( blockingDialogMessage )

                if( File.Exists( self.ErrorMessageFile ) ):
                    reader = File.OpenText( self.ErrorMessageFile )
                    message = reader.ReadToEnd()
                    reader.Close
                    FailRender( message )

                countdown = countdown - 500
                self.MaxSocket.WaitForConnection( 500, True )

                countdown = countdown - 3000
                receivedToken = self.MaxSocket.Receive( 3000 )

                if( receivedToken.startswith( "TOKEN:" ) ):
                    receivedToken = receivedToken[6:]
                else:
                    self.MaxSocket.Disconnect( False )

            except Exception, e:
                if( not isinstance( e, SimpleSocketTimeoutException ) ):
                    #self.ShutdownMax()
                    FailRender( "%s: Error getting connection from 3dsmax: %s" % (errorMessageOperation, e.Message) )

            if( IsCanceled() ):
                #self.ShutdownMax()
                FailRender( "%s: Initialization was canceled by deadline" % errorMessageOperation )

        if( not self.MaxSocket.IsConnected ):
            #self.ShutdownMax()
            if( countdown > 0 ):
                FailRender( "%s: Max exited unexpectedly - check that max starts up with no dialog messages\n%s" % (errorMessageOperation, self.NetworkLogGet()) )
            else:
                FailRender( "%s: Timed out waiting for 3ds max to start - consider increasing the LoadMaxTimeout in the 3dsmax plugin configuration\n%s" % (errorMessageOperation, self.NetworkLogGet()) )

        if( receivedToken != self.AuthentificationToken ):
            #self.ShutdownMax()
            FailRender( "%s: Did not receive expected token from lightning plugin. (got \"%s\") - check the install of lightningMax.dlx\n%s" % (errorMessageOperation, receivedToken, self.NetworkLogGet()) )

    def LoadMaxFile( self ):
        # Set some pre-loading settings.
        if( self.IgnoreMissingExternalFiles ):
            self.MaxSocket.Send( "IgnoreMissingExternalFiles" )
        if( self.IgnoreMissingUVWs ):
            self.MaxSocket.Send( "IgnoreMissingUVWs" )
        if( self.IgnoreMissingXREFs ):
            self.MaxSocket.Send( "IgnoreMissingXREFs" )
        if( self.IgnoreMissingDLLs ):
            self.MaxSocket.Send( "IgnoreMissingDLLs" )
        if( self.DisableMultipass ):
            self.MaxSocket.Send( "DisableMultipass" )
        if( self.PathConfigFile != "" ):
            self.MaxSocket.Send( "PathConfigFile,\"" + self.PathConfigFile.replace( "\\", "/" ) + "\"" )
            if( self.MergePathConfigFile ):
                self.MaxSocket.Send( "MergePathConfigFile" )

        # Now load the max scene file.
        LogInfo( "Loading 3dsmax scene file" )
        message = "StartJob,\"" + self.MaxFilename.replace( "\\", "/" ) + "\",\"" + self.Camera + "\""
        self.MaxSocket.Send( message )

        countdown = self.StartJobTimeout * 1000
        response = ""

        #while( countdown > 0 and response == "" and not IsCanceled() and MonitoredManagedProcessIsRunning( self.ProgramName ) ):
        while( countdown > 0 and response == "" and not IsCanceled() ):
            VerifyMonitoredManagedProcess( self.ProgramName )
            FlushMonitoredManagedProcessStdout( self.ProgramName )

            blockingDialogMessage = CheckForMonitoredManagedProcessPopups( self.ProgramName )
            if( blockingDialogMessage != "" ):
                #self.ShutdownMax()
                FailRender( blockingDialogMessage )

            try:
                countdown = countdown - 100
                response = self.MaxSocket.Receive( 100 )

                # If this is a STDOUT message, print it out and reset 'response' so that we keep looping
                match = self.StdoutRegex.Match( response )
                if( match.Success ):
                    LogInfo( match.Groups[ 1 ].Value )
                    response = ""

                # If this is a WARN message, print it out and reset 'response' so that we keep looping
                match = self.WarnRegex.Match( response )
                if( match.Success ):
                    LogWarning( match.Groups[ 1 ].Value )
                    response = ""

            except Exception, e:
                if( not isinstance( e, SimpleSocketTimeoutException ) ):
                    FailRender( "Unexpected error while waiting for the max file to load: %s" % e.Message )

        if( IsCanceled() ):
            #self.ShutdownMax()
            FailRender( "StartJob was canceled by Deadline." )

        if( response == "" ):
            #self.ShutdownMax()
            FailRender( "Timed out waiting for the 3dsmax renderer to initialize - increase StartJobTimeout in the plugin configuration.\n%s" % self.NetworkLogGet() )

        if( response.startswith( "ERROR: " ) ):
            #self.ShutdownMax()
            FailRender( "3dsmax: %s\n%s" % (response[7:], self.NetworkLogGet()) )

        if( not response.startswith( "SUCCESS" ) ):
            #self.ShutdownMax()
            FailRender( "Did not receive a success message in response to StartJob.\nResponse: %s\n%s" % (response, self.NetworkLogGet()) )

        if( len( response ) > 7 ):
            LogInfo( response[8:] )

        # Execute the customizations script if it exists. Note that this needs to be done before the
        # post-load settings below because customize.ms sets the output image size, and some settings
        # (like RegionRendering) need to be set AFTER the output image size is set.
        customizationScript = Path.Combine( GetPluginDirectory(), "customize.ms" )
        if( File.Exists( customizationScript ) ):
            self.ExecuteScript( 0, customizationScript, False )

        # Set some post-load settings.
        self.MaxSocket.Send( "SlaveFolders,\"" + GetJobsDataDirectory().replace( "\\", "/" ) + "\",\"" + GetPluginDirectory().replace( "\\", "/" ) + "\"" )
        self.MaxSocket.Send( "TempFolder,\"" + GetSystemTempPath().replace( "\\", "/" ) )
        if( self.LocalRendering ):
            self.MaxSocket.Send( "LocalRendering" )
        if( self.RestartEachFrame ):
            self.MaxSocket.Send( "RestartRendererMode" )
        if( not self.ShowFrameBuffer ):
            self.MaxSocket.Send( "HideFrameBuffer" )
        if( self.RegionRendering ):
            self.MaxSocket.Send( "RegionRendering,%d,%d,%d,%d,%d,%s" % (self.RegionPadding, self.RegionLeft, self.RegionTop, self.RegionRight, self.RegionBottom, self.RegionType) )
        if( self.OverrideSaveFile ):
            self.MaxSocket.Send( "OverrideSaveFile,%d" % self.SaveFile )
        if( self.RedirectOutput ):
            self.MaxSocket.Send( "OutputImageFilename,%s" % self.RenderOutputOverride.replace( "\\", "/" ) )
        if( self.FrameNumberBase != 0 ):
            self.MaxSocket.Send( "FrameNumberBase,%d" % self.FrameNumberBase )
        if( self.RemovePadding ):
            self.MaxSocket.Send( "RemovePadding" )
        if( self.FailOnBlackFrames ):
            self.MaxSocket.Send( "FailOnBlackFrames,%d,%.4f" % (self.BlackPixelPercentage, self.BlackPixelThreshold) )
            if( self.BlackFramesCheckRenderElements ):
                self.MaxSocket.Send( "BlackFramesCheckRenderElements" )
        if( self.UseJpegOutput ):
            self.MaxSocket.Send( "UseJpegOutput,%s" % self.JpegOutputPath.replace( "\\", "/" ) )
        if( self.IgnoreRenderElements ):
            self.MaxSocket.Send( "IgnoreRenderElements" )
        elif( len(self.IgnoreRenderElementsByName) > 0 ):
            ignoreElementMessage = "IgnoreRenderElementsByName,\"" + self.IgnoreRenderElementsByName[0] + "\""
            for elementIndex in range( 1, len(self.IgnoreRenderElementsByName) ):
                ignoreElementMessage = ignoreElementMessage + ",\"" + self.IgnoreRenderElementsByName[elementIndex] + "\""
            self.MaxSocket.Send( ignoreElementMessage )

    def RenderFrame( self, frameNumber ):
        if( not MonitoredManagedProcessIsRunning( self.ProgramName ) ):
            FailRender( "3dsmax exited unexpectedly before being told to render a frame" )

        self.CurrentFrame = frameNumber
        # Reset the current progress if necessary.
        #if( self.CurrentFrame == self.StartFrame ):
        #    SetProgress( 0.0 )

        # Update output filename if necessary.
        if self.RegionRendering and self.RegionRenderingSingleJob:
            self.RegionRenderingSingleFrame = str(GetStartFrame())

            self.RegionLeft = GetIntegerPluginInfoEntryWithDefault( "RegionLeft" + self.RegionRenderingSingleFrame, 0 )
            self.RegionTop = GetIntegerPluginInfoEntryWithDefault( "RegionTop" + self.RegionRenderingSingleFrame, 0 )
            self.RegionRight = GetIntegerPluginInfoEntryWithDefault( "RegionRight" + self.RegionRenderingSingleFrame, 0 )
            self.RegionBottom = GetIntegerPluginInfoEntryWithDefault( "RegionBottom" + self.RegionRenderingSingleFrame, 0 )
            LogInfo( "Region rendering enabled: left = %d, top = %d, right = %d, bottom = %d, padding = %d, type = %s" % (self.RegionLeft, self.RegionTop, self.RegionRight, self.RegionBottom, self.RegionPadding, self.RegionType) )
            self.MaxSocket.Send( "RegionRendering,%d,%d,%d,%d,%d,%s" % (self.RegionPadding, self.RegionLeft, self.RegionTop, self.RegionRight, self.RegionBottom, self.RegionType) )

            self.RenderOutputOverride = GetPluginInfoEntryWithDefault( "RegionFilename" + self.RegionRenderingSingleFrame, "" ).strip()
            LogInfo( "Overriding render output: %s" % self.RenderOutputOverride )
            self.MaxSocket.Send( "OutputImageFilename,%s" % self.RenderOutputOverride.replace( "\\", "/" ) )

            reRegionFilenameIndex = 0
            reRegionFilename = GetPluginInfoEntryWithDefault( "RegionReFilename" + self.RegionRenderingSingleFrame + "_" + str(reRegionFilenameIndex), "" )
            while reRegionFilename != "":
                self.MaxSocket.Send( "OutputImageReFilename,%d,%s" % (reRegionFilenameIndex,reRegionFilename.replace( "\\", "/" )) )
                reRegionFilenameIndex = reRegionFilenameIndex + 1
                reRegionFilename = GetPluginInfoEntryWithDefault( "RegionReFilename" + self.RegionRenderingSingleFrame + "_" + str(reRegionFilenameIndex), "" )

        self.MaxSocket.Send( "CurrentTask,%s" % GetCurrentTaskId() )

        # If we are running a max script job, then just execute the script.
        if( self.MaxScriptJob ):
            self.ExecuteScript( self.CurrentFrame, self.MaxScriptJobScript, True )
        else:
            # First run the pre frame script if necessary.
            if( self.PreFrameScript != "" ):
                self.ExecuteScript( self.CurrentFrame, self.PreFrameScript, False )

            # Now tell lightning to render this frame.
            message = "RenderTask," + str(frameNumber)
            self.MaxSocket.Send( message )

            response = ""
            try:
                response = self.MaxSocket.Receive( 5000 )
            except SimpleSocketTimeoutException:
                FailRender( "RenderFrame: Timed out waiting for the lightning 3dsmax plugin to acknowledge the RenderTask command.\n%s" % self.NetworkLogGet() )

            if( response.startswith( "ERROR: " ) ):
                FailRender( "RenderFrame: %s" % response[7:] )

            if( not response.startswith( "STARTED" ) ):
                FailRender( "RenderFrame: Did not receive a started message in response to RenderTask - got \"%s\"\n%s" % (response, self.NetworkLogGet()) )

            # Wait for the render to complete.
            #self.PollUntilComplete( True )
            self.PollUntilComplete( not self.DisableProgressUpdateTimeout )

            # Now run the post frame script if necessary.
            if( self.PostFrameScript != "" ):
                self.ExecuteScript( self.CurrentFrame, self.PostFrameScript, False )

        # Update the current progress if necessary.
        #if( self.CurrentFrame == self.EndFrame ):
        #    SetProgress( 100.0 )

    def ExecuteScript( self, frameNumber, script, setTime ):
        if( not MonitoredManagedProcessIsRunning( self.ProgramName ) ):
            FailRender( "3dsmax exited unexpectedly before being told to execute a script" )

        # Tell lightning to execute this script.
        LogInfo( "Executing script: %s" % script )
        message = "ExecuteScript," + str(frameNumber) + "," + script.replace( "\\", "/" )
        if( setTime ):
            message = message + ",setTime"
        self.MaxSocket.Send( message )

        response = ""
        try:
            response = self.MaxSocket.Receive( 10000 )
        except SimpleSocketTimeoutException:
            FailRender( "ExecuteScript: Timed out waiting for the lightning 3dsmax plugin to acknowledge the ExecuteScript command.\n%s" % self.NetworkLogGet() )

        if( response.startswith( "ERROR: " ) ):
            FailRender( "ExecuteScript: %s" % response[7:] )

        if( not response.startswith( "STARTED" ) ):
            FailRender( "ExecuteScript: Did not receive a started message in response to ExecuteScript - got \"%s\"\n%s" % (response, self.NetworkLogGet()) )

        # Wait for the script to complete.
        self.PollUntilComplete( False )

    def PollUntilComplete( self, timeoutEnabled ):
        progressCountdown = self.ProgressUpdateTimeout * 1000

        while( progressCountdown > 0 and self.MaxSocket.IsConnected and not IsCanceled() ):
            try:
                VerifyMonitoredManagedProcess( self.ProgramName )
                FlushMonitoredManagedProcessStdout( self.ProgramName )

                blockingDialogMessage = CheckForMonitoredManagedProcessPopups( self.ProgramName )
                if( blockingDialogMessage != "" ):
                    FailRender( blockingDialogMessage )

                # Only decrement the timeout value if timeouts are enabled
                if timeoutEnabled:
                    progressCountdown = progressCountdown - 500

                start = DateTime.Now.Ticks
                while( TimeSpan.FromTicks( DateTime.Now.Ticks - start ).Milliseconds < 500 ):
                    request = self.MaxSocket.Receive( 500 )

                    # We received a request, so reset the progress update timeout.
                    progressCountdown = self.ProgressUpdateTimeout * 1000

                    match = self.FunctionRegex.Match( request )
                    if( match.Success ):
                        # Call the lightning function handler method to see if we should reply or if the render is finished.
                        reply = ""
                        try:
                            reply = self.LightingFunctionHandler( match.Groups[ 1 ].Value )
                            if( reply != "" ):
                                self.MaxSocket.Send( reply )
                        except Exception, e:
                            FailRender( e.Message )
                        continue

                    match = self.SuccessMessageRegex.Match( request )
                    if( match.Success ): # Render finished successfully
                        return match.Groups[ 1 ].Value

                    if( self.SuccessNoMessageRegex.IsMatch( request ) ): # Render finished successfully
                        return ""

                    if( self.CanceledRegex.IsMatch( request ) ): # Render was canceled
                        FailRender( "Render was canceled" )
                        continue

                    match = self.ErrorRegex.Match( request )
                    if( match.Success ): # There was an error
                        FailRender( "%s\n%s" % (match.Groups[ 1 ].Value, self.NetworkLogGet()) )
                        continue
            except Exception, e:
                if( isinstance( e, SimpleSocketTimeoutException ) ):
                    if( progressCountdown <= 0 ):
                        FailRender( "Timed out waiting for the next progress update - consider increasing the ProgressUpdateTimeout in the plugin configuration\n%s" % self.NetworkLogGet() )
                elif( isinstance( e, SimpleSocketException ) ):
                    FailRender( "RenderTask: 3dsmax may have crashed (%s)" % e.Message )
                else:
                    FailRender( "RenderTask: Unexpected exception (%s)" % e.Message )

        if( IsCanceled() ):
            FailRender( "Render was canceled" )

        if( not self.MaxSocket.IsConnected ):
            FailRender( "Socket disconnected unexpectedly" )

        return "undefined"

    def LightingFunctionHandler( self, request ):
        try:
            match = self.ProgressRegex.Match( request )
            if( match.Success ):
                progress = Convert.ToSingle( match.Groups[ 1 ].Value )
                if( progress >= 0 ):
                    if( (self.EndFrame - self.StartFrame) != -1 ):
                        length = ( self.EndFrame - self.StartFrame + 1.0 )
                        weightPercentage = 1.0 / length
                        completionPercentage = ( ( self.CurrentFrame - self.StartFrame ) / length ) * 100.0
                        actualProgress = ( progress * weightPercentage ) + completionPercentage
                        SetProgress( actualProgress )
                    else:
                        SetProgress( progress )

                return ""
        except:
            return ""

        try:
            match = self.SetTitleRegex.Match( request )
            if( match.Success ):
                SetStatusMessage( "Frame " + str(self.CurrentFrame) + " - " + match.Groups[ 1 ].Value )
                return ""
        except:
            return ""

        try:
            match = self.StdoutRegex.Match( request )
            if( match.Success ):
                LogInfo( match.Groups[ 1 ].Value )
                return ""
        except:
            return ""

        try:
            match = self.WarnRegex.Match( request )
            if( match.Success ):
                LogWarning( match.Groups[ 1 ].Value )
                return ""
        except:
            return ""

        match = self.GetJobInfoEntryRegex.Match( request )
        if( match.Success ):
            entry = GetPluginInfoEntryWithDefault( match.Groups[ 1 ].Value, "" )
            if( entry != "" ):
                return "SUCCESS: " + entry
            else:
                return "NOVALUE"

        match = self.GetSubmitInfoEntryRegex.Match( request )
        if( match.Success ):
            try:
                return "SUCCESS: " + GetJobInfoEntry( match.Groups[ 1 ].Value )
            except:
                return "NOVALUE"

        match = self.GetSubmitInfoEntryElementCountRegex.Match( request )
        if( match.Success ):
            try:
                return "SUCCESS: " + str(GetJobInfoEntryElementCount( match.Groups[ 1 ].Value ))
            except:
                return "NOVALUE"

        match = self.GetSubmitInfoEntryElementRegex.Match( request )
        if( match.Success ):
            try:
                return "SUCCESS: " + GetJobInfoEntryElement( match.Groups[ 2 ].Value, Convert.ToInt32( match.Groups[ 1 ].Value ) )
            except:
                return "NOVALUE"

        match = self.GetAuxFileRegex.Match( request )
        if( match.Success ):
            try:
                return self.AuxiliaryFilenames[ Convert.ToInt32( match.Groups[ 1 ].Value ) ]
            except:
                return "NOVALUE"

        else:
            # Unknown message.
            FailRender( "Got unexpected request from the lightning max plugin: \"" + request + "\"\n" + self.NetworkLogGet() )
            return "CANCEL"

        return ""

######################################################################
## This is the class that starts up the 3dsmax process.
######################################################################
class MaxProcess (ManagedProcess):
    def InitializeProcess( self ):
        self.ProcessPriority = ProcessPriorityClass.BelowNormal
        self.UseProcessTree = True
        #self.PopupHandling = True
        self.PopupHandling = GetBooleanPluginInfoEntryWithDefault( "PopupHandling", True )
        self.StdoutHandling = False
        self.HideDosWindow = False

        # Set the affinity to the current thread number if rendering on one cpu per task
        if GetBooleanPluginInfoEntryWithDefault( "OneCpuPerTask", False ):
            cpuCount = GetCpuCount()
            cpu = ( ( cpuCount + GetThreadNumber() ) % cpuCount ) # just in case there are more tasks than cpus
            self.ProcessAffinity = (cpu,)

        # For Brazil
        self.AddPopupIgnorer( ".*Brazil Console.*" )

        # For Finalrender
        self.AddPopupIgnorer( ".*MSP Acceleration.*" )

        # For Fume FX
        self.AddPopupIgnorer( ".*FumeFX.*" )
        self.AddPopupIgnorer( ".*FumeFX Dynamics:.*" )
        self.AddPopupHandler( "FumeFX\s*$", "Yes" )

        # For Maxwell
        self.AddPopupIgnorer( ".*Maxwell Translation Window.*" )
        self.AddPopupIgnorer( ".*Import Multilight.*" )

        # For Craft Director Tools
        self.AddPopupIgnorer( ".*New updates are available - Craft Director Tools.*" )

        # For Hair Farm progress dialog
        self.AddPopupIgnorer( "Hair Farm" )

        # For render progress dialog
        self.AddPopupIgnorer( "Batch Render In Progress" )

        # For Pencil plugin
        self.AddPopupIgnorer( "Pencil" )

        # For VRay in workstation mode (such as when rendering mental ray)
        self.AddPopupHandler( ".*VRay authorization.*", "Cancel" )
        self.AddPopupHandler( ".*V-Ray warning.*", "OK" )

        # For File Units Mismatch dialog
        self.AddPopupHandler( ".*File Load: Units Mismatch.*", "Adopt the File's Unit Scale?;OK" )

        # Handle Autodesk 3dsMax2010+ "File Load: Gamma & LUT Settings Mismatch" dialog in workstation mode
        # Do you want to: "Adopt the File's Gamma and LUT Settings?"
        self.AddPopupHandler( ".*File Load: Gamma & LUT Settings Mismatch.*", "Adopt the File's Gamma and LUT Settings?;OK" )

        # Handle 3ds Max "gamma/LUT correction" dialog in workstation mode
        # "Do you want gamma/LUT correction to be ENABLED to correspond with the setting in this file?" Yes or No
        self.AddPopupHandler( ".*3ds Max.*", "Yes" )

        # Handle NTSC PAL Animation
        self.AddPopupHandler( ".*Frame Rate Change.*", "OK" )

        # Handle MAXScript Auto-load errors
        self.AddPopupHandler( ".*MAXScript Auto-load Script Error.*", "OK" )

        # Handle Render History Settings dialog
        #self.AddPopupHandler( ".*Render history settings.*", "OK;No" )
        self.AddPopupIgnorer( ".*Render history settings.*" )
        self.AddPopupIgnorer( ".*Render history note.*" )

        # Handle Crash dialog
        self.AddPopupHandler( ".*Warning - the software has encountered a problem.*", "Don't show me this error again;Continue" )

        # Handle Frantic Films' FPS Watchdog dialog
        self.AddPopupHandler( ".*Frantic Films FPS Watchdog.*", "OK" )

        # Handle Missing DLLs dialog in workstation mode
        self.AddPopupHandler( ".*Missing Dlls.*", "Cancel" )

        # Handle Brazil Rio warning dialog
        self.AddPopupHandler( "Brazil r/s Rio Warning", "OK" )

        # Handle a 3dsmax warning dialog
        self.AddPopupHandler( "3D Studio MAX", "OK" )

        # Handle Craft Director Tools software update dialog
        self.AddPopupHandler( ".*New updates are available - Craft Director Tools.*", "Cancel" )

        # Handle Standard MAX Pop-Up dialog
        self.AddPopupHandler( ".*Pop-up Note.*", "OK" )

        # Handle Wacom Tablet Version Mismatch dialog
        self.AddPopupHandler( ".*Tablet Version Mismatch.*", "OK" )

        # Handle Wacom Tablet Driver dialog
        self.AddPopupHandler( ".*Tablet Driver.*", "OK" )

        # Handle Craft Director Tools Missing Nodes dialog
        self.AddPopupHandler( ".*Gather error.*", "OK" )

        # Handle Image I/O Error dialog
        self.AddPopupHandler( ".*Image I/O Error.*", "Retry" )

        # Handle nPower Plug-in Messages dialog
        self.AddPopupHandler( ".*Important nPower Plug-in Messages.*", "OK" )

        # Handle Glu3D Plugin Version Expired Warning dialog
        self.AddPopupHandler( ".*glu3D.*", "OK" )

        # Handle Glu3D No Particle/Mesh Cache Warning dialog
        self.AddPopupHandler( ".*glu3D Warning!.*", "OK" )

        # Handle Bitmap Filter Error dialog
        self.AddPopupHandler( ".*Bitmap Filter Error.*", "OK" )

        # Handle Maxwell Plug-in Update Notification dialog
        self.AddPopupHandler( ".*Maxwell Plug-in Update Notification.*", "Don't notify me about this version automatically;Close" )

        # Handle RealFlow Plug-in Update Notification dialog
        self.AddPopupHandler( ".*RealFlow Plug-in Update Notification.*", "Don't notify me about this version automatically;Close" )

        # Handle 3dsMax Learning Movies (Essential Skills Movies) dialog in workstation mode (first time 3dsMax starts up only)
        self.AddPopupHandler( ".*Learning Movies.*", "Show this dialog at startup;Close" )

        # Handle Obsolete File dialog in workstation mode
        self.AddPopupHandler( ".*Obsolete File.*", "Don't display this message.;OK" )

        # Handle "Error Loading" dialog due to a corrupt node/geometry in the 3dsMax scene (normally found with data transferred from Maya/FBX files)
        self.AddPopupHandler( ".*Error Loading.*", "OK" )

        # Handle "IO Error" dialog in workstation mode due to 3dsMax "Set Project Folder" incorrect. Error Message: "The following configuration path(s) do not exist."
        # Typically this error occurs due to a part of the 3dsMax project folder structure being set to a location that no longer exists.
        self.AddPopupHandler( ".*IO Error.*", "OK" )

        # Handle "Error" dialog due to "Loading of custom driver failed:Forcing null driver mode" incompatible graphics driver identified in 3dsmax.ini
        self.AddPopupHandler( ".*Error.*", "OK" )
        self.AddPopupHandler( ".*Loading of custom driver failed.*", "OK" )

        # Handle "Warning" dialog due rendering Hair with high-poly growth objects.
        self.AddPopupHandler( "Warning", "OK" )

        # Handle Vue xStream "Welcome New User!" Dialog in GUI / Workstation mode
        self.AddPopupHandler( ".*Welcome To Vue 8 xStream!.*", "Don't show this dialog again;Close" )

        # Handle Vue xStream Dialog: Switching to OpenGL Engine when an incompatible graphics card is detected
        self.AddPopupHandler( ".*Vue 8 xStream.*", "OK" )

        # Handle 3ds Max Performance Driver Dialog in GUI / Workstation mode
        self.AddPopupHandler( ".*3ds Max Performance Driver.*", "OK" )

        # Handle Autodesk Customer Involvement Program dialog in workstation mode
        # Default setting is "Participate Anonymously"
        self.AddPopupHandler( ".* Autodesk Customer Involvement Program.*", "OK" )

        # nPower Software Plugin Product Selection Dialog - v7.00+ - Potential Options Below:
        # Geometry Creation - [Power Solids], [Power BODY Toolkit], [Power NURBS Pro]
        # Standard Translators - [Power Translators], [Power Translators Pro], [Power Translators Rhino]
        # Bundles - [Solids Bundle (Solids + Translators)], [NURBS Bundle (NURBS + Translators)]
        # Native Translators - [Power Translators Universal]
        self.AddPopupHandler( ".*nPower Software Plugin Product Selection Dialog.*", "Solids Bundle (Solids + Translators);OK" )

        # Handle the "An exception occured when loading DRA" error that can popup.
        self.AddPopupHandler( ".*Exception.*", "OK" )

        # DbxHost Message - causes both 3dsMax and Deadline Slave app to crash out
        # ObjectDBX has reported a fatal error: Unable to load the Modeler DLLs
        # http://www.the-area.com/forum/autodesk-3ds-max/3ds-max-through-2008/error-upon-startup/
        # http://forums.cgsociety.org/showthread.php?t=835640
        # http://usa.autodesk.com/adsk/servlet/ps/dl/item?siteID=123112&id=5581859&linkID=9241177
        self.AddPopupHandler( ".*DbxHost Message.*", "OK" )

        # Handle the "Unable to write to FumeFX.ini" error
        self.AddPopupHandler( "File error", "OK" )

        # Handle Hair high-poly growth object
        self.AddPopupHandler( ".*Hair with high-poly growth objects.*", "OK" )

    def RenderExecutable( self ):
        global ManagedMaxProcessRenderExecutable
        return ManagedMaxProcessRenderExecutable

    def RenderArgument( self ):
        global ManagedMaxProcessRenderArgument
        return ManagedMaxProcessRenderArgument

    def StartupDirectory( self ):
        global ManagedMaxProcessStartupDirectory
        return ManagedMaxProcessStartupDirectory
