import re

from System import *
from System.Diagnostics import *
from System.IO import *
from System.Environment import *

from Deadline.Plugins import *
from Deadline.Scripting import *
from Deadline.Slaves import *

from System.Diagnostics import Stopwatch

######################################################################
## Import standard python module
######################################################################
import sys
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
if root not in sys.path:
    sys.path.append(root)


from PyDeadline.deadline import env, plugins
from PyDeadline.deadline import callback
from PyDeadline.deadline import MxDLJob

######################################################################
## This is the function that Deadline calls to get an instance of the
## main DeadlinePlugin class.
######################################################################
def GetDeadlinePlugin():
    return MXMayaCmdPlugin()

######################################################################
## This is the main DeadlinePlugin class for the FusionCmd plugin.
######################################################################
class MXMayaCmdPlugin (DeadlinePlugin):
    Version = 0
    Build = "none"
    Renderer = "mayasoftware"
    
    LocalRendering = False
    LocalRenderDirectory = ""
    NetworkRenderDirectory = ""
    RegionRendering = False
    SingleRegionJob = False
    SingleRegionFrame = 0
    SingleRegionIndex = ""
    FinishedFrameCount = 0
    IgnoreError211 = False
    
    PreviousFinishedFrame = ""
    SkipNextFrame = False
    
    vrayRenderingImage = False
    
    ## Called by Deadline to initialize the process.
    def InitializeProcess( self ):
        # Set the plugin specific settings.
        self.SingleFramesOnly = False
        self.PluginType = PluginType.Simple

        # Set the process specific settings.
        self.ProcessPriority = ProcessPriorityClass.BelowNormal
        self.UseProcessTree = True
        self.PopupHandling = True
        self.StdoutHandling = True
        
        # Catch licensing errors.
        self.AddStdoutHandler( "FLEXlm error: .*", self.HandleFlexlmError )
        #self.AddStdoutHandler( ".*Error: (Mayatomr) : could not get a license", self.HandleFlexlmError )
        
        # Was there something wrong in the command line?
        self.AddStdoutHandler( "Usage: Render .*", self.HandleUsageError )
        
        # Progress updates, works when rendering multiple frames per chunk.
        self.AddStdoutHandler( "Finished Rendering.*\\.([0-9]+)\\.[^\\.]+", self.HandleChunkedProgress1 )
        self.AddStdoutHandler( ".*Finished Rendering.*", self.HandleChunkedProgress2 )
        
        # Some status messages.
        self.AddStdoutHandler( "Constructing shading groups|Rendering current frame", self.HandleStatusMessage )
        
        # Error message handling.
        self.AddStdoutHandler( ".*Error: .*|.*Warning: .*", self.HandleErrorMessage )
        
        # Mental Ray progress handling.
        self.AddStdoutHandler( "progr: +([0-9]+\\.[0-9]+)% +rendered", self.HandleMentalRayProgress )
        self.AddStdoutHandler( "progr: +([0-9]+\\.[0-9]+)% +computing final gather points", self.HandleMentalRayGathering )
        self.AddStdoutHandler( "progr: writing image file .* \\(frame ([0-9]+)\\)", self.HandleMentalRayWritingFrame )
        self.AddStdoutHandler( "progr: +rendering finished", self.HandleMentalRayComplete )
        
        self.AddStdoutHandler( "\\[PROGRESS\\] Completed frame*", self.HandleProgressMessage2 )
        self.AddStdoutHandler( ".*\\[PROGRESS\\] TURTLE rendering frame 100\\.00.*", self.HandleProgressMessage2 )
        self.AddStdoutHandler( ".*Render complete.*", self.HandleProgressMessage2 )
        
        self.AddStdoutHandler( "\\[PROGRESS\\] Percentage of rendering done: (.*)", self.HandleProgressMessage3 )
        self.AddStdoutHandler( ".*\\[PROGRESS\\] TURTLE rendering frame ([0-9]+\\.[0-9]+).*", self.HandleProgressMessage3 )
        self.AddStdoutHandler( ".*RIMG : +([0-9]+)%", self.HandleProgressMessage3 )
        #self.AddStdoutHandler( "([0-9]+)%", self.HandleProgressMessage3 )
        
        self.AddStdoutHandler( "V-Ray: Building light cache*", self.HandleVrayMessage )
        self.AddStdoutHandler( "V-Ray: Prepass ([0-9]+) of ([0-9]+)*", self.HandleVrayMessage )
        self.AddStdoutHandler( "V-Ray: Rendering image*", self.HandleVrayMessage )
        self.AddStdoutHandler( "V-Ray: +([0-9]+)%", self.HandleVrayProgress )
        self.AddStdoutHandler( "V-Ray: +([0-9]+) %", self.HandleVrayProgress )
        self.AddStdoutHandler( "([0-9]+) % completed", self.HandleVrayProgress )
        
        self.AddStdoutHandler( "V-Ray: Updating frame at time ([0-9]+)", self.HandleVrayExportProgress )
        self.AddStdoutHandler( "V-Ray: Render complete", self.HandleVrayExportComplete )
        
        self.AddStdoutHandler( "\\[PROGRESS\\] ([0-9]+) percent", self.HandleProgressMessage1 )
        self.AddStdoutHandler( "([0-9]+)%", self.HandleProgressMessage1 )
        
        # Set the popup ignorers.
        self.AddPopupIgnorer( ".*entry point.*" )
        self.AddPopupIgnorer( ".*Entry Point.*" )
        
        # Ignore Vray popup
        self.AddPopupIgnorer( ".*Render history settings.*" )
    
    ## Called by Deadline to get the render executable.
    def RenderExecutable( self ):
        self.Version = StringUtils.ParseLeadingNumber( GetPluginInfoEntry( "Version" ) )
        versionString = str( self.Version ).replace( ".", "_" )
        
        mayaExecutable = ""
        mayaExeList = GetConfigEntry( "RenderExecutable" + versionString )
        
        self.Build = GetPluginInfoEntryWithDefault( "Build", "None" ).lower()
        if( self.Build == "32bit" ):
            LogInfo( "Enforcing 32 bit build of Maya" )
            mayaExecutable = SearchFileListFor32Bit( mayaExeList )
            if( mayaExecutable == "" ):
                FailRender( "32 bit Maya " + versionString + " render executable was not found in the semicolon separated list \"" + mayaExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        
        elif( self.Build == "64bit" ):
            LogInfo( "Enforcing 64 bit build of Maya" )
            mayaExecutable = SearchFileListFor64Bit( mayaExeList )
            if( mayaExecutable == "" ):
                FailRender( "64 bit Maya " + versionString + " render executable was not found in the semicolon separated list \"" + mayaExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        
        else:
            LogInfo( "Not enforcing a build of Maya" )
            mayaExecutable = SearchFileList( mayaExeList )
            if( mayaExecutable == "" ):
                FailRender( "Maya " + versionString + " render executable was not found in the semicolon separated list \"" + mayaExeList + "\". The path to the render executable can be configured from the Plugin Configuration in the Deadline Monitor." )
        
        # If on the Mac, set some environment variables (these are normally set by MayaENV.sh when running the Maya Terminal).
        if IsRunningOnMac():
            mayaBinFolder = Path.GetDirectoryName( mayaExecutable )
            path = Environment.GetEnvironmentVariable( "PATH" )
            if path:
                if path.find( path ) == -1:
                    usrAwComBin = "/usr/aw/COM/bin"
                    usrAwComEtc = "/usr/aw/COM/etc"
                    
                    LogInfo( "Adding " + mayaBinFolder + " to PATH environment variable for this session" )
                    LogInfo( "Adding " + usrAwComBin + " to PATH environment variable for this session" )
                    LogInfo( "Adding " + usrAwComEtc + " to PATH environment variable for this session" )
                    
                    path = mayaBinFolder + ":" + usrAwComBin + ":" + usrAwComEtc + ":" + path
                    Environment.SetEnvironmentVariable( "PATH", path )
            
            mayaLocation = Path.GetDirectoryName( mayaBinFolder )
            LogInfo( "Setting MAYA_LOCATION environment variable to " + mayaLocation + " for this session" )
            Environment.SetEnvironmentVariable( "MAYA_LOCATION", mayaLocation )
            
            mayaMacOSFolder = Path.Combine( mayaLocation, "MacOS" )
            libraryPath = Environment.GetEnvironmentVariable( "DYLD_LIBRARY_PATH" )
            if libraryPath:
                if libraryPath.find( mayaMacOSFolder ) == -1:
                    LogInfo( "Adding " + mayaMacOSFolder + " to DYLD_LIBRARY_PATH environment variable for this session" )
                    libraryPath = mayaMacOSFolder + ":" + libraryPath
                    Environment.SetEnvironmentVariable( "DYLD_LIBRARY_PATH", libraryPath )
        
        return mayaExecutable
    
    ## Called by Deadline to get the render arguments.
    def RenderArgument( self ):
        #self.LocalRendering = GetBooleanConfigEntry( "LocalRendering" )
        self.LocalRendering = GetBooleanPluginInfoEntryWithDefault( "LocalRendering", False )
        self.Renderer = GetPluginInfoEntryWithDefault( "Renderer", "mayaSoftware" ).lower()
        
        self.NetworkRenderDirectory = GetPluginInfoEntryWithDefault( "OutputFilePath", "" ).strip().replace( "\\", "/" )
        self.NetworkRenderDirectory = CheckPathMapping( self.NetworkRenderDirectory ).replace( "\\", "/" )
        if( len( self.NetworkRenderDirectory ) > 0 and ( self.NetworkRenderDirectory.endswith( "\\" ) or self.NetworkRenderDirectory.endswith( "/" ) ) ):
            #self.NetworkRenderDirectory = Path.GetDirectoryName( self.NetworkRenderDirectory )
            self.NetworkRenderDirectory = self.NetworkRenderDirectory.rstrip( "/\\" )
        
        if( self.LocalRendering and self.Renderer != "mentalrayexport" and self.Renderer != "vrayexport" ):
            if( len( self.NetworkRenderDirectory ) == 0 ):
                self.LocalRendering = False
                LogInfo( "OutputFilePath was not specified in the plugin info file, rendering to network drive" )
            else:
                self.LocalRenderDirectory = CreateTempDirectory( "mayaOutput" )
                LogInfo( "Rendering to local drive, will copy files and folders to final location after render is complete" )
        else:
            LogInfo( "Rendering to network drive" )
            self.LocalRendering = False
        
        self.IgnoreError211 = GetBooleanPluginInfoEntryWithDefault( "IgnoreError211", False )
        
        LogInfo( "Rendering with Maya version " + str(self.Version) )
        
        rendererArguments = ""
        
        usingRenderLayers = GetBooleanPluginInfoEntryWithDefault( "UsingRenderLayers", False )
        renderLayer = GetPluginInfoEntryWithDefault( "RenderLayer", "" ).strip()
        
        self.RegionRendering = GetBooleanPluginInfoEntryWithDefault( "RegionRendering", False )
        self.SingleRegionJob = GetBooleanPluginInfoEntryWithDefault( "RegionSingleJob", False )
        self.SingleRegionFrame = GetPluginInfoEntryWithDefault( "RegionSingleFrame", "0" )
        self.SingleRegionIndex = str(GetStartFrame())
        
        # These can be different for some renderers.
        byFrameArg = "-b"
        renLayerArg = "-rl"
        
        # If this is a render layer job, but no layer was specified, then assume that all layers
        # are being rendered as part of the same job.
        if( self.Renderer == "file" or ( usingRenderLayers and len( renderLayer ) == 0 ) ):
            LogInfo( "Rendering all layers - using the renderer(s) set in the Maya render settings." )
            
            rendererArguments += " -r file"
            
        else:
            if( self.Renderer == "vray" ):
                LogInfo( "Rendering with VRay." )
                
                rendererArguments += " -r vray " + self.GetTileRenderArgument()
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                
            elif( self.Renderer == "vrayexport" ):
                LogInfo( "Exporting with VRay" )
                
                rendererArguments += " -r vray -exportFileName \"" + GetPluginInfoEntry( "VRayExportFile" ) + "\" -noRender"
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                
            elif( self.Renderer == "maxwell" ):
                LogInfo( "Rendering with Maxwell." )
                
                renLayerArg = "-l"
                
                rendererArguments += " -r maxwell "
                rendererArguments += " -nt " + GetPluginInfoEntryWithDefault( "MaxProcessors", "0" )
                rendererArguments += BlankIfEitherIsBlank( " -rt ", GetPluginInfoEntryWithDefault( "MaxwellRenderTime", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -sl ", GetPluginInfoEntryWithDefault( "MaxwellSamplingLevel", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                
                slaveFound = False
                thisSlave = Environment.MachineName.lower()
                interactiveSlaves = GetConfigEntryWithDefault( "MaxwellInteractiveSlaves", "" ).split( ',' )
                for slave in interactiveSlaves:
                    if slave.lower().strip() == thisSlave:
                        LogInfo( "This slave is in the Maxwell interactive license list - an interactive license for Maxwell will be used instead of a render license" )
                        slaveFound = True
                        break
                
                if not slaveFound:
                    rendererArguments += " -cmd -node"
                
            elif( self.Renderer == "gelato" ):
                LogInfo( "Rendering with Gelato." )
                
                rendererArguments += " -r gelato "
                rendererArguments += " -n " + GetPluginInfoEntryWithDefault( "MaxProcessors", "0" )
                rendererArguments += BlankIfEitherIsBlank( " -mb ", GetPluginInfoEntryWithDefault( "MotionBlur", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -ard ", GetPluginInfoEntryWithDefault( "AspectRatio", "" ) )
            
            elif( self.Renderer == "3delight" ):
                LogInfo( "Rendering with 3delight." )
                
                byFrameArg = "-inc"
                renLayerArg = "-lr"
                
                rendererArguments += " -r 3delight -an 1 " + self.GetTileRenderArgument()
                rendererArguments += " -cpus " + GetPluginInfoEntryWithDefault( "MaxProcessors", "0" )
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
            
            elif( self.Renderer == "finalrender" ):
                LogInfo( "Rendering with Final Render" )
                
                rendererArguments += "-r fr -v 2 " + self.GetTileRenderArgument()
                rendererArguments += " -n " + GetPluginInfoEntryWithDefault( "MaxProcessors", "0" )
                rendererArguments += BlankIfEitherIsBlank( " -mb ", GetPluginInfoEntryWithDefault( "MotionBlur", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -ard ", GetPluginInfoEntryWithDefault( "AspectRatio", "" ) )
            
            elif( self.Renderer == "renderman" ):
                LogInfo( "Rendering with Renderman for Maya" )
                
                rendererArguments += " -r rman " + self.GetTileRenderArgument()
                rendererArguments += " -n " + GetPluginInfoEntryWithDefault( "MaxProcessors", "0" )
                rendererArguments += BlankIfEitherIsBlank( " -setAttr motionBlur ", GetPluginInfoEntryWithDefault( "MotionBlur", "" ) )
                
                width = GetPluginInfoEntryWithDefault( "ImageWidth", "" )
                height = GetPluginInfoEntryWithDefault( "ImageHeight", "" )
                if( len( width ) > 0 and len( height ) > 0 ):
                    rendererArguments += " -res " + width + " " + height
                
            elif( self.Renderer == "turtle" ):
                LogInfo( "Rendering with Turtle" )
                
                rendererArguments += " -r turtle " + self.GetTileRenderArgument()
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                
            elif( self.Renderer == "mentalray" ):
                LogInfo( "Rendering with Mental Ray" )
                
                verbosity = 5
                verbosityString = GetPluginInfoEntryWithDefault( "MentalRayVerbose", "Progress Messages" )
                if verbosityString == "Fatal Messages Only":
                    verbosity = 1
                elif verbosityString == "Error Messages":
                    verbosity = 2
                elif verbosityString == "Warning Messages":
                    verbosity = 3
                elif verbosityString == "Info Messages":
                    verbosity = 4
                elif verbosityString == "Progress Messages":
                    verbosity = 5
                elif verbosityString == "Detailed Messages (Debug)":
                    verbosity = 6
                
                rendererArguments += "-r mr -v " + str(verbosity) + " " + self.GetTileRenderArgument()
                
                #rendererArguments += BlankIfEitherIsBlank( " -rt ", GetPluginInfoEntryWithDefault( "MaxProcessors", "" ) )
                numThreads = GetIntegerPluginInfoEntryWithDefault( "MaxProcessors", 0 )
                if numThreads > 0:
                    rendererArguments += " -rt " + str(numThreads)
                else:
                    if self.Version >= 2008:
                        rendererArguments += " -art"
                
                if self.Version >= 2008:
                    autoMemoryLimit = GetBooleanPluginInfoEntryWithDefault( "AutoMemoryLimit", True )
                    if autoMemoryLimit:
                        rendererArguments += " -aml"
                    else:
                        memoryLimit = GetIntegerPluginInfoEntryWithDefault( "MemoryLimit", 0 )
                        if memoryLimit >= 0:
                            rendererArguments += " -mem " + str(memoryLimit)
                
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -ard ", GetPluginInfoEntryWithDefault( "AspectRatio", "" ) )
                
            elif( self.Renderer == "mentalrayexport" ):
                LogInfo( "Exporting with Mental Ray" )
                
                rendererArguments += " -r mi -file \"" + GetPluginInfoEntry( "MentalRayExportfile" ) + "\""
                
                binary = GetBooleanPluginInfoEntryWithDefault( "MentalRayExportBinary", False )
                rendererArguments += " -binary " + str(binary).lower()
                if not binary:
                    rendererArguments += " -tabstop " + GetPluginInfoEntryWithDefault( "MentalRayExportTabStop", "8" )
                
                perFrame = GetIntegerPluginInfoEntryWithDefault( "MentalRayExportPerFrame", 2 )
                rendererArguments += " -perframe " + str(perFrame)
                if perFrame != 0:
                    rendererArguments += " -padframe " + GetPluginInfoEntryWithDefault( "MentalRayExportPadFrame", "4" )
                
                if self.Version >= 2008:
                    if GetBooleanPluginInfoEntryWithDefault( "MentalRayExportPerLayer", False ):
                        rendererArguments += " -perlayer 1"
                    else:
                        rendererArguments += " -perlayer 0"
                
                pathnames = GetPluginInfoEntryWithDefault( "MentalRayExportPathNames", "" ).strip()
                if len( pathnames ) > 0:
                    pathnames = pathnames.replace( "1", "a" ) # absolute
                    pathnames = pathnames.replace( "2", "r" ) # relative
                    pathnames = pathnames.replace( "3", "n" ) # none
                    rendererArguments += " -exportPathNames " + pathnames
                    
                fragment = GetBooleanPluginInfoEntryWithDefault( "MentalRayExportFragment", False )
                if fragment:
                    rendererArguments += " -fragmentExport"
                    
                    materials = GetBooleanPluginInfoEntryWithDefault( "MentalRayExportFragmentMaterials", False )
                    if materials:
                        rendererArguments += " -fragmentMaterials"
                    
                    shaders = GetBooleanPluginInfoEntryWithDefault( "MentalRayExportFragmentShaders", False )
                    if shaders:
                        rendererArguments += " -fragmentIncomingShdrs"
                    
                    childDag = GetBooleanPluginInfoEntryWithDefault( "MentalRayExportFragmentChildDag", False )
                    if childDag:
                        rendererArguments += " -fragmentChildDag"
                    
                filters = GetPluginInfoEntryWithDefault( "MentalRayExportFilterString", "" ).strip()
                if len( filters ) > 0:
                    rendererArguments += " -exportFilterString \"" + filters + "\""
    
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -ard ", GetPluginInfoEntryWithDefault( "AspectRatio", "" ) )
                
            elif( self.Renderer == "mayahardware" ):
                LogInfo( "Rendering with Maya Hardware" )
                
                rendererArguments += " -r hw"
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -mb ", GetPluginInfoEntryWithDefault( "MotionBlur", "" ) )
                
            elif( self.Renderer == "mayavector" ):
                LogInfo( "Rendering with Maya Vector" )
                
                rendererArguments += " -r vr"
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                
            else:
                LogInfo( "Rendering with Maya Software" )
                
                antialiasing = GetPluginInfoEntryWithDefault( "AntiAliasing", "" )
                if antialiasing == "low":
                    antialiasing = "3"
                elif antialiasing == "medium":
                    antialiasing = "2"
                elif antialiasing == "high":
                    antialiasing = "1"
                elif antialiasing == "highest":
                    antialiasing = "0"
                
                rendererArguments += " -r sw " + self.GetTileRenderArgument()
                rendererArguments += " -n " + GetPluginInfoEntryWithDefault( "MaxProcessors", "0" )
                rendererArguments += BlankIfEitherIsBlank( " -eaa ", antialiasing )
                rendererArguments += BlankIfEitherIsBlank( " -mb ", GetPluginInfoEntryWithDefault( "MotionBlur", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -x ", GetPluginInfoEntryWithDefault( "ImageWidth", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -y ", GetPluginInfoEntryWithDefault( "ImageHeight", "" ) )
                rendererArguments += BlankIfEitherIsBlank( " -ard ", GetPluginInfoEntryWithDefault( "AspectRatio", "" ) )
                
            if len( renderLayer ) > 0:
                rendererArguments += " " + renLayerArg + " " + renderLayer
        
        # Set the common arguments which are available for all renderers.
        commonArguments = ""
        if GetBooleanPluginInfoEntryWithDefault( "RenderHalfFrames", False ):
            LogInfo( "Rendering half frames" )
            commonArguments += " -s " + str(GetStartFrame()) + " -e " + str(GetEndFrame() + 0.5) + " " + byFrameArg + " 0.5 -rfs " + str(GetStartFrame() * 2)
        else:
            if self.RegionRendering and self.SingleRegionJob:
                commonArguments += " -s " + str(self.SingleRegionFrame) + " -e " + str(self.SingleRegionFrame) + " " + byFrameArg + " 1"
            else:
                commonArguments += " -s " + str(GetStartFrame()) + " -e " + str(GetEndFrame()) + " " + byFrameArg + " 1"
        
        renderDirectory = self.NetworkRenderDirectory
        if( len( self.LocalRenderDirectory ) > 0 ):
            renderDirectory = self.LocalRenderDirectory
        
        # These output arguments aren't supported by 3delight.
        if( self.Renderer != "3delight" ):
            commonArguments += BlankIfEitherIsBlank( " -rd \"", BlankIfEitherIsBlank( renderDirectory, "\"" ) )
            
            if self.RegionRendering and self.SingleRegionJob:
                outputFilePrefix = GetPluginInfoEntryWithDefault( "RegionPrefix" + self.SingleRegionIndex, "" ).replace( "\\", "/" )
            else:
                outputFilePrefix = GetPluginInfoEntryWithDefault( "OutputFilePrefix", "" ).strip().replace( "\\", "/" )
            commonArguments += BlankIfEitherIsBlank( BlankIfEitherIsBlank( " -im \"", outputFilePrefix ), "\"" )
            
        commonArguments += BlankIfEitherIsBlank( BlankIfEitherIsBlank( " -cam \"", GetPluginInfoEntryWithDefault( "Camera", "") ), "\"" )
        
        # The -proj option isn't supported by mental ray export.
        if( self.Renderer != "mentalrayexport" ):
            projectPath = GetPluginInfoEntryWithDefault( "ProjectPath", "" ).strip().replace( "\\", "/" )
            projectPath = CheckPathMapping( projectPath ).replace( "\\", "/" )
            if( len( projectPath ) > 0 and ( projectPath.endswith( "\\" ) or projectPath.endswith( "/" ) ) ):
                #projectPath = Path.GetDirectoryName( projectPath )
                projectPath = projectPath.rstrip( "/\\" )
            
            commonArguments += BlankIfEitherIsBlank( BlankIfEitherIsBlank( " -proj \"", projectPath ), "\"" )
        
        # Build the final argument list.
        renderArguments = ""
        
        sceneFile = GetPluginInfoEntryWithDefault( "SceneFile", GetDataFilename() ).strip().replace( "\\", "/" )
        sceneFile = CheckPathMapping( sceneFile ).replace( "\\", "/" )
        sceneFile = PathUtils.ToPlatformIndependentPath( sceneFile )
        
        commandLineOptions = GetPluginInfoEntryWithDefault( "CommandLineOptions", "" )
        if GetBooleanPluginInfoEntryWithDefault( "UseOnlyCommandLineOptions", False ):
            renderArguments = commandLineOptions + " \"" + sceneFile + "\""
        else:
            renderArguments = rendererArguments + " " + commonArguments + " " + commandLineOptions + " \"" + sceneFile + "\""
        
        return renderArguments
    
    def GetTileRenderArgument( self ):
        tileArgument = ""
        
        if( self.RegionRendering ):
            if self.SingleRegionJob:
                regionLeft = GetPluginInfoEntryWithDefault( "RegionLeft" + self.SingleRegionIndex, "0" )
                regionRight = GetPluginInfoEntryWithDefault( "RegionRight" + self.SingleRegionIndex, "0" )
                regionTop = GetPluginInfoEntryWithDefault( "RegionTop" + self.SingleRegionIndex, "0" )
                regionBottom = GetPluginInfoEntryWithDefault( "RegionBottom" + self.SingleRegionIndex, "0" )
            else:
                regionLeft = GetIntegerPluginInfoEntryWithDefault( "RegionLeft", 0 )
                regionRight = GetIntegerPluginInfoEntryWithDefault( "RegionRight", 0 )
                regionTop = GetIntegerPluginInfoEntryWithDefault( "RegionTop", 0 )
                regionBottom = GetIntegerPluginInfoEntryWithDefault( "RegionBottom", 0 )
            
            if( self.Renderer == "renderman" ):
                resx = GetIntegerPluginInfoEntryWithDefault( "ImageWidth", 0 )
                resy = GetIntegerPluginInfoEntryWithDefault( "ImageHeight", 0 )
                if( resx > 0 and resy > 0 ):
                    regionLeftPercent = float(regionLeft) / float(resx)
                    regionRightPercent = float(regionRight) / float(resx)
                    regionTopPercent = float(regionTop) / float(resy)
                    regionBottomPercent = float(regionBottom) / float(resy)
                    
                    tileArgument = " -crop " + str(regionLeftPercent) + " " + str(regionRightPercent) + " " + str(regionTopPercent) + " " + str(regionBottomPercent)
            elif( self.Renderer == "turtle" ):
                tileArgument = " -region " + str(regionLeft) + " " + str(regionTop) + " " + str(regionRight) + " " + str(regionBottom)
            elif( self.Renderer == "3delight" ):
                resx = GetIntegerPluginInfoEntryWithDefault( "ImageWidth", 0 )
                resy = GetIntegerPluginInfoEntryWithDefault( "ImageHeight", 0 )
                if( resx > 0 and resy > 0 ):
                    regionLeftPercent = float(regionLeft) / float(resx)
                    regionRightPercent = float(regionRight) / float(resx)
                    regionTopPercent = float(regionTop) / float(resy)
                    regionBottomPercent = float(regionBottom) / float(resy)
                
                tileArgument = " -crop true -crminx " + str(regionLeftPercent) + " -crminy " + str(regionTopPercent) + " -crmaxx " + str(regionRightPercent) + " -crmaxy " + str(regionBottomPercent)
            else:
                tileArgument = " -reg " + str(regionLeft) + " " + str(regionRight) + " " + str(regionTop) + " " + str(regionBottom)
        
        return tileArgument
    
    ## Called by Deadline before the render begins.
    def PreRenderTasks( self ):
        try:
            self.FinishedFrameCount = 0
            self.PreviousFinishedFrame = ""
            self.SkipNextFrame = False

            self.stopwatch = Stopwatch()
            self.stopwatch.Start()

            env_map_json = GetJobInfoEntry(MxDLJob.hEnv)
            dir_map_json = GetJobInfoEntry(MxDLJob.hDirMap)
            plugins_json = GetJobInfoEntry(MxDLJob.hPlugins)

            plugins_env_map = plugins.mxConfigMayaPlugin(plugins_json)

            env.mxSetDriverMapping(dir_map_json)
            env.mxSetMayaEnvironment(env_map_json, plugins_env_map)

            plugins_json = GetJobInfoEntry(MxDLJob.hPlugins)
        except:
            import traceback
            LogInfo(traceback.format_exc())

    
    ## Called by Deadline after the render finishes.
    def PostRenderTasks( self ):
        if( self.LocalRendering and self.Renderer != "3delight" ):
            LogInfo( "Moving output files and folders from " + self.LocalRenderDirectory + " to " + self.NetworkRenderDirectory )
            VerifyAndMoveDirectory( self.LocalRenderDirectory, self.NetworkRenderDirectory, True, 0 )
        
        if( self.RegionRendering and not self.SingleRegionJob ):
            paddingRegex = re.compile( "[^\\?#]*([\\?#]+).*" )
            
            outputFileCount = GetJobInfoEntryElementCount( "OutputFileNames" )
            for fileIndex in range( 0, outputFileCount ):
                for frameNum in range( GetStartFrame(), GetEndFrame() + 1 ):
                    outputDirectory = GetJobInfoEntryElement( "OutputDirectories", fileIndex )
                    outputDirectory = CheckPathMapping( outputDirectory ).replace( "\\", "/" )
                    
                    outputFilename = GetJobInfoEntryElement( "OutputFileNames", fileIndex )
                    
                    outputPath = Path.Combine( outputDirectory, outputFilename )
                    match = re.match( paddingRegex, outputPath )
                    if( match != None ):
                        padding = match.group(1)
                        
                        frame = StringUtils.ToZeroPaddedString( frameNum, len( padding ), False )
                        outputPath = outputPath.replace( padding, frame )
                    
                    LogInfo( "Calling tile assembler on " + outputPath )
                    
                    arguments = "\"" + outputPath + "\""
                    if( self.Renderer == "mentalray" or self.Renderer == "vray" ):
                        LogInfo( "  Setting --not-cropped flag because we're using " + self.Renderer )
                        arguments = "--not-cropped --ignore-overlap " + arguments
                    
                    if (IsRunningOnWindows() or IsRunningOnLinux()) and Is64Bit():
                        LogInfo( "Using 64 bit Tile Assembler" )
                        RunProcess( "tileassembler64", arguments, GetDeadlineBinPath(), -1 )
                    else:
                        RunProcess( "tileassembler", arguments, GetDeadlineBinPath(), -1 )

        ## rollback PATH environment variable
        env.mxRollbackPathEnvironment()

        ## send cost request
        self.stopwatch.Stop()

        r_time = self.stopwatch.Elapsed.TotalHours

        sm = SlaveManager.GetInstance()
        cpus = sm.Slave.CPUs
        task_id = sm.Slave.CurrentTasksString
        job = sm.Slave.CurrentJob

        cb = callback.MxDLTaskFinishCallback(job, task_id, cpus, r_time)
        LogInfo(cb.send())

    
    ## Called by Deadline to check the exit code from the renderer.
    def CheckExitCode( self, exitCode ):
        if exitCode != 0:
            if exitCode == 206:
                FailRender( "Maya could not parse the command line. Two common causes for this are using the wrong project directory, or using a drive root ( i.e. \"c:\\\" ) as the output directory." )
            elif ( exitCode == 211 and self.IgnoreError211 ):
                LogInfo( "Renderer reported an error with error code 211. This will be ignored, since the option to ignore it is specified in the Job Properties." );
            else:
                FailRender( "Renderer returned non-zero error code %d. Check the renderer's output." % exitCode )
    
    def HandleFlexlmError( self ):
        FailRender( self.GetRegexMatch(0) )
    
    def HandleUsageError( self ):
        FailRender( "Bad command line arguments: " + RenderArgument() + "\nMaya Error: " + self.GetPreviousStdoutLine() )
    
    def HandleChunkedProgress1( self ):
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        if( endFrame - startFrame + 1 != 0 ):
            SetProgress( 100 * ( int(self.GetRegexMatch(1)) - startFrame + 1 ) / ( endFrame - startFrame + 1 ) )
    
    def HandleChunkedProgress2( self ):
        self.FinishedFrameCount += 1
        
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        if( endFrame - startFrame + 1 != 0 ):
            SetProgress( 100 * ( self.FinishedFrameCount / ( endFrame - startFrame + 1) ) )
    
    def HandleStatusMessage( self ):
        SetStatusMessage( self.GetRegexMatch(0) )
        
    def HandleErrorMessage( self ):
        errorMessage = self.GetRegexMatch(0)
        #if( not GetBooleanConfigEntry( "StrictErrorChecking" ) ):
        if( not GetBooleanPluginInfoEntryWithDefault( "StrictErrorChecking", True ) ):
            LogWarning( "Deadline is ignoring error \"" + errorMessage + "\" because plugin setting Strict Error Checking is disabled." )
        else:
            ignoreError = True
            
            if( errorMessage.find( "could not get a license" ) != -1 ):
                ignoreError = False
            #elif( errorMessage.find( "Error: Cannot find procedure " ) != -1 ):
            #    ignoreError = False
            elif( errorMessage.find( "This scene does not have any renderable cameras" ) != -1 ):
                ignoreError = False
            elif( ( errorMessage.find( "Error: Camera" ) != -1 ) and ( errorMessage.find( "does not exist" ) != -1 ) ):
                ignoreError = False
            elif( errorMessage.find( "Warning: The post-processing failed while attempting to rename file" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: Failed to open IFF file for reading" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: An exception has occurred, rendering aborted." ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Cannot open project" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: Cannot load scene" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: Scene was not loaded properly, please check the scene name" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: Graphics card capabilities are insufficient for rendering." ) != -1 ):
                ignoreError = False
            #elif( errorMessage.find( "Error: No object matches name:" ) != -1 ):
            #    ignoreError = False
            elif( ( errorMessage.find( "Error: The attribute " ) != -1 ) and ( errorMessage.find( "was locked in a referenced file, and cannot be unlocked." ) != -1 ) ):
                ignoreError = False
            elif( errorMessage.find( "Not enough storage is available to process this command." ) != -1 ):
                ignoreError = False
            elif( errorMessage.find("Error: (Mayatomr) : mental ray has stopped with errors, see the log" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Warning: (Mayatomr.Scene) : no render camera found, final scene will be incomplete and can't be rendered" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "mental ray: out of memory" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "The specified module could not be found." ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: (Mayatomr.Export) : mental ray startup failed with errors" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Number of arguments on call to preLayerScript does not match number of parameters in procedure definition." ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: rman Fatal:" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "rman Error:" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Error: There was a fatal error rendering the scene." ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Could not obtain a license" ) != -1 ):
                ignoreError = False
            elif( errorMessage.find( "Could not read V-Ray environment variable" ) != -1 ):
                ignoreError = False
            #elif( errorMessage.find( "was not found on MAYA_PLUG_IN_PATH." ) != -1 ):
            #    ignoreError = False
            
            if( ignoreError ):
                LogInfo( "Deadline is ignoring error: \"" + errorMessage + "\" because plugin setting Strict Error Checking is enabled and this error is not usually fatal." )
            else:
                FailRender( "Deadline caught error \"" + errorMessage + "\". If this error message is unavoidable but not fatal to your render, please email support@thinkboxsoftware.com with the error message, and disable the Maya job setting Strict Error Checking." )
    
    def HandleProgressMessage1( self ):
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        if( endFrame - startFrame + 1 != 0 ):
            SetProgress( ( float(self.GetRegexMatch(1)) + self.FinishedFrameCount * 100 ) / ( endFrame - startFrame + 1 ) )
            self.SuppressThisLine()
    
    def HandleProgressMessage2( self ):
        self.FinishedFrameCount += 1
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        if( endFrame - startFrame + 1 != 0 ):
            SetProgress( ( self.FinishedFrameCount * 100 ) / ( endFrame - startFrame + 1 ) )
    
    def HandleProgressMessage3( self ):
        SetProgress( float(self.GetRegexMatch(1)) )
        self.SuppressThisLine()
    
    def HandleVrayMessage ( self ):
        progressStatus = None
        errorMessage = self.GetRegexMatch(0)
        if( errorMessage.find( "V-Ray: Building light cache" ) != -1 ):
            progressStatus = 'Building light cache.'
        elif( errorMessage.find( "V-Ray: Building global photon map" ) != -1 ):
            progressStatus = self.GetRegexMatch(0)
        elif( errorMessage.find( "V-Ray: Prepass" ) != -1 ):
            progressStatus = self.GetRegexMatch(0)
        elif( errorMessage.find( "V-Ray: Rendering image" ) != -1 ):
            progressStatus = self.GetRegexMatch(0)
            self.vrayRenderingImage = True
                
        if progressStatus is not None:
            SetStatusMessage(progressStatus)
            
    def HandleVrayProgress ( self ):
        if self.vrayRenderingImage == True:
            startFrame = GetStartFrame()
            endFrame = GetEndFrame()
            if( endFrame - startFrame + 1 != 0 ):
                SetProgress( ( float(self.GetRegexMatch(1)) + self.FinishedFrameCount * 100 ) / ( endFrame - startFrame + 1 ) )
    
    def HandleVrayExportProgress( self ):
        self.FinishedFrameCount += 1
        
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        if( endFrame - startFrame + 1 != 0 ):
            SetProgress( ( (self.FinishedFrameCount-1) * 100 ) / ( endFrame - startFrame + 1 ) )
        
    def HandleVrayExportComplete( self ):
        SetProgress( 100 )
    
    ########################################################################
    ## Mental Ray progress handling.
    ########################################################################
    def HandleMentalRayProgress( self ):
        startFrame = GetStartFrame()
        endFrame = GetEndFrame()
        if( endFrame - startFrame + 1 != 0 ):
            SetProgress( ( float(self.GetRegexMatch(1)) + self.FinishedFrameCount * 100 ) / ( endFrame - startFrame + 1 ) )
            SetStatusMessage( self.GetRegexMatch(0) )
            self.SuppressThisLine()
    
    def HandleMentalRayComplete( self ):
        if self.SkipNextFrame:
            self.SkipNextFrame = False
        else:
            self.FinishedFrameCount += 1
            startFrame = GetStartFrame()
            endFrame = GetEndFrame()
            if( endFrame - startFrame + 1 != 0 ):
                SetProgress( ( self.FinishedFrameCount * 100 ) / ( endFrame - startFrame + 1 ) )
    
    def HandleMentalRayGathering( self ):
        SetStatusMessage( self.GetRegexMatch(0) )
        self.SuppressThisLine()
    
    def HandleMentalRayWritingFrame( self ):
        currFinishedFrame = self.GetRegexMatch(1)
        if self.PreviousFinishedFrame == currFinishedFrame:
            self.SkipNextFrame = True
        else:
            self.PreviousFinishedFrame = currFinishedFrame
