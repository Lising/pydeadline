# -*- coding: utf-8 -*-

# import standard python lib
from System.Environment import *
import sys
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
if root not in sys.path:
    sys.path.append(root)
root += r'\PyDeadline\Python26\Lib'
if root not in sys.path:
    sys.path.append(root)
from PyDeadline.deadline import MxDLCreateJob

# main
def __main__((dlArgs, qsArgs)):

    # import base64
    # import json
    # data = {
    #     "job_info": {
    #         "user": 'admin',
    #         "project": 'test_project',
    #         "name": 'hahaha_test',
    #         "file": r'Y:\MatrixStudio\PyDeadline\sample_maya.ma',
    #         "output_root": r'Y:\MatrixStudio\PyDeadline\project\images',
    #         "frames": '1-20',
    #         "chunk_size": 3,
    #         "env": {"AAA":"A111", "BBB":"B222"},
    #         "map": {"c:/":"Z:/"},
    #         "render": "Maya",
    #         "server": "renderserver"
    #         "plugins": []
    #     },
    #     "render_info": {
    #         "project": r'Y:\MatrixStudio\PyDeadline\project',
    #         "version": '2014',
    #         "args": ''
    #     }
    # }
    #
    # data = base64.encodestring(json.dumps(data))

    data = qsArgs['data']
    return MxDLCreateJob().response(data)
