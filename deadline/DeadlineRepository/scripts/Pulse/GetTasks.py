# -*- coding: utf-8 -*-

# import standard python lib
from System.Environment import *
import sys
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
if root not in sys.path:
    sys.path.append(root)
root += r'\PyDeadline\Python26\Lib'
if root not in sys.path:
    sys.path.append(root)
from PyDeadline.deadline import MxDLGetTasks


# main
def __main__((dlArgs, qsArgs)):

    # import base64
    # import json
    # data = {
    #     "id": '999_050_999_5587db52',
    #     "limit": 2,
    #     "offset": 0
    # }
    #
    # data = base64.encodestring(json.dumps(data))

    data = qsArgs['data']
    return MxDLGetTasks().response(data)
