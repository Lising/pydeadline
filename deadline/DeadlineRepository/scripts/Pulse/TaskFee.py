# -*- coding: utf-8 -*-

# import standard python lib
from System.Environment import *
import sys
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
if root not in sys.path:
    sys.path.append(root)
root += r'\PyDeadline\Python26\Lib'
if root not in sys.path:
    sys.path.append(root)
from PyDeadline.deadline import MxDLTaskFee


# main
def __main__((dlArgs, qsArgs)):

    data = qsArgs['data']
    return MxDLTaskFee().response(data)
