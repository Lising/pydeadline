__all__ = ['MxDLCallback',
           'MxDLTaskFinishCallback']

# ------------------------------------------------------------------------------

import urllib
import urllib2
from schema import MxDLTaskFinishRequestSchema, MxDLTaskFinishResponseSchema
import traceback
import base64

from Deadline.Scripting import *
from Deadline.Configuration import DeadlineConfig

# ------------------------------------------------------------------------------

class MxDLCallback(object):

    def _send(self, cmd, json_data):
        data = base64.encodestring(json_data).replace('\n', '').replace('\r', '')

        server = ''
        port = 0
        for pulsename in RepositoryUtils.GetPulseNames():
            pulseinfo = RepositoryUtils.GetPulseInfo(pulsename)
            if pulseinfo.PulseState == 'Running':
                server = pulseinfo.PulseName
                port = DeadlineConfig.GetNetworkSettings().PulseWebServicePort

        url = 'http://%s:%d/%s?data=%s' % (server, port, cmd, data)
        req = urllib2.Request(url)

        try:
            res_data = urllib2.urlopen(req)
            res = res_data.read()

            response = MxDLTaskFinishResponseSchema.createFromJson(res)

            return response.data()['data']
        except:
            return traceback.format_exc()


class MxDLTaskFinishCallback(MxDLCallback):

    Cmd = 'TaskFee'

    def __init__(self, job, task_id, cpu, r_time):
        self.schema = MxDLTaskFinishRequestSchema()

        self.schema.setUser(job.JobUserName)
        self.schema.setProject(job.ExtraInfo0)
        self.schema.setJobId(job.JobId)
        self.schema.setTaskId(int(task_id))
        self.schema.setStatus('Complete')
        self.schema.setCpu(cpu)
        self.schema.setRenderTime(r_time)

    def send(self):
        data = self.schema.toJson()
        return self._send(self.Cmd, data)
