# coding=utf-8

__all__ = ['MxDLCmd',
           'MxDLCreateUser',
           'MxDLCreateJob',
           'MxDLDashboard',
           'MxDLGetJobs',
           'MxDLSuspendJobs',
           'MxDLResumeJobs',
           'MxDLFreezeJobs',
           'MxDLUnfreezeJobs',
           'MxDLBlockJobs',
           'MxDLUnblockJobs',
           'MxDLDeleteJobs',
           'MxDLGetTasks',
           'MxDLGetTaskLog',
           'MxDLGetIdleCount',
           'MxDLTaskFee',
           'MxDLCreateFTPUser',
           'MxDLFreezeFTPUser',
           'MxDLUnfreezeFTPUser',
           'MxDLDeleteFTPUser']

# ------------------------------------------------------------------------------

from schema import *
from log import *
from job import *
from ftp import *
import traceback
import base64
import tempfile
import urllib
import urllib2

# IronPython lib
from System.Collections.Specialized import StringCollection
from Deadline.Scripting import *
from Deadline.Users import UserInfo

# ------------------------------------------------------------------------------

class MxDLCmd(object):
    def __init__(self):
        pass

    def response(self, encoded_data):
        response_schema = MxDLResponseSchema()
        try:
            data = self._do(base64.decodestring(encoded_data))
            response_schema.setError(0)
            response_schema.setData(data)
        except:
            MxDLDebug.log(traceback.format_exc())
            response_schema.setError(1)
            response_schema.setData(traceback.format_exc())

        return response_schema.toJson()

    def _do(self, json_data):
        return ''


# ------------------------------------------------------------------------------

class MxDLCreateUser(MxDLCmd):
    def __init__(self):
        super(MxDLCreateUser, self).__init__()

    def _do(self, json_data):
        request = MxDLCreateUserRequestSchema.createFromJson(json_data)
        new_user = UserInfo(request.name(), '', '')
        RepositoryUtils.SaveUserInfo(new_user)
        response = MxDLCreateUserResponseSchema()
        return response.data()


# ------------------------------------------------------------------------------

class MxDLCreateJob(MxDLCmd):
    def __init__(self):
        super(MxDLCreateJob, self).__init__()

    def _do(self, json_data):
        request = MxDLCreateJobRequestSchema.createFromJson(json_data)
        job_info = request.jobInfo()
        plugin_info = request.renderInfo()

        # generate file path
        info_dir = self.__getTempDir()
        job_info_path = '/'.join([info_dir, 'deadline_job_info.job'])
        plugin_info_path = '/'.join([info_dir, 'deadline_plugin_info.job'])

        # write info file
        job_info_file = MxDLJobInfoFile(job_info)
        job_info_file.save(job_info_path)
        plugin_info_file = MxDLJobInfoReflector.createPluginInfoFile(job_info, plugin_info)
        plugin_info_file.save(plugin_info_path)

        # create new job
        p1args = StringCollection()
        p1args.Add(job_info_path)
        p1args.Add(plugin_info_path)
        result = ClientUtils.ExecuteCommandAndGetOutput(p1args)

        # return response
        response = MxDLCreateJobResponseSchema()
        response.setJobId(self.__getJobIdFromResult(result))
        return response.data()

    def __getJobIdFromResult(self, result):
        if 'Result=Success' in result:
            job_id = result.split('JobID=')[1].strip()
            return job_id
        else:
            raise

    def __getTempDir(self):
        return tempfile.gettempdir()


# ------------------------------------------------------------------------------

class MxDLDashboard(MxDLCmd):

    def __init__(self):
        super(MxDLDashboard, self).__init__()

    def _do(self, json_data):
        request = MxDLDashboardRequestSchema.createFromJson(json_data)
        response = MxDLDashboardResponseSchema()

        user = request.user()

        s_active = 0
        s_finished = 0
        s_paused = 0
        s_failed = 0
        s_queued = 0

        job_ids = PulseUtils.GetPulseJobIds()
        for job_id in job_ids:
            job = PulseUtils.GetPulseJobInfo(job_id)
            if user == job[MxDLJob.hUserName]:
                status = job[MxDLJob.hStatus]
                if status == 'Active':
                    s_active += 1
                elif status == 'Completed':
                    s_finished += 1
                elif status == 'Failed':
                    s_failed += 1
                elif status == 'Queued':
                    s_queued += 1
                elif status == 'Suspended':
                    s_paused += 1

        response.setFinished(s_finished)
        response.setActive(s_active)
        response.setPaused(s_paused)
        response.setQueued(s_queued)
        response.setFailed(s_failed)

        return response.data()


# ------------------------------------------------------------------------------

class MxDLGetJobs(MxDLCmd):

    def __init__(self):
        super(MxDLGetJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLGetJobRequestSchema.createFromJson(json_data)
        response = MxDLGetJobsResponseSchema()

        valid_jobs = []
        user = request.user()
        start = request.limit() * request.offset()
        job_ids = PulseUtils.GetPulseJobIds()
        for job_id in job_ids:
            job = PulseUtils.GetPulseJobInfo(job_id)
            if job[MxDLJob.hStatus] == 'Archived':
                continue
            if user == job[MxDLJob.hUserName]:
                if job[MxDLJob.hProject] == request.project() or request.project() == '':
                    name_filter = request.nameFilter().lower()
                    type_filter = request.typeFilter()
                    if name_filter != '' and name_filter not in job[MxDLJob.hName].lower():
                        continue
                    if type_filter != '':
                        n1 = job[MxDLJob.hPluginName]
                        n2 = MxDLJobInfoReflector.toPluginName(type_filter)
                        if n1 != n2:
                            continue

                    valid_jobs.append(job)

        response.setTotal(len(valid_jobs))
        if start < len(valid_jobs):
            end = min(len(valid_jobs), start+request.limit())
            valid_jobs = sorted(valid_jobs, key=lambda _job: _job[MxDLJob.hSubmitDataTime])
            for i in range(start, end):
                job = valid_jobs[i]
                schema = MxDLGetJobResponseSchema()
                schema.setId(job[MxDLJob.hId])
                schema.setProject(job[MxDLJob.hProject])
                schema.setServerName(job[MxDLJob.hServerName])
                schema.setName(job[MxDLJob.hName])
                schema.setRender(MxDLJobInfoReflector.toRenderName(job[MxDLJob.hPluginName]))
                schema.setStatus(job[MxDLJob.hStatus])
                schema.setFile(job[MxDLJob.hFile])
                schema.setOutput(job[MxDLJob.hOutputDirectories])

                schema.setTotalTasks(int(job[MxDLJob.hTaskCount]))
                schema.setSubmitDataTime(job[MxDLJob.hSubmitDataTime])
                schema.setStartDataTime(job[MxDLJob.hStartedDataTime])
                schema.setFinishDataTime(job[MxDLJob.hCompletedDataTime])

                self.setStatistics(job, schema)
                response.addJobResponseData(schema.data())

        return response.data()

    def setStatistics(self, job, schema):
        job_id = job['JobId']
        tasks = PulseUtils.GetJobTasks(job_id)

        failed = 0
        completed = 0
        total = 0
        for task in tasks:
            status = task[MxDLTask.hStatus]
            frame_count = len(task[MxDLTask.hTaskFrameList].split(','))
            total += frame_count
            if status == MxDLTask.hStatus_Completed:
                completed += frame_count
            elif status == MxDLTask.hStatus_Failed:
                failed += frame_count

        schema.setFailedFrames(failed)
        schema.setCompletedFrames(completed)
        schema.setFrameCount(total)


# ------------------------------------------------------------------------------

class MxDLSuspendJobs(MxDLCmd):

    def __init__(self):
        super(MxDLSuspendJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLJobOpRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()
        response.setFailed(request.jobIdList())
        deadline_ctrl = DeadlineScriptManager.GetInstance().DeadlineController

        for job_id in request.jobIdList():
            try:
                job = ScriptUtils.GetJob(job_id, True)
                deadline_ctrl.SuspendJob(job)
                response.removeFailedId(job_id)
            except:
                pass

        return response.data()


# ------------------------------------------------------------------------------

class MxDLResumeJobs(MxDLCmd):

    def __init__(self):
        super(MxDLResumeJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLJobOpRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()
        response.setFailed(request.jobIdList())
        deadline_ctrl = DeadlineScriptManager.GetInstance().DeadlineController

        for job_id in request.jobIdList():
            try:
                job = ScriptUtils.GetJob(job_id, True)
                deadline_ctrl.ResumeJob(job)
                response.removeFailedId(job_id)
            except:
                pass

        return response.data()


# ------------------------------------------------------------------------------

class MxDLFreezeJobs(MxDLCmd):

    def __init__(self):
        super(MxDLFreezeJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLJobOpRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()
        response.setFailed(request.jobIdList())

        for job_id in request.jobIdList():
            try:
                job = ScriptUtils.GetJob(job_id, True)
                job.JobPool = 'freeze_pool'
                RepositoryUtils.SaveJob(job)
                response.removeFailedId(job_id)
            except:
                pass

        return response.data()


# ------------------------------------------------------------------------------

class MxDLUnfreezeJobs(MxDLCmd):

    def __init__(self):
        super(MxDLUnfreezeJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLJobOpRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()
        response.setFailed(request.jobIdList())

        for job_id in request.jobIdList():
            try:
                job = ScriptUtils.GetJob(job_id, True)
                job.JobPool = 'none'
                RepositoryUtils.SaveJob(job)
                response.removeFailedId(job_id)
            except:
                pass

        return response.data()


# ------------------------------------------------------------------------------

class MxDLBlockJobs(MxDLCmd):

    def __init__(self):
        super(MxDLBlockJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLJobOpRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()
        response.setFailed(request.jobIdList())

        deadline_ctrl = DeadlineScriptManager.GetInstance().DeadlineController

        for job_id in request.jobIdList():
            try:
                job = ScriptUtils.GetJob(job_id, True)
                orig_status = job.JobStatus
                deadline_ctrl.SuspendJob(job)
                job.JobPool = 'freeze_pool'
                RepositoryUtils.SaveJob(job)
                if orig_status == 'Rendering' or orig_status == 'Pending':
                    deadline_ctrl.ResumeJob(job)
                response.removeFailedId(job_id)
            except:
                pass

        return response.data()


# ------------------------------------------------------------------------------

class MxDLUnblockJobs(MxDLCmd):

    def __init__(self):
        super(MxDLUnblockJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLJobOpRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()
        response.setFailed(request.jobIdList())

        for job_id in request.jobIdList():
            try:
                job = ScriptUtils.GetJob(job_id, True)
                job.JobPool = 'none'
                RepositoryUtils.SaveJob(job)
                response.removeFailedId(job_id)
            except:
                pass

        return response.data()


# ------------------------------------------------------------------------------

class MxDLDeleteJobs(MxDLCmd):

    def __init__(self):
        super(MxDLDeleteJobs, self).__init__()

    def _do(self, json_data):
        request = MxDLDeleteJobRequestSchema.createFromJson(json_data)
        response = MxDLJobOpResponseSchema()

        try:
            id_list = request.ids()
        except:
            id_list = []

        deadline_ctrl = DeadlineScriptManager.GetInstance().DeadlineController
        if len(id_list) > 0:
            for job_id in id_list:
                try:
                    # deadline_ctrl.DeleteJob(job_id, False)
                    job = ScriptUtils.GetJob(job_id, True)
                    deadline_ctrl.ArchiveJob(job)
                except:
                    response.addFailedId(job_id)
        else:
            project_id_list = request.project()
            for project_id in project_id_list:
                job_ids = PulseUtils.GetPulseJobIds()
                for job_id in job_ids:
                    job = PulseUtils.GetPulseJobInfo(job_id)
                    if project_id == job[MxDLJob.hProject]:
                        try:
                            # deadline_ctrl.DeleteJob(job_id, False)
                            job = ScriptUtils.GetJob(job_id, True)
                            deadline_ctrl.ArchiveJob(job)
                        except:
                            response.addFailedId(job_id)

        return response.data()


# ------------------------------------------------------------------------------

class MxDLGetTasks(MxDLCmd):

    def __init__(self):
        super(MxDLGetTasks, self).__init__()

    def _do(self, json_data):
        request = MxDLGetTaskRequestSchema.createFromJson(json_data)
        response = MxDLGetTasksResponseSchema()

        job_id = request.jobId()
        start = request.limit() * request.offset()
        tasks = PulseUtils.GetJobTasks(job_id)

        if start < len(tasks):
            end = min(len(tasks), start+request.limit())

            for i in range(start, end):
                task = tasks[i]
                schema = MxDLGetTaskResponseSchema()
                schema.setTaskId(int(task[MxDLTask.hId]))
                schema.setFrameList(self.formatFrameList(task[MxDLTask.hTaskFrameList]))
                schema.setStatus(task[MxDLTask.hStatus])
                schema.setProgress(int(task[MxDLTask.hProgress].replace('%', '')))
                schema.setErrorCount(int(task[MxDLTask.hErrorCount]))
                schema.setRenderTime(task[MxDLTask.hRenderTime])
                schema.setCpu(task[MxDLTask.hPeakCpuUsage])
                schema.setStartDataTime(task[MxDLTask.hStartDataTime])
                response.addTaskResponseData(schema.data())

        return response.data()

    def formatFrameList(self, s):
        l = []
        for i in s.split(','):
            l.append(int(i))
        return l


# ------------------------------------------------------------------------------

class MxDLGetTaskLog(MxDLCmd):

    def __init__(self):
        super(MxDLGetTaskLog, self).__init__()

    def _do(self, json_data):
        request = MxDLGetTaskLogRequestSchema.createFromJson(json_data)
        response = MxDLGetTaskLogResponseSchema()

        job_id = request.jobId()
        task_id = request.taskId()

        job = ScriptUtils.GetJob(job_id, True)
        task = RepositoryUtils.GetJobTasks(job, True)[task_id]
        ctrl = DeadlineScriptManager.GetInstance().DeadlineController
        reports = ctrl.GetJobLogReports(job, task)

        log = u''
        if len(reports) > 0:
            report = reports[0]
            if len(report) > 0:
                r = list(report)[-1].ReportMessage
                for line in r.splitlines():
                    if u'0: STDOUT: ' in line:
                        log += line.replace(u'0: STDOUT: ', u'') + u'\n'

        response.setLog(log.decode(u'raw_unicode_escape'))
        return response.data()


# ------------------------------------------------------------------------------

class MxDLGetIdleCount(MxDLCmd):

    def __init__(self):
        super(MxDLGetIdleCount, self).__init__()

    def _do(self, json_data):
        response = MxDLGetIdleCountResponseSchema()

        slave_names = PulseUtils.GetPulseSlaveNames()
        idle_count = 0
        for slave_name in slave_names:
            slave_info = PulseUtils.GetPulseSlaveInfo(slave_name)
            if slave_info['SlaveState'] == 'Idle':
                idle_count += 1

        response.setTotalCount(len(slave_names))
        response.setIdleCount(idle_count)
        return response.data()


# ------------------------------------------------------------------------------

class MxDLTaskFee(MxDLCmd):

    def __init__(self):
        super(MxDLTaskFee, self).__init__()

    def _do(self, data):
        coded_data = urllib.quote(data)
        url = 'http://%s:%d/%s?data=%s' % (MxDLConfigSchema.Singleton.frontalServer(),
                                           MxDLConfigSchema.Singleton.frontalPort(),
                                           'costaccount/cost', coded_data)
        req = urllib2.Request(url)

        try:
            res_data = urllib2.urlopen(req)
            res = res_data.read()
            return res
        except:
            return traceback.format_exc()


# ------------------------------------------------------------------------------

class MxDLCreateFTPUser(MxDLCmd):

    def __init__(self):
        super(MxDLCreateFTPUser, self).__init__()

    def _do(self, json_data):
        request = MxDLCreateFTPUserRequestSchema.createFromJson(json_data)
        user_name = request.userName()
        password = request.password()
        #home_dir = request.homeDir()

        f = MxDLFilezillaConfigXML.configFilePath()

        # modify the setting file
        config = MxDLFilezillaConfigXML()
        config.read(f)
        config.addUser(user_name, password)

        response = MxDLCreateFTPUserResponseSchema()
        config.write(f)

        # return response
        return response.data()


# ------------------------------------------------------------------------------

class MxDLFreezeFTPUser(MxDLCmd):

    def __init__(self):
        super(MxDLFreezeFTPUser, self).__init__()

    def _do(self, json_data):
        request = MxDLFreezeFTPUserRequestSchema.createFromJson(json_data)
        user_name = request.userName()

        f = MxDLFilezillaConfigXML.configFilePath()

        # modify the setting file
        config = MxDLFilezillaConfigXML()
        config.read(f)
        config.freezeUser(user_name, True)

        response = MxDLFreezeFTPUserResponseSchema()
        config.write(f)

        # return response
        return response.data()


# ------------------------------------------------------------------------------

class MxDLUnfreezeFTPUser(MxDLCmd):

    def __init__(self):
        super(MxDLUnfreezeFTPUser, self).__init__()

    def _do(self, json_data):
        request = MxDLUnfreezeFTPUserRequestSchema.createFromJson(json_data)
        user_name = request.userName()
        user_id = request.userId()

        f = MxDLFilezillaConfigXML.configFilePath()

        # modify the setting file
        config = MxDLFilezillaConfigXML()
        config.read(f)
        config.freezeUser(user_name, False)

        job_ids = PulseUtils.GetPulseJobIds()
        for job_id in job_ids:
            job = ScriptUtils.GetJob(job_id, True)
            if user_id == job.UserName:
                try:
                    job.JobPool = 'none'
                    RepositoryUtils.SaveJob(job)
                except:
                    pass

        response = MxDLUnfreezeFTPUserResponseSchema()
        config.write(f)

        # return response
        return response.data()


# ------------------------------------------------------------------------------

class MxDLDeleteFTPUser(MxDLCmd):

    def __init__(self):
        super(MxDLDeleteFTPUser, self).__init__()

    def _do(self, json_data):
        request = MxDLUnfreezeFTPUserRequestSchema.createFromJson(json_data)
        user_name = request.userName()

        f = MxDLFilezillaConfigXML.configFilePath()

        # modify the setting file
        config = MxDLFilezillaConfigXML()
        config.read(f)
        config.deleteUser(user_name)

        response = MxDLDeleteFTPUserResponseSchema()
        config.write(f)

        # return response
        return response.data()

