__all__ = ['MxDLDebug']

import tempfile


class MxDLDebug(object):

    DebugMode = True

    @classmethod
    def log(cls, s):
        if cls.DebugMode:
            temp_root = tempfile.gettempdir()
            log_file = open(temp_root+'/mx_deadline.log', 'a')
            try:
                log_file.write(s+'\n')
            finally:
                log_file.close()