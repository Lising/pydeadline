# coding=utf-8

all = ['MxDLFilezillaConfigXML']

# ------------------------------------------------------------------------------

from System import Xml
import os
import hashlib

from schema import MxDLConfigSchema

# ------------------------------------------------------------------------------

class MxDLFilezillaConfigXML(object):

    Default_User = 'default_user'

    ConfigFile = 'FileZilla Server.xml'

    def __init__(self):
        self.dom = None

    def read(self, f):
        self.dom = Xml.XmlDocument()
        self.dom.Load(f)

    def write(self, f):
        self.dom.Save(f)
        # fp = open(f, 'w')
        # try:
        #     self.dom.writexml(f, addindent='    ', newl='\n', encoding='utf-8')
        #     fp.close()
        # except Exception, e:
        #     fp.close()
        #     raise e

    def addUser(self, name, password):
        user_root = dir = MxDLConfigSchema.Singleton.ftpServerRemote() + '/' + name
        if not os.path.exists(user_root):
            os.mkdir(user_root)

        dir = MxDLConfigSchema.Singleton.ftpServerLocal() + '/' + name
        # root = self.dom.documentElement
        user_root = self.dom.GetElementsByTagName('Users').ItemOf[0]
        users = user_root.GetElementsByTagName('User')

        for user in users:
            if user.Attributes['Name'].Value == name:
                return

        for user in users:
            if user.Attributes['Name'].Value == self.Default_User:
                new_user = user.CloneNode(True)

                attr = new_user.Attributes['Name']
                attr.Value = name
                new_user.Attributes.SetNamedItem(attr)

                permission = new_user.GetElementsByTagName('Permission').ItemOf[0]
                attr = permission.Attributes['Dir']
                attr.Value = dir
                permission.Attributes.SetNamedItem(attr)

                options = new_user.GetElementsByTagName('Option')
                for option in options:
                    attr = option.Attributes['Name']
                    if attr.Value == 'Pass':
                        option.InnerText = hashlib.md5(password).hexdigest()
                        break

                # attr = new_user.Attributes['Name']
                # attr.Value = name
                # new_user.Attributes.SetNamedItem(attr)
                #
                # new_user.setAttribute('Name', name)
                # permission = new_user.getElementsByTagName('Permission')[0]
                # permission.setAttribute('Dir', dir)
                user_root.AppendChild(new_user)
                break

    def freezeUser(self, name, yn):
        # root = self.dom.documentElement
        # users = root.getElementsByTagName('User')
        user_root = self.dom.GetElementsByTagName('Users').ItemOf[0]
        users = user_root.GetElementsByTagName('User')

        for user in users:
            if user.Attributes['Name'].Value == name:
                options = user.GetElementsByTagName('Option')

                for option in options:
                    if option.Attributes['Name'].Value == 'FileRead':
                        if yn:
                            option.FirstChild.Value = '0'
                        else:
                            option.FirstChild.Value = '1'
                    #
                    # option_name = option.getAttribute('Name')
                    # if option_name == 'FileRead':
                    #     if yn:
                    #         option.firstChild.nodeValue = '0'
                    #     else:
                    #         option.firstChild.nodeValue = '1'
                break

    def deleteUser(self, name):
        user_root = self.dom.GetElementsByTagName('Users').ItemOf[0]
        users = user_root.GetElementsByTagName('User')

        for user in users:
            if user.Attributes['Name'].Value == name:
                user.ParentNode.RemoveChild(user)
                break

    @classmethod
    def configFilePath(cls):
        return MxDLConfigSchema.Singleton.ftpServerRoot() + '/' + cls.ConfigFile