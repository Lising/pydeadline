# coding=utf-8

__all__ = ['MxDLJob',
           'MxDLTask',
           'MxDLJobInfoReflector',
           'MxDLJobInfoFile',
           'MxDLMayaJobFile',
           'MxDLMaxJobFile']

#------------------------------------------------------------------------------

from schema import *

# IronPython lib
from System.Collections.Specialized import StringCollection
from Deadline.Scripting import *

import codecs
import os


#------------------------------------------------------------------------------

class MxDLTask(object):

    hId = 'TaskId'
    hStatus = 'Status'
    hTaskFrameList = 'TaskFrameList'
    hErrorCount = 'ErrorCount'
    hProgress = 'Progress'
    hRenderTime = 'NormalizedRenderTime'
    hPeakCpuUsage = 'PeakCpuUsage'
    hStartDataTime = 'RenderStartTime'

    hStatus_Completed = 'Completed'
    hStatus_Failed = 'Failed'


class MxDLJob(object):

    hName = 'Name'
    hPluginName = 'PluginName'
    hUserName = 'JobUserName'
    hId = 'JobId'
    hStatus = 'Status'
    hOutputDirectories = 'OutputDirectories'
    hTaskCount = 'TaskCount'
    hChunkSize = 'ChunkSize'
    hSubmitDataTime = 'SubmitDateTimeString'
    hStartedDataTime = 'StartedDateTimeString'
    hCompletedDataTime = 'CompletedDateTimeString'

    hProject = 'ExtraInfo0'
    hFile = 'ExtraInfo1'
    hEnv = 'ExtraInfo2'
    hDirMap = 'ExtraInfo3'
    hServerName = 'ExtraInfo4'
    hPlugins = 'ExtraInfo5'


#------------------------------------------------------------------------------

class MxDLJobInfoReflector(object):

    RenderList = [MxDLMayaRenderInfoSchema.RenderName,
                  MxDLMaxRenderInfoSchema.RenderName]
    PluginList = ['MXMayaCmd',
                  'MXMaxCmd']

    FileClassMap = {}

    @classmethod
    def toPluginName(cls, render_name):
        index = cls.RenderList.index(render_name)
        return cls.PluginList[index]

    @classmethod
    def toRenderName(cls, plugin_name):
        index = cls.PluginList.index(plugin_name)
        return cls.RenderList[index]

    @classmethod
    def createPluginInfoFile(cls, job_info, plugin_info):
        schema = MxDLJobInfoSchema(job_info)
        FileClass = cls.FileClassMap[schema.render()]
        return FileClass(job_info, plugin_info)


#------------------------------------------------------------------------------

class MxDLJobInfoFile(object):

    hName = 'Name'

    def __init__(self, data):
        self.__schema = MxDLJobInfoSchema(data)

    def save(self, path):
        content = [u'Plugin=%s' % MxDLJobInfoReflector.toPluginName(self.__schema.render()),
                   u'%s=%s' % (self.hName, self.__schema.name()),
                   u'UserName=%s' % self.__schema.user(),
                   u'OutputDirectory0=%s' % self.__schema.outputRoot(),
                   u'Frames=%s' % self.__schema.frames(),
                   u'ChunkSize=%s' % self.__schema.chunkSize(),

                   u'%s=%s' % (MxDLJob.hProject, self.__schema.project()),  # use this to store project id
                   u'%s=%s' % (MxDLJob.hFile, self.__schema.file()),  # use this to store environment
                   u'%s=%s' % (MxDLJob.hEnv, self.__schema.envToJson()),  # use this to store environment
                   u'%s=%s' % (MxDLJob.hDirMap, self.__schema.dirMapToJson()),  # use this to store dirmap
                   u'%s=%s' % (MxDLJob.hServerName, self.__schema.serverName()),  # use this to store server name
                   u'%s=%s' % (MxDLJob.hPlugins, self.__schema.pluginsToJson()),  # use this to store plugins list

                   # future using
                   # u'InitialStatus=%s' % u'Suspended',  # for debug
                   u'Priority=%d' % 50,
                   u'Pool=%s' % u'',
                   u'MachineLimit=%d' % 100,
                   u'OnJobComplete=%s' % u'',
                   u'TaskTimeoutMinutes=%s' % u'',
                   u'MinRenderTimeMinutes=%s' % u'',
                   u'ConcurrentTasks=%s' % u'',
                   u'Department=%s' % u'',
                   u'Group=%s' % u'',
                   u'LimitGroups=%s' % u'',
                   u'JobDependencies=%s' % u'']

        file_object = codecs.open(path, u'wb', u'utf-8')
        try:
            for i in content:
                file_object.write(i + os.linesep)
        finally:
            file_object.close()


class MxDLMayaJobFile(object):

    def __init__(self, job_info, plugin_info):
        self.__jobinfo_schema = MxDLJobInfoSchema(job_info)
        self.__plugin_schema = MxDLMayaRenderInfoSchema(plugin_info)

    def save(self, path):
        file_object = open(path, 'w')
        stat = False
        try:
            file_object.write('SceneFile=%s\n' % self.__jobinfo_schema.file())
            file_object.write('ProjectPath=%s\n' % self.__plugin_schema.project())
            file_object.write('OutputFilePath=%s\n' % self.__jobinfo_schema.outputRoot())
            file_object.write('Version=%s\n' % self.__plugin_schema.version())
            file_object.write('CommandLineOptions=%s\n' % self.__plugin_schema.args())

            file_object.write('Renderer=%s\n' % 'file')
            file_object.write('Build=%s\n' % '64bit')
            file_object.write('StrictErrorChecking=%s\n' % 'True')
            file_object.write('LocalRendering=%s\n' % 'True')
            file_object.write('MaxProcessors=%d\n' % 0)
            file_object.write('LocalRendering=%s\n' % 'True')
            file_object.write('AutoMemoryLimit=%s\n' % 'True')
            file_object.write('MemoryLimit=%d\n' % 0)
            file_object.write('UseOnlyCommandLineOptions=%d\n' % 0)
            file_object.write('IgnoreError211=%s\n' % 'False')

            stat = True
        finally:
            file_object.close()
            return stat

MxDLJobInfoReflector.FileClassMap[MxDLMayaRenderInfoSchema.RenderName] = MxDLMayaJobFile


class MxDLMaxJobFile(object):

    def __init__(self, job_info, plugin_info):
        self.__jobinfo_schema = MxDLJobInfoSchema(job_info)
        self.__plugin_schema = MxDLMaxRenderInfoSchema(plugin_info)

    def save(self, path):
        file_object = open(path, 'w')
        stat = False
        try:
            file_object.write('SceneFile=%s\n' % self.__jobinfo_schema.file())
            # file_object.write('ProjectPath=%s\n' % self.__plugin_schema.project())
            file_object.write('OutputFilename=%s\n' % self.__jobinfo_schema.outputRoot())
            file_object.write('Version=%s\n' % self.__plugin_schema.version())
            #file_object.write('CommandLineOptions=%s\n' % self.__plugin_schema.args())

            #file_object.write('Renderer=%s\n' % 'file')
            file_object.write('Build=%s\n' % '64bit')
            #file_object.write('StrictErrorChecking=%s\n' % 'True')
            file_object.write('LocalRendering=%s\n' % 'True')
            file_object.write('MaxProcessors=%d\n' % 0)
            file_object.write('LocalRendering=%s\n' % 'True')
            file_object.write('AutoMemoryLimit=%s\n' % 'True')
            file_object.write('MemoryLimit=%d\n' % 0)
            #file_object.write('UseOnlyCommandLineOptions=%d\n' % 0)
            #file_object.write('IgnoreError211=%s\n' % 'False')
            file_object.write('Application=Max\n')
            file_object.write('BitmapRoot=%s\n' % self.__plugin_schema.bitmapRoot())

            stat = True
        finally:
            file_object.close()
            return stat

MxDLJobInfoReflector.FileClassMap[MxDLMaxRenderInfoSchema.RenderName] = MxDLMaxJobFile