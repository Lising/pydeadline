__all__ = ['mxSetPathEnvironment',
           'mxRollbackPathEnvironment',
           'mxSetMayaEnvironment',
           'mxSetMaxEnvironment',
           'mxSetDriverMapping']


from System.Environment import *
import myjson
import os


def mxSetDriverMapping(dir_map_json):

    dir_map = myjson.loads(dir_map_json)

    for driver, path in dir_map.iteritems():
        parts = []
        for p in path.replace('/', '\\').split('\\'):
            if p.strip() != '':
                parts.append(p)
        path = '\\\\' + '\\'.join(parts)
        os.system('net use %s /delete /yes' % driver)
        os.system('net use %s "%s"' % (driver, path))


def mxSetPathEnvironment(path):
    orig_path_from_env = GetEnvironmentVariable('ORIG_PATH')
    if not orig_path_from_env:
        orig_path_from_env = GetEnvironmentVariable('Path')
        SetEnvironmentVariable('ORIG_PATH', orig_path_from_env)

    SetEnvironmentVariable('Path', orig_path_from_env + ';' + path)


#
# Invoke this method when task is finished on Slave
#
def mxRollbackPathEnvironment():
    orig_path_from_env = GetEnvironmentVariable('ORIG_PATH')
    if orig_path_from_env:
        SetEnvironmentVariable('Path', orig_path_from_env)


def mxCombineEnvMap(src_list):
    dst_map = {}
    for env_map in src_list:
        for key, value in env_map.iteritems():
            if key not in dst_map:
                dst_map[key] = value
            else:
                dst_map[key] = ';'.join([dst_map[key], value])
    return dst_map


def mxSetMayaEnvironment(env_map_json, plugins_map):

    env_map = myjson.loads(env_map_json)
    env_map = mxCombineEnvMap([plugins_map, env_map])

    for n, v in env_map.iteritems():
        if n.lower() == 'path':
            mxSetPathEnvironment(v)
        else:
            SetEnvironmentVariable(n, v)


def mxSetMaxEnvironment(env_map_json, plugins_map):

    env_map = myjson.loads(env_map_json)
    env_map = mxCombineEnvMap([plugins_map, env_map])

    for n, v in env_map.iteritems():
        if n.lower() == 'path':
            mxSetPathEnvironment(v)
        else:
            SetEnvironmentVariable(n, v)