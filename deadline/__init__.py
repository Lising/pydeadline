##
# This package should only be run under the pulse web service
##


from System.Environment import GetEnvironmentVariable
#root = GetEnvironmentVariable('PYDEADLINE')
root = r'B:\MatrixStudio'
python_root = root + r'\PyDeadline\Python26\Lib'
import sys
if python_root not in sys.path:
    sys.path.append(python_root)

from test import *
from schema import *
from cmd import *
from callback import *
from env import *
from job import *
from log import *

# read config file
filepath = root + r'\.env'
fp = open(filepath)
import myjson
try:
    config = myjson.load(fp)
    MxDLConfigSchema.Singleton = MxDLConfigSchema(config)
finally:
    fp.close()