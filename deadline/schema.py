# coding=utf-8

__all__ = ['MxDLSchema',
           'MxDLConfigSchema',
           'MxDLRequestSchema',
           'MxDLResponseSchema',
           'MxDLCreateUserRequestSchema',
           'MxDLCreateUserResponseSchema',
           'MxDLJobInfoSchema',
           'MxDLMayaRenderInfoSchema',
           'MxDLMaxRenderInfoSchema',
           'MxDLCreateJobRequestSchema',
           'MxDLCreateJobResponseSchema',
           'MxDLDashboardRequestSchema',
           'MxDLDashboardResponseSchema',
           'MxDLGetJobRequestSchema',
           'MxDLGetJobsResponseSchema',
           'MxDLGetJobResponseSchema',
           'MxDLJobOpRequestSchema',
           'MxDLJobOpResponseSchema',
           'MxDLDeleteJobRequestSchema',
           'MxDLGetTaskRequestSchema',
           'MxDLGetTaskResponseSchema',
           'MxDLGetTasksResponseSchema',
           'MxDLGetTaskLogRequestSchema',
           'MxDLGetTaskLogResponseSchema',
           'MxDLGetIdleCountResponseSchema',
           'MxDLTaskFinishRequestSchema',
           'MxDLTaskFinishResponseSchema',
           'MxDLCreateFTPUserRequestSchema',
           'MxDLCreateFTPUserResponseSchema',
           'MxDLFreezeFTPUserRequestSchema',
           'MxDLFreezeFTPUserResponseSchema',
           'MxDLUnfreezeFTPUserRequestSchema',
           'MxDLUnfreezeFTPUserResponseSchema',
           'MxDLDeleteFTPUserRequestSchema',
           'MxDLDeleteFTPUserResponseSchema']

#------------------------------------------------------------------------------

import myjson


#------------------------------------------------------------------------------

class MxDLSchema(object):

    def __init__(self, data=None):
        if not data:
            data = {}
        self.__data = data

    def data(self):
        return self.__data

    def setData(self, data):
        self.__data = data

    def toJson(self):
        return myjson.dumps(self.__data)

    def _createGetterSetter(self, getter, setter, handle_name):
        self.__setattr__(getter, lambda: self.__data.__getitem__(handle_name))
        self.__setattr__(setter, lambda x: self.__data.__setitem__(handle_name, x))

    @classmethod
    def createFromJson(cls, json_object):
        return cls(myjson.loads(json_object))


#------------------------------------------------------------------------------

class MxDLConfigSchema(MxDLSchema):

    Singleton = None

    hFrontalServer = 'server'
    hFrontalPort = 'port'

    hFtpServerRoot = 'ftp_server_root'
    hFtpServerRemote = 'ftp_server_remote'
    hFtpServerLocal = 'ftp_server_local'

    hPluginRoot = 'plugin_root'

    def __init__(self, data=None):
        super(MxDLConfigSchema, self).__init__(data)
        self._createGetterSetter('frontalServer', 'setFrontalServer', self.hFrontalServer)
        self._createGetterSetter('frontalPort', 'setFrontalPort', self.hFrontalPort)
        self._createGetterSetter('ftpServerRoot', 'setFtpServerRoot', self.hFtpServerRoot)
        self._createGetterSetter('ftpServerRemote', 'setFtpServerRemote', self.hFtpServerRemote)
        self._createGetterSetter('ftpServerLocal', 'setFtpServerLocal', self.hFtpServerLocal)
        self._createGetterSetter('pluginRoot', 'setPluginRoot', self.hPluginRoot)


#------------------------------------------------------------------------------

class MxDLRequestSchema(MxDLSchema):

    hCmd = 'cmd'
    hData = 'data'

    def __init__(self, data=None):
        super(MxDLRequestSchema, self).__init__(data)
        self._createGetterSetter('cmd', 'setCmd', self.hCmd)
        self._createGetterSetter('data', 'setData', self.hData)


class MxDLResponseSchema(MxDLSchema):

    hError = 'error'
    hData = 'data'

    def __init__(self, data=None):
        super(MxDLResponseSchema, self).__init__(data)
        self._createGetterSetter('error', 'setError', self.hError)
        self._createGetterSetter('data', 'setData', self.hData)


#------------------------------------------------------------------------------

class MxDLCreateUserRequestSchema(MxDLSchema):

    hName = 'name'

    def __init__(self, data=None):
        super(MxDLCreateUserRequestSchema, self).__init__(data)
        self._createGetterSetter('name', 'setName', self.hName)


#------------------------------------------------------------------------------

class MxDLCreateUserResponseSchema(MxDLSchema):

    def __init__(self, data=None):
        super(MxDLCreateUserResponseSchema, self).__init__(data)


#------------------------------------------------------------------------------

class MxDLJobInfoSchema(MxDLSchema):

    hProject = 'project'
    hUser = 'user'
    hName = 'name'
    hFile = 'file'
    hOutputRoot = 'output_root'
    hFrames = 'frames'
    hChunkSize = 'chunk_size'
    hEnv = 'env'
    hDirMap = 'map'
    hRender = 'render'
    hServerName = 'server'
    hPlugins = 'plugins'

    def __init__(self, data=None):
        super(MxDLJobInfoSchema, self).__init__(data)
        self._createGetterSetter('project', 'setProject', self.hProject)
        self._createGetterSetter('user', 'setUser', self.hUser)
        self._createGetterSetter('name', 'setName', self.hName)
        self._createGetterSetter('file', 'setFile', self.hFile)
        self._createGetterSetter('outputRoot', 'setOutputRoot', self.hOutputRoot)
        self._createGetterSetter('frames', 'setFrames', self.hFrames)
        self._createGetterSetter('chunkSize', 'setChunkSize', self.hChunkSize)
        self._createGetterSetter('env', 'setEnv', self.hEnv)
        self._createGetterSetter('dirMap', 'setDirMap', self.hDirMap)
        self._createGetterSetter('render', 'setRender', self.hRender)
        self._createGetterSetter('serverName', 'setServerName', self.hServerName)
        self._createGetterSetter('plugins', 'setPlugins', self.hPlugins)

    def envToJson(self):
        return myjson.dumps(self.env())

    def dirMapToJson(self):
        return myjson.dumps(self.dirMap())

    def pluginsToJson(self):
        return myjson.dumps(self.plugins())


class MxDLMayaRenderInfoSchema(MxDLSchema):

    hProject = 'project'
    hVersion = 'version'
    hArgs = 'args'

    RenderName = 'Maya'

    def __init__(self, data=None):
        super(MxDLMayaRenderInfoSchema, self).__init__(data)
        self._createGetterSetter('project', 'setProject', self.hProject)
        self._createGetterSetter('version', 'setVersion', self.hVersion)
        self._createGetterSetter('args', 'setArgs', self.hArgs)


class MxDLMaxRenderInfoSchema(MxDLSchema):

    hVersion = 'version'
    hBitmapRoot = 'bitmaproot'
    hArgs = 'args'

    RenderName = 'Max'

    def __init__(self, data=None):
        super(MxDLMaxRenderInfoSchema, self).__init__(data)
        self._createGetterSetter('version', 'setVersion', self.hVersion)
        self._createGetterSetter('args', 'setArgs', self.hArgs)
        self._createGetterSetter('bitmapRoot', 'setBitmapRoot', self.hBitmapRoot)


class MxDLCreateJobRequestSchema(MxDLSchema):

    hJobInfo = 'job_info'
    hRenderInfo = 'render_info'

    def __init__(self, data=None):
        super(MxDLCreateJobRequestSchema, self).__init__(data)
        self._createGetterSetter('jobInfo', 'setJobInfo', self.hJobInfo)
        self._createGetterSetter('renderInfo', 'setRenderInfo', self.hRenderInfo)


class MxDLCreateJobResponseSchema(MxDLSchema):

    hJobId = 'job_id'

    def __init__(self, data=None):
        super(MxDLCreateJobResponseSchema, self).__init__(data)
        self._createGetterSetter('jobId', 'setJobId', self.hJobId)


# ------------------------------------------------------------------------------

class MxDLDashboardRequestSchema(MxDLSchema):
    hUser = 'user'

    def __init__(self, data=None):
        super(MxDLDashboardRequestSchema, self).__init__(data)
        self._createGetterSetter('user', 'setUser', self.hUser)


class MxDLDashboardResponseSchema(MxDLSchema):
    hFinished = 'finished'
    hActive = 'active'
    hPaused = 'paused'
    hQueued = 'queued'
    hFailed = 'failed'

    def __init__(self, data=None):
        super(MxDLDashboardResponseSchema, self).__init__(data)
        self._createGetterSetter('finished', 'setFinished', self.hFinished)
        self._createGetterSetter('active', 'setActive', self.hActive)
        self._createGetterSetter('paused', 'setPaused', self.hPaused)
        self._createGetterSetter('queued', 'setQueued', self.hQueued)
        self._createGetterSetter('failed', 'setFailed', self.hFailed)


# ------------------------------------------------------------------------------

class MxDLGetJobRequestSchema(MxDLSchema):

    hUser = 'user'
    hProject = 'project'
    hNameFilter = 'name_filter'
    hTypeFilter = 'type_filter'
    hLimit = 'limit'
    hOffset = 'offset'

    def __init__(self, data=None):
        super(MxDLGetJobRequestSchema, self).__init__(data)
        self._createGetterSetter('user', 'setUser', self.hUser)
        self._createGetterSetter('project', 'setProject', self.hProject)
        self._createGetterSetter('nameFilter', 'setNameFilter', self.hNameFilter)
        self._createGetterSetter('typeFilter', 'setTypeFilter', self.hTypeFilter)
        self._createGetterSetter('limit', 'setLimit', self.hLimit)
        self._createGetterSetter('offset', 'setOffset', self.hOffset)


class MxDLGetJobsResponseSchema(MxDLSchema):

    hTotal = 'total'
    hJobList = 'job_list'

    def __init__(self, data=None):
        super(MxDLGetJobsResponseSchema, self).__init__(data)
        self._createGetterSetter('jobList', 'setJobList', self.hJobList)
        self._createGetterSetter('total', 'setTotal', self.hTotal)
        ## FIXME: should check that if list has already existed.
        self.setJobList([])

    def addJobResponseData(self, response_data):
        self.jobList().insert(0, response_data)


class MxDLGetJobResponseSchema(MxDLSchema):

    hId = 'id'
    hProject = 'project'
    hServerName = 'server'
    hName = 'name'
    hRender = 'render'
    hStatus = 'status'
    hFile = 'file'
    hOutput = 'output'
    hFrameCount = 'frame_count'
    hFailedFrames = 'failed_frames'
    hCompletedFrames = 'completed_frames'
    hTotalTasks = 'total_tasks'
    hSubmitDataTime = 'submit_datatime'
    hStartDataTime = 'start_datatime'
    hFinishDataTime = 'finish_datatime'

    def __init__(self, data=None):
        super(MxDLGetJobResponseSchema, self).__init__(data)
        self._createGetterSetter('id', 'setId', self.hId)
        self._createGetterSetter('project', 'setProject', self.hProject)
        self._createGetterSetter('serverName', 'setServerName', self.hServerName)
        self._createGetterSetter('name', 'setName', self.hName)
        self._createGetterSetter('render', 'setRender', self.hRender)
        self._createGetterSetter('status', 'setStatus', self.hStatus)
        self._createGetterSetter('file', 'setFile', self.hFile)
        self._createGetterSetter('output', 'setOutput', self.hOutput)
        self._createGetterSetter('frameCount', 'setFrameCount', self.hFrameCount)
        self._createGetterSetter('failedFrames', 'setFailedFrames', self.hFailedFrames)
        self._createGetterSetter('completedFrames', 'setCompletedFrames', self.hCompletedFrames)
        self._createGetterSetter('totalTasks', 'setTotalTasks', self.hTotalTasks)
        self._createGetterSetter('submitDataTime', 'setSubmitDataTime', self.hSubmitDataTime)
        self._createGetterSetter('startDataTime', 'setStartDataTime', self.hStartDataTime)
        self._createGetterSetter('finishDataTime', 'setFinishDataTime', self.hFinishDataTime)


#------------------------------------------------------------------------------

class MxDLJobOpRequestSchema(MxDLSchema):

    hJobIdList = 'job_id_list'

    def __init__(self, data=None):
        super(MxDLJobOpRequestSchema, self).__init__(data)
        self._createGetterSetter('jobIdList', 'setJobIdList', self.hJobIdList)


class MxDLJobOpResponseSchema(MxDLSchema):

    hFailed = 'failed'

    def __init__(self, data=None):
        super(MxDLJobOpResponseSchema, self).__init__(data)
        self._createGetterSetter('failed', 'setFailed', self.hFailed)
        self.setFailed([])

    def addFailedId(self, id):
        self.failed().append(id)

    def removeFailedId(self, id):
        self.failed().remove(id)


#------------------------------------------------------------------------------

class MxDLDeleteJobRequestSchema(MxDLSchema):

    hProject = 'project'
    hIds = 'ids'

    def __init__(self, data=None):
        super(MxDLDeleteJobRequestSchema, self).__init__(data)
        self._createGetterSetter('project', 'setProject', self.hProject)
        self._createGetterSetter('ids', 'setIds', self.hIds)


#------------------------------------------------------------------------------

class MxDLGetTaskRequestSchema(MxDLSchema):

    hJobId = 'id'
    hLimit = 'limit'
    hOffset = 'offset'

    def __init__(self, data=None):
        super(MxDLGetTaskRequestSchema, self).__init__(data)
        self._createGetterSetter('jobId', 'setJobId', self.hJobId)
        self._createGetterSetter('limit', 'setLimit', self.hLimit)
        self._createGetterSetter('offset', 'setOffset', self.hOffset)


class MxDLGetTaskResponseSchema(MxDLSchema):

    hTaskId = 'id'
    hFrameList = 'frame_list'
    hStatus = 'status'
    hProgress = 'progress'
    hErrorCount = 'error_count'
    hRenderTime = 'render_time'
    hCpu = 'cpu'
    hStartDataTime = 'start_datatime'

    def __init__(self, data=None):
        super(MxDLGetTaskResponseSchema, self).__init__(data)
        self._createGetterSetter('taskId', 'setTaskId', self.hTaskId)
        self._createGetterSetter('frameList', 'setFrameList', self.hFrameList)
        self._createGetterSetter('progress', 'setProgress', self.hProgress)
        self._createGetterSetter('status', 'setStatus', self.hStatus)
        self._createGetterSetter('errorCount', 'setErrorCount', self.hErrorCount)
        self._createGetterSetter('renderTime', 'setRenderTime', self.hRenderTime)
        self._createGetterSetter('cpu', 'setCpu', self.hCpu)
        self._createGetterSetter('startDataTime', 'setStartDataTime', self.hStartDataTime)


class MxDLGetTasksResponseSchema(MxDLSchema):

    hTaskList = 'task_list'

    def __init__(self, data=None):
        super(MxDLGetTasksResponseSchema, self).__init__(data)
        self._createGetterSetter('taskList', 'setTaskList', self.hTaskList)
        if data is None:
            self.setTaskList([])

    def addTaskResponseData(self, response):
        self.taskList().append(response)

#------------------------------------------------------------------------------

class MxDLGetTaskLogRequestSchema(MxDLSchema):

    hJobId = 'job_id'
    hTaskId = 'task_id'

    def __init__(self, data=None):
        super(MxDLGetTaskLogRequestSchema, self).__init__(data)
        self._createGetterSetter('jobId', 'setJobId', self.hJobId)
        self._createGetterSetter('taskId', 'setTaskId', self.hTaskId)


class MxDLGetTaskLogResponseSchema(MxDLSchema):

    hLog = 'log'

    def __init__(self, data=None):
        super(MxDLGetTaskLogResponseSchema, self).__init__(data)
        self._createGetterSetter('log', 'setLog', self.hLog)


#------------------------------------------------------------------------------

class MxDLGetIdleCountResponseSchema(MxDLSchema):

    hIdle = 'idle'
    hTotal = 'total'

    def __init__(self, data=None):
        super(MxDLGetIdleCountResponseSchema, self).__init__(data)
        self._createGetterSetter('idleCount', 'setIdleCount', self.hIdle)
        self._createGetterSetter('totalCount', 'setTotalCount', self.hTotal)


#------------------------------------------------------------------------------

class MxDLTaskFinishRequestSchema(MxDLSchema):

    hUser = 'user'
    hProject = 'project'
    hJobId = 'job_id'
    hTaskId = 'task_id'
    hStatus = 'status'
    hCpu = 'cpu'
    hRenderTime = 'render_time'

    def __init__(self, data=None):
        super(MxDLTaskFinishRequestSchema, self).__init__(data)
        self._createGetterSetter('user', 'setUser', self.hUser)
        self._createGetterSetter('project', 'setProject', self.hProject)
        self._createGetterSetter('jobId', 'setJobId', self.hJobId)
        self._createGetterSetter('taskId', 'setTaskId', self.hTaskId)
        self._createGetterSetter('status', 'setStatus', self.hStatus)
        self._createGetterSetter('cpu', 'setCpu', self.hCpu)
        self._createGetterSetter('renderTime', 'setRenderTime', self.hRenderTime)


class MxDLTaskFinishResponseSchema(MxDLSchema):

    hResult = 'result'

    def __init__(self, data=None):
        super(MxDLTaskFinishResponseSchema, self).__init__(data)
        self._createGetterSetter('result', 'setResult', self.hResult)


#------------------------------------------------------------------------------

class MxDLCreateFTPUserRequestSchema(MxDLSchema):

    hUserName = 'name'
    hPassword = 'pass'
    #hHomeDir = 'home'

    def __init__(self, data=None):
        super(MxDLCreateFTPUserRequestSchema, self).__init__(data)
        self._createGetterSetter('userName', 'setUserName', self.hUserName)
        self._createGetterSetter('password', 'setPassword', self.hPassword)
        #self._createGetterSetter('homeDir', 'setHomeDir', self.hHomeDir)


class MxDLCreateFTPUserResponseSchema(MxDLSchema):

    # hResult = 'result'

    def __init__(self, data=None):
        super(MxDLCreateFTPUserResponseSchema, self).__init__(data)
        # self._createGetterSetter('result', 'setResult', self.hResult)


#------------------------------------------------------------------------------

class MxDLFreezeFTPUserRequestSchema(MxDLSchema):

    hUserName = 'name'

    def __init__(self, data=None):
        super(MxDLFreezeFTPUserRequestSchema, self).__init__(data)
        self._createGetterSetter('userName', 'setUserName', self.hUserName)


class MxDLFreezeFTPUserResponseSchema(MxDLSchema):

    # hResult = 'result'

    def __init__(self, data=None):
        super(MxDLFreezeFTPUserResponseSchema, self).__init__(data)
        # self._createGetterSetter('result', 'setResult', self.hResult)


#------------------------------------------------------------------------------

class MxDLUnfreezeFTPUserRequestSchema(MxDLSchema):

    hUserName = 'name'
    hUserId = 'userid'

    def __init__(self, data=None):
        super(MxDLUnfreezeFTPUserRequestSchema, self).__init__(data)
        self._createGetterSetter('userName', 'setUserName', self.hUserName)
        self._createGetterSetter('userId', 'setUserId', self.hUserId)


class MxDLUnfreezeFTPUserResponseSchema(MxDLSchema):

    # hResult = 'result'

    def __init__(self, data=None):
        super(MxDLUnfreezeFTPUserResponseSchema, self).__init__(data)
        # self._createGetterSetter('result', 'setResult', self.hResult)


#------------------------------------------------------------------------------

class MxDLDeleteFTPUserRequestSchema(MxDLSchema):

    hUserName = 'name'

    def __init__(self, data=None):
        super(MxDLDeleteFTPUserRequestSchema, self).__init__(data)
        self._createGetterSetter('userName', 'setUserName', self.hUserName)


class MxDLDeleteFTPUserResponseSchema(MxDLSchema):

    # hResult = 'result'

    def __init__(self, data=None):
        super(MxDLDeleteFTPUserResponseSchema, self).__init__(data)
        # self._createGetterSetter('result', 'setResult', self.hResult)


#------------------------------------------------------------------------------

if __name__ == '__main__':
    a = MxDLCreateJobRequestSchema()
    a.setJobInfo('test')
    aj = a.toJson()
    print aj
    b = MxDLCreateJobRequestSchema.createFromJson(aj)
    print b.jobInfo()