# __all__ = ['mxConfigMayaPluginEnv']


import myjson
import os
import schema
import env
import sys

from System import Environment
from System.Diagnostics import Process, ProcessStartInfo
from System.IO import File, Path, Directory
from System.Text import Encoding


# def mxConfigMayaModuleEnv(root, env_json):
#     if 'PATH' in env_json:
#         env_json['PATH'] = '/'.join([root, env_json['PATH']])
#     if 'MAYA_SCRIPT_PATH' in env_json:
#         script_path = '/'.join([root, env_json['MAYA_SCRIPT_PATH']])
#         env_json['MAYA_SCRIPT_PATH'] = script_path
#         env_json['PYTHONPATH'] = script_path
#         if script_path not in sys.path:
#             sys.path.append(script_path)
#     if 'MAYA_PLUG_IN_PATH' in env_json:
#         env_json['MAYA_PLUG_IN_PATH'] = '/'.join([root, env_json['MAYA_PLUG_IN_PATH']])
#
#     return env_json
#
#
# def mxConfigMayaArnoldModuleEnv(root, env_json):
#     if 'MTOA_EXTENSIONS_PATH' in env_json:
#         env_json['MTOA_EXTENSIONS_PATH'] = '/'.join([root, env_json['MTOA_EXTENSIONS_PATH']])
#     if 'ARNOLD_PLUGIN_PATH' in env_json:
#         env_json['ARNOLD_PLUGIN_PATH'] = '/'.join([root, env_json['ARNOLD_PLUGIN_PATH']])
#
#     return env_json
#
#
# def mxConfigMayaMentalrayModuleEnv(root, env_json):
#     if 'MI_CUSTOM_SHADER_PATH' in env_json:
#         env_json['MI_CUSTOM_SHADER_PATH'] = '/'.join([root, env_json['MI_CUSTOM_SHADER_PATH']])
#     if 'MI_LIBRARY_PATH' in env_json:
#         env_json['MI_LIBRARY_PATH'] = '/'.join([root, env_json['MI_LIBRARY_PATH']])
#
#     return env_json


def mxGetPluginRoot(type, name):
    root = schema.MxDLConfigSchema.Singleton.pluginRoot()
    plugin, version = name.replace('\\', '/').split('/')
    plugin_root = '/'.join([root, type, plugin, version])
    return plugin_root

def mxConfigMaxPathFile(maproot, version):
    app_local = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)
    init_file_path = '%s\\Autodesk\\3dsmax\\%s - 64bit\\enu\\3dsmax.ini' % (app_local, version)
    init_bak_file_path = '%s.bak' % init_file_path

    if not os.path.exists(init_bak_file_path):
        File.Copy(init_file_path, init_bak_file_path)

    ctx = ''

    index = 9
    for parent, dirnames, filenames in os.walk(maproot):
        ctx += 'Dir%s=%s\r\n' % (str(index), parent)
        index += 1

    orig_ctx = File.ReadAllText(init_bak_file_path)
    head, foot = orig_ctx.split('[BitmapDirs]\r\n')
    parts = foot.split('[')
    parts[0] = parts[0] + ctx

    ctx = head + '[BitmapDirs]\r\n' + '['.join(parts)

    # ctx = parts[0] + ctx + '[XReferenceDirs]\r\n' + parts[1]

    File.WriteAllText(init_file_path, ctx)


def mxConfigMaxPluginEnv(plugins_json):
    plugins = myjson.loads(plugins_json)
    configs = []
    for p in plugins:
        plugin_root = mxGetPluginRoot('max', p)
        env_path = '/'.join([plugin_root, '.env'])
        plugin_config = {}

        if os.path.exists(env_path):
            fp = open(env_path)
            try:
                plugin_config = myjson.load(fp)
            finally:
                fp.close()

            plugin_root_handle = '@PLUGINROOT'
            for p in plugin_config.keys():
                plugin_config[p] = plugin_config[p].replace(plugin_root_handle, plugin_root)

        configs.append(plugin_config)

    plugins_env_map = env.mxCombineEnvMap(configs)
    return plugins_env_map


def mxConfigMaxPlugin(plugins_json):
    plugins = myjson.loads(plugins_json)

    config = ''

    for i, p in enumerate(plugins):
        plugin_root = mxGetPluginRoot('max', p)
        if 'vray' in p.lower():
            config += 'V-Ray main plug-ins=%s/plugins\r\n' % plugin_root
            config += 'V-Ray additional plug-ins=%s/plugins/vrayplugins\r\n' % plugin_root
        elif 'maxforestpackpro' in p.lower():
            config += 'Itoo Software Forest Pack Pro plug-ins=%s/plugins\r\n' % plugin_root
            os.system(r'reg import %s' % os.path.join(plugin_root, r'reg\addSoft.reg'))
            os.system(r'reg import %s' % os.path.join(plugin_root, r'reg\addWow64.reg'))
        elif i:
            config += 'Additional MAX Plug-ins%s=%s/plugins\r\n' % (str(i), plugin_root)
        else:
            config += 'Additional MAX Plug-ins=%s/plugins\r\n' % plugin_root

    return config

def mxMakeTempFile(scene_path):
    temp_scene_path = Path.GetTempPath() + 'temp.max'
    File.Copy(scene_path, temp_scene_path, True)
    return temp_scene_path


def mxPrepareFile(version, scene_path, output_path, bitmap_root, max_install_root, env_json, plugin_env_map):
    max_exe = max_install_root + '\\3dsmax.exe'
    #startup_path = max_install_root + '/scripts/startup/temp.ms'
    startup_path = 'c:\\vray_changePath.ms'
    scripts_contents = '''fileName = "%s"\r\n
newPath = "%s"\r\n''' % (scene_path.replace('\\', '\\\\').replace('/', ''), output_path.replace('\\', '\\\\'))
    scripts_contents += File.ReadAllText('B:\\StartupPluinsEnv\\max\\scripts\\vray_changePath.ms')
    File.WriteAllText(startup_path, scripts_contents)

    user_app_dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).replace('\\Roaming', '')
    _3dsmax_ini = '%s/Local/Autodesk/3dsmax/%s - 64bit/enu/3dsmax.ini' % (user_app_dir, version)
    orig_3dsmax_ini = '%s/Local/Autodesk/3dsmax/%s - 64bit/enu/3dsmax.ini.bak' % (user_app_dir, version)

    if not os.path.exists(orig_3dsmax_ini):
        File.Copy(_3dsmax_ini, orig_3dsmax_ini, True)

    if bitmap_root != '':
        max_ini_ctx = File.ReadAllText(orig_3dsmax_ini) + '\n'
        max_ini_ctx += mxConfigMaxPathFile(bitmap_root, version)
        File.WriteAllText(_3dsmax_ini, max_ini_ctx, Encoding.Default)

    # cmd = '"%s" -mip -silent -U MAXScript "%s" "%s"' % (max_exe, startup_path, scene_path)
    #
    # env_map = json.loads(env_json)
    # env_map = env.mxCombineEnvMap([plugin_env_map, env_map, {
    #     'PATH': max_install_root,
    #     'ADSK_3DSMAX_x64_'+version: max_install_root
    # }])
    #
    # p = subprocess.Popen(args=cmd, env=env_map)
    # p.wait()

    bat_content = []

    env_map = myjson.loads(env_json)
    env_map = env.mxCombineEnvMap([plugin_env_map, env_map])
    for n, v in env_map.iteritems():
        bat_content.append('set %s=%%%s%%;%s' % (n, n, v))

    bat_content.append('"%s" -mip -silent -U MAXScript "%s" "%s"' % (max_exe, startup_path, scene_path))
    bat_content = '\r\n'.join(bat_content)
    bat_path = 'c:/mx_prepare_max_file.bat'
    File.WriteAllText(bat_path, bat_content)

    proc = Process.Start(bat_path)
    if not File.Exists("c:/HandleMaxPopup.exe"):
        File.Copy(r"B:\MatrixStudio\HandleMaxPopup.exe", r"c:/HandleMaxPopup.exe", False)

    # for pp in Process.GetProcessesByName('HandleMaxPopup'):
    #     pp.kill()
    try:
        os.system("taskkill /im HandleMaxPopup.exe")
    except:
        pass
    Process.Start(r"c:/HandleMaxPopup.exe")
    proc.WaitForExit()

    # File.Delete(startup_path)
    # max_ini_ctx = File.ReadAllText(orig_3dsmax_ini)
    # File.WriteAllText(_3dsmax_ini, max_ini_ctx, Encoding.Default)


def __copyTree(src_dir, dst_dir):
    if not Directory.Exists(dst_dir):
        Directory.CreateDirectory(dst_dir)

    for f in Directory.GetFiles(src_dir):
        new_path = dst_dir + "\\" + f.Substring(src_dir.Length + 1)
        File.Copy(f, new_path, True)

    for d in Directory.GetDirectories(src_dir):
        new_path = dst_dir + "\\" + d.Substring(src_dir.Length + 1)
        __copyTree(d, new_path)


def mxCopyMaxPluginRelatedFiles(plugin_json, max_install_root):
    plugins = myjson.loads(plugin_json)
    for i, p in enumerate(plugins):
        plugin_root = mxGetPluginRoot('max', p)
        copy_dir_path = '/'.join([plugin_root, '.copy'])
        if os.path.exists(copy_dir_path):
            __copyTree(copy_dir_path, max_install_root)
        #
        #
        # if 'maxmultiscatter' in p.lower():
        #     File.Copy(plugin_root+'/MultiScatterMental.dlo', max_install_root+'/MultiScatterMental.dlo', True)
        #     File.Copy(plugin_root+'/MultiScatterVRay20001.dlo', max_install_root+'/MultiScatterVRay20001.dlo', True)
        #     File.Copy(plugin_root+'/MultiScatterVRay21001.dlo', max_install_root+'/MultiScatterVRay21001.dlo', True)
        #     File.Copy(plugin_root+'/MultiScatterVRay22002.dlo', max_install_root+'/MultiScatterVRay22002.dlo', True)
        #     File.Copy(plugin_root+'/MultiScatterVRay23001.dlo', max_install_root+'/MultiScatterVRay23001.dlo', True)
        #     File.Copy(plugin_root+'/MultiScatterVRaySP6.dlo', max_install_root+'/MultiScatterVRaySP6.dlo', True)


def mxConfigMayaPluginEnv(plugin_full_name):
    plugin_root = mxGetPluginRoot('maya', plugin_full_name)
    env_path = '/'.join([plugin_root, '.env'])
    config = {}

    if os.path.exists(env_path):
        fp = open(env_path)
        import myjson
        try:
            config = myjson.load(fp)
        finally:
            fp.close()

        plugin_root_handle = '@PLUGINROOT'
        for p in config.keys():
            config[p] = config[p].replace(plugin_root_handle, plugin_root)
            if 'MAYA_SCRIPT_PATH' == p or 'PYTHONPATH' == p:
                script_path = config[p]
                if script_path not in sys.path:
                    sys.path.append(script_path)
                if p == 'MAYA_SCRIPT_PATH':
                    if 'PYTHONPATH' in config:
                        config['PYTHONPATH'] = config['PYTHONPATH'] + ';' + script_path
                    else:
                        config['PYTHONPATH'] = script_path
        #
        # config = mxConfigMayaModuleEnv(plugin_root, config)
        # config = mxConfigMayaArnoldModuleEnv(plugin_root, config)
        # config = mxConfigMayaMentalrayModuleEnv(plugin_root, config)
    return config


def mxConfigMayaPlugin(plugins_json):
    plugins = myjson.loads(plugins_json)
    configs = []
    for p in plugins:
        c = mxConfigMayaPluginEnv(p)
        configs.append(c)

    comman_script_path = schema.MxDLConfigSchema.Singleton.pluginRoot() + '/maya/scripts'
    configs.append({
        'MAYA_SCRIPT_PATH': comman_script_path,
        'PYTHONPATH': comman_script_path
    })

    plugins_env_map = env.mxCombineEnvMap(configs)
    return plugins_env_map